﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MiniProject262.Migrations
{
    public partial class miniproject262_v001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "m_admin",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    biodata_id = table.Column<long>(nullable: false),
                    code = table.Column<string>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_admin", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "m_admin");
        }
    }
}
