﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MiniProject262.Migrations
{
    public partial class miniproject262_v003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "m_admin");

            migrationBuilder.AddColumn<bool>(
                name: "is_delete",
                table: "m_admin",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "m_bank",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    va_code = table.Column<string>(maxLength: 10, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_bank", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_biodata",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    fullname = table.Column<string>(maxLength: 255, nullable: true),
                    mobile_phone = table.Column<string>(maxLength: 15, nullable: true),
                    image = table.Column<byte[]>(nullable: true),
                    image_path = table.Column<string>(maxLength: 255, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_biodata", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_biodata_address",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    biodata_id = table.Column<long>(nullable: false),
                    label = table.Column<string>(maxLength: 100, nullable: true),
                    recipient = table.Column<string>(maxLength: 100, nullable: true),
                    recipient_phone_number = table.Column<string>(maxLength: 15, nullable: true),
                    location_id = table.Column<long>(nullable: false),
                    postal_code = table.Column<string>(maxLength: 10, nullable: true),
                    address = table.Column<string>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_biodata_address", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_blood_group",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    code = table.Column<string>(maxLength: 5, nullable: true),
                    description = table.Column<string>(maxLength: 255, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_blood_group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_courier",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_courier", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_customer",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    biodata_id = table.Column<long>(nullable: false),
                    dob = table.Column<DateTime>(nullable: false),
                    gender = table.Column<string>(maxLength: 1, nullable: true),
                    blood_group_id = table.Column<long>(nullable: false),
                    rhesus_type = table.Column<string>(maxLength: 5, nullable: true),
                    height = table.Column<decimal>(nullable: false),
                    weight = table.Column<decimal>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_customer", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_customer_member",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    parent_biodata_id = table.Column<long>(nullable: false),
                    customer_id = table.Column<long>(nullable: false),
                    customer_relation_id = table.Column<long>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_customer_member", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_customer_relation",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_customer_relation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_doctor",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    biodata_id = table.Column<long>(nullable: false),
                    str = table.Column<string>(maxLength: 50, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_doctor", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_doctor_education",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_id = table.Column<long>(nullable: false),
                    education_level_id = table.Column<long>(nullable: false),
                    institution_name = table.Column<string>(maxLength: 100, nullable: true),
                    major = table.Column<string>(maxLength: 100, nullable: true),
                    start_year = table.Column<string>(maxLength: 4, nullable: true),
                    end_year = table.Column<string>(maxLength: 4, nullable: true),
                    is_last_education = table.Column<bool>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_doctor_education", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_education_level",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 10, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_education_level", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_location",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    parent_id = table.Column<long>(nullable: false),
                    location_level_id = table.Column<long>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_location", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_location_level",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    abbreviation = table.Column<string>(maxLength: 50, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_location_level", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_facility",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    medical_facility_category_id = table.Column<long>(nullable: false),
                    location_id = table.Column<long>(nullable: false),
                    full_address = table.Column<string>(nullable: true),
                    email = table.Column<string>(maxLength: 100, nullable: true),
                    phone_code = table.Column<string>(maxLength: 10, nullable: true),
                    phone = table.Column<string>(maxLength: 15, nullable: true),
                    fax = table.Column<string>(maxLength: 15, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_facility", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_facility_category",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_facility_category", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_facility_schedule",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    medical_facility_id = table.Column<long>(nullable: true),
                    day = table.Column<string>(nullable: true),
                    time_schedule_start = table.Column<string>(nullable: true),
                    time_schedule_end = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_facility_schedule", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_item",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    medical_item_category_id = table.Column<long>(nullable: false),
                    composition = table.Column<string>(nullable: true),
                    medical_item_segmentation_id = table.Column<long>(nullable: false),
                    manufacturer = table.Column<string>(nullable: true),
                    indication = table.Column<string>(nullable: true),
                    dosage = table.Column<string>(nullable: true),
                    directions = table.Column<string>(nullable: true),
                    contraindication = table.Column<string>(nullable: true),
                    caution = table.Column<string>(nullable: true),
                    packaging = table.Column<string>(nullable: true),
                    price_max = table.Column<long>(nullable: false),
                    price_min = table.Column<long>(nullable: false),
                    image = table.Column<byte[]>(nullable: true),
                    image_path = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_item", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_item_category",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_item_category", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_item_segmentation",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_item_segmentation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_menu",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    url = table.Column<string>(maxLength: 20, nullable: true),
                    parent_id = table.Column<long>(maxLength: 50, nullable: false),
                    big_icon = table.Column<string>(nullable: true),
                    small_icon = table.Column<string>(maxLength: 100, nullable: true),
                    created_by = table.Column<long>(maxLength: 100, nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_menu", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_menu_role",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    menu_id = table.Column<long>(nullable: false),
                    role_id = table.Column<long>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_menu_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_payment_method",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    created_by = table.Column<long>(maxLength: 50, nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_payment_method", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_role",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    code = table.Column<string>(maxLength: 20, nullable: true),
                    created_by = table.Column<long>(maxLength: 20, nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_specialization",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    created_by = table.Column<long>(maxLength: 50, nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_specialization", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_user",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    biodata_id = table.Column<long>(nullable: false),
                    role_id = table.Column<long>(nullable: false),
                    email = table.Column<string>(nullable: true),
                    password = table.Column<string>(maxLength: 100, nullable: true),
                    login_attempt = table.Column<int>(maxLength: 255, nullable: false),
                    is_locked = table.Column<bool>(nullable: false),
                    last_login = table.Column<DateTime>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_user", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "m_wallet_default_nominal",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nominal = table.Column<int>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_wallet_default_nominal", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_appointment",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: false),
                    doctor_office_id = table.Column<long>(nullable: false),
                    doctor_office_schedule_id = table.Column<long>(nullable: false),
                    doctor_office_treatment_id = table.Column<long>(nullable: false),
                    appointment_date = table.Column<DateTime>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_appointment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_appointment_cancellation",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    appointment_id = table.Column<long>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_appointment_cancellation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_appointment_done",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    appointment_id = table.Column<long>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_appointment_done", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_appointment_reschedule_history",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    appointment_id = table.Column<long>(nullable: true),
                    doctor_office_schedule_id = table.Column<long>(nullable: true),
                    doctor_office_treatment_id = table.Column<long>(nullable: true),
                    appointment_date = table.Column<DateTime>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_appointment_reschedule_history", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_courier_discount",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    courier_type_id = table.Column<long>(nullable: false),
                    value = table.Column<decimal>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_courier_discount", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_current_doctor_specialization",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_id = table.Column<long>(nullable: true),
                    specialization_id = table.Column<long>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_current_doctor_specialization", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_chat",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: true),
                    doctor_id = table.Column<long>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_chat", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_chat_history",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_chat_id = table.Column<long>(nullable: true),
                    chat_content = table.Column<string>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_chat_history", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_custom_nominal",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: true),
                    nominal = table.Column<int>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_custom_nominal", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_registered_card",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: true),
                    card_number = table.Column<string>(maxLength: 20, nullable: true),
                    validity_period = table.Column<DateTime>(nullable: true),
                    cvv = table.Column<string>(maxLength: 5, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_registered_card", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_va",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: true),
                    va_number = table.Column<string>(maxLength: 30, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_va", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_va_history",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_va_id = table.Column<long>(nullable: true),
                    amount = table.Column<decimal>(nullable: true),
                    expired_on = table.Column<DateTime>(nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_va_history", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_wallet",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: true),
                    pin = table.Column<string>(maxLength: 6, nullable: true),
                    balance = table.Column<decimal>(nullable: true),
                    barcode = table.Column<string>(maxLength: 50, nullable: true),
                    points = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_wallet", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_wallet_top_up",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_wallet_id = table.Column<long>(nullable: false),
                    amount = table.Column<decimal>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_wallet_top_up", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_customer_wallet_withdraw",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: false),
                    wallet_default_nominal_id = table.Column<long>(nullable: false),
                    amount = table.Column<int>(nullable: false),
                    bank_name = table.Column<string>(nullable: false),
                    account_number = table.Column<string>(nullable: false),
                    account_name = table.Column<string>(nullable: false),
                    otp = table.Column<int>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_customer_wallet_withdraw", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_office",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_id = table.Column<long>(nullable: false),
                    mdeical_facility_id = table.Column<long>(nullable: false),
                    specialization = table.Column<string>(maxLength: 100, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_office", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_office_schedule",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_id = table.Column<long>(nullable: false),
                    medical_facility_schedule_id = table.Column<long>(nullable: false),
                    slot = table.Column<int>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_office_schedule", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_office_treatment",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_treatment_id = table.Column<long>(nullable: false),
                    doctor_office_id = table.Column<long>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_office_treatment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_office_treatment_price",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_office_treatment_id = table.Column<long>(nullable: false),
                    price = table.Column<decimal>(nullable: false),
                    price_start_from = table.Column<decimal>(nullable: false),
                    price_until_from = table.Column<decimal>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_office_treatment_price", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_treatment",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_id = table.Column<long>(nullable: false),
                    name = table.Column<string>(maxLength: 50, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_treatment", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_medical_item_purchase",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customer_id = table.Column<long>(nullable: false),
                    payment_method_id = table.Column<long>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_medical_item_purchase", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_medical_item_purchase_detail",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    medical_item_purchase_id = table.Column<long>(nullable: false),
                    medical_item_id = table.Column<long>(nullable: false),
                    qty = table.Column<int>(nullable: false),
                    medical_facility_id = table.Column<long>(nullable: false),
                    courir_id = table.Column<long>(nullable: false),
                    sub_total = table.Column<decimal>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_medical_item_purchase_detail", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_reset_password",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    old_password = table.Column<string>(maxLength: 255, nullable: true),
                    new_password = table.Column<string>(maxLength: 255, nullable: true),
                    reset_for = table.Column<string>(maxLength: 20, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_reset_password", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_token",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    email = table.Column<string>(maxLength: 100, nullable: true),
                    user_id = table.Column<long>(nullable: false),
                    token = table.Column<string>(maxLength: 50, nullable: true),
                    expired_on = table.Column<DateTime>(nullable: false),
                    is_expired = table.Column<bool>(nullable: false),
                    used_for = table.Column<string>(maxLength: 20, nullable: true),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_token", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "t_treatment_discount",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    doctor_office_treatment_price_id = table.Column<long>(nullable: false),
                    value = table.Column<decimal>(nullable: false),
                    created_by = table.Column<long>(nullable: false),
                    created_on = table.Column<DateTime>(nullable: false),
                    modified_by = table.Column<long>(nullable: false),
                    modified_on = table.Column<DateTime>(nullable: false),
                    deleted_by = table.Column<long>(nullable: false),
                    deleted_on = table.Column<DateTime>(nullable: false),
                    is_delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_treatment_discount", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "m_bank");

            migrationBuilder.DropTable(
                name: "m_biodata");

            migrationBuilder.DropTable(
                name: "m_biodata_address");

            migrationBuilder.DropTable(
                name: "m_blood_group");

            migrationBuilder.DropTable(
                name: "m_courier");

            migrationBuilder.DropTable(
                name: "m_customer");

            migrationBuilder.DropTable(
                name: "m_customer_member");

            migrationBuilder.DropTable(
                name: "m_customer_relation");

            migrationBuilder.DropTable(
                name: "m_doctor");

            migrationBuilder.DropTable(
                name: "m_doctor_education");

            migrationBuilder.DropTable(
                name: "m_education_level");

            migrationBuilder.DropTable(
                name: "m_location");

            migrationBuilder.DropTable(
                name: "m_location_level");

            migrationBuilder.DropTable(
                name: "m_medical_facility");

            migrationBuilder.DropTable(
                name: "m_medical_facility_category");

            migrationBuilder.DropTable(
                name: "m_medical_facility_schedule");

            migrationBuilder.DropTable(
                name: "m_medical_item");

            migrationBuilder.DropTable(
                name: "m_medical_item_category");

            migrationBuilder.DropTable(
                name: "m_medical_item_segmentation");

            migrationBuilder.DropTable(
                name: "m_menu");

            migrationBuilder.DropTable(
                name: "m_menu_role");

            migrationBuilder.DropTable(
                name: "m_payment_method");

            migrationBuilder.DropTable(
                name: "m_role");

            migrationBuilder.DropTable(
                name: "m_specialization");

            migrationBuilder.DropTable(
                name: "m_user");

            migrationBuilder.DropTable(
                name: "m_wallet_default_nominal");

            migrationBuilder.DropTable(
                name: "t_appointment");

            migrationBuilder.DropTable(
                name: "t_appointment_cancellation");

            migrationBuilder.DropTable(
                name: "t_appointment_done");

            migrationBuilder.DropTable(
                name: "t_appointment_reschedule_history");

            migrationBuilder.DropTable(
                name: "t_courier_discount");

            migrationBuilder.DropTable(
                name: "t_current_doctor_specialization");

            migrationBuilder.DropTable(
                name: "t_customer_chat");

            migrationBuilder.DropTable(
                name: "t_customer_chat_history");

            migrationBuilder.DropTable(
                name: "t_customer_custom_nominal");

            migrationBuilder.DropTable(
                name: "t_customer_registered_card");

            migrationBuilder.DropTable(
                name: "t_customer_va");

            migrationBuilder.DropTable(
                name: "t_customer_va_history");

            migrationBuilder.DropTable(
                name: "t_customer_wallet");

            migrationBuilder.DropTable(
                name: "t_customer_wallet_top_up");

            migrationBuilder.DropTable(
                name: "t_customer_wallet_withdraw");

            migrationBuilder.DropTable(
                name: "t_doctor_office");

            migrationBuilder.DropTable(
                name: "t_doctor_office_schedule");

            migrationBuilder.DropTable(
                name: "t_doctor_office_treatment");

            migrationBuilder.DropTable(
                name: "t_doctor_office_treatment_price");

            migrationBuilder.DropTable(
                name: "t_doctor_treatment");

            migrationBuilder.DropTable(
                name: "t_medical_item_purchase");

            migrationBuilder.DropTable(
                name: "t_medical_item_purchase_detail");

            migrationBuilder.DropTable(
                name: "t_reset_password");

            migrationBuilder.DropTable(
                name: "t_token");

            migrationBuilder.DropTable(
                name: "t_treatment_discount");

            migrationBuilder.DropColumn(
                name: "is_delete",
                table: "m_admin");

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "m_admin",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
