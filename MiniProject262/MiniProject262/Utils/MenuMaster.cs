﻿using Microsoft.AspNetCore.Mvc;
using MiniProject262.Models;
using MiniProject262.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Utils
{
    public class MenuMaster
    {
        private readonly MiniProjectContext _miniproject;
        public MenuMaster(MiniProjectContext dbContext)
        {
            _miniproject = dbContext;
        }

        public IEnumerable<m_menu> GetMenuMaster()
        {
            return _miniproject.m_menu.AsEnumerable();

        }

        public IEnumerable<VMMenuRole> GetMenuMaster(int? role)
        {
            var result = (from a in _miniproject.m_menu
                          join b in _miniproject.m_menu_role
                          on a.id equals b.menu_id
						  //join c in _miniproject.m_role
						  //on b.role_id equals c.id
						  where b.role_id == role || b.role_id == null
						  select new VMMenuRole
                          {
                              m_menu_id = a.id,
                              m_menu_name = a.name,
                              //menu_role_role_id = c.id,
                              //role_name = c.name,
                              url = a.url,
                              m_menu_parent_id = a.parent_id,
                              big_icon = a.big_icon,
                              small_icon = a.small_icon
                          }).ToList();
            return result;
        }

        public IEnumerable<VMMenuRole> GetMenuOption(int? role)
        {
            var result = (from a in _miniproject.m_menu
                          join b in _miniproject.m_menu_role
                          on a.id equals b.menu_id
                          //join c in _miniproject.m_role
                          //on b.role_id equals c.id
                          where b.role_id == role || b.role_id == null
                          //b.role_id == role
                          select new VMMenuRole
                          {
                              m_menu_id = a.id,
                              m_menu_name = a.name,
                              //menu_role_role_id = c.id,
                              //role_name = c.name,
                              big_icon = a.big_icon,
                              url = a.url,
                              m_menu_parent_id = a.parent_id
                          }).ToList();
            return result;
        }
    }
}