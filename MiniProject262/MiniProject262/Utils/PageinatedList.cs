﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MiniProject262
{
    public class PageinatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }

        public PageinatedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            this.AddRange(items);
        }

        public static IEnumerable<SelectListItem> PageSizeList()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "5", Value = "5"},
                new SelectListItem{Text = "10", Value = "10"},
                new SelectListItem{Text = "20", Value = "20"},
            };
            return items;
        }
        public static IEnumerable<SelectListItem> PageSizeListHome()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "8", Value = "8"},
                new SelectListItem{Text = "12", Value = "12"},
                new SelectListItem{Text = "24", Value = "24"},
            };
            return items;
        }

      

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public static PageinatedList<T> CreateAsync(List<T> source, int pageIndex, int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            return new PageinatedList<T>(items, count, pageIndex, pageSize);
        }


    }
}
