﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMBank
    {
		public Int64 id { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string name { get; set; }
		[StringLength(10, ErrorMessage = "String exceed max value")]
		public string va_code { get; set; }
		public Int64 created_by { get; set; }

        public Int64 id_user { get; set; }
        public Int64 biodata_id { get; set; }
        public Int64 role_id { get; set; }


        public Int64 id_bio { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string fullname { get; set; }
      



    }
}
