﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMMenuRole
    {

        public Int64 m_menu_id { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string m_menu_name { get; set; }
        public Int64? m_menu_parent_id { get; set; }
        public string url { get; set; }
        public Int64? parent_id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string big_icon { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string small_icon { get; set; }

        public Int64 menrol_id { get; set; }
        public Int64? menu_id { get; set; }
        public Int64? role_id { get; set; }

        public Int64 menu_role_id { get; set; }
        public Int64? menu_role_menu_id { get; set; }


        public Int64? menu_role_role_id { get; set; }


        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string role_name { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string role_code { get; set; }

        public bool is_delete_menu_role { get; set; }

    }
}
