﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMCariDokter
    {
        public Int64 id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string name { get; set; }
        public Int64? parent_id { get; set; }
        public Int64? location_level_id { get; set; }
        public Int64? medical_facility_category_id { get; set; }
        public Int64? location_id { get; set; }
        public string full_address { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string email { get; set; }
        [StringLength(10, ErrorMessage = "String exceed max value")]
        public string phone_code { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string phone { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string fax { get; set; }
        public string abbreviation { get; set; }
        public Int64? doctor_id { get; set; }
        public Int64 mdeical_facility_id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        [Required]
        public string specialization { get; set; }
        public Int64? biodata_id { get; set; }
        [StringLength(50, ErrorMessage = "String exceed max value")]
        public string str { get; set; }
        public string fullname { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string mobile_phone { get; set; }
        public byte[] image { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string image_path { get; set; }
        public Int64? specialization_id { get; set; }
        public string namaLokasi { get; set; }
        public string spesialisasi { get; set; }
        public string tindakanMedis { get; set; }
        public Int64? specId { get; set; }
        public bool is_delete { get; set; }
        public Int64? treatId { get; set; }
        public string rumahSakit { get; set; }
        public Int64? DoctorOffice_id { get; set; }
        public Int64? DoctorBackup_id { get; set; }
        public int pengalaman { get; set; }
        public DateTime created_on { get; set; }
    }
}
