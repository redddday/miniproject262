﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMPasien
    {
        public Int64 id_customer_member { get; set; }
        public Int64? parent_biodata_id { get; set; }
        public Int64? customer_id { get; set; }
        public Int64? customer_relation_id { get; set; }

        public string relation_name { get; set; }

        public Int64? biodata_id { get; set; }
        public DateTime? dob { get; set; }
        [StringLength(1, ErrorMessage = "String exceed max value")]
        public string gender { get; set; }
        public Int64? blood_group_id { get; set; }

        [StringLength(5, ErrorMessage = "String exceed max value")]
        public string rhesus_type { get; set; }
        public decimal? height { get; set; }
        public decimal? weight { get; set; }

        public string code { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string description { get; set; }

        public string fullname { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string mobile_phone { get; set; }
        public byte[] image { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string image_path { get; set; }

        public DateTime created_on_biodata { get; set; }
        public DateTime created_on_customer { get; set; }
        public DateTime created_on_cusmem { get; set; }

        public Int64 created_by_biodata { get; set; }
        public Int64 created_by_customer { get; set; }
        public Int64 created_by_cusmem { get; set; }

        public Int64? modified_by_biodata { get; set; }
        public Int64? modified_by_customer { get; set; }
        public Int64? modified_by_cusmem { get; set; }
        public DateTime? modified_on_cusmem { get; set; }
        public DateTime? modified_on_biodata { get; set; }
        public DateTime? modified_on_customer { get; set; }

        
        public int Age { get;set;}

        public Int64? chat { get; set; }
    }
}
