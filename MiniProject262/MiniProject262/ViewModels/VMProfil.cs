﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMProfil
    {
        public Int64 id { get; set; }
        public Int64 biodata_id { get; set; }
        public Int64 customer_id { get; set; }
        public Int64 role_id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string email { get; set; }

        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string password { get; set; }
        public int login_attempt { get; set; }
        public bool is_locked { get; set; }
        public DateTime last_login { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string name { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string code { get; set; }

        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string fullname { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string mobile_phone { get; set; }
        public byte[] image { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string image_path { get; set; }
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dob { get; set; }
        public string otp { get; set; }
        public string old_password { get; set; }
        public string new_password { get; set; }
        public string reset_for { get; set; }


        public int id_doctor { get; set; }
        public int id_mdoctor_edu { get; set; }
        public string institution_name { get; set; }
        public string major { get; set; }
        public string start_year { get; set; }
        public string end_year { get; set; }
        public bool? is_last_edu { get; set; }
        public Int64 id_edu_level { get; set; }
        public string name_edu_lv { get; set; }
        public Int64 id_medic_facility { get; set; }
        public string name_medic_facility { get; set; }
        public string day_mf_schedule { get; set; }
        public string code_role { get; set; }
        public string specialization { get; set; }
        public string doctor_office_specialization { get; set; }



        public Int64 created_by { get; set; }
        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
