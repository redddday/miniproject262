﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMChat
    {
        public Int64 id { get; set; }
        public Int64? customer_id { get; set; }
        public Int64? doctor_id { get; set; }

        public Int64 chathistoryid { get; set; }
        public Int64? customer_chat_id { get; set; }
        public string chat_content { get; set; }
    }
}
