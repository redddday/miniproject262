﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
	public class VMCariObat
	{
		public Int64 id { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string name { get; set; }
		public Int64? medical_item_category_id { get; set; }
		public string composition { get; set; }
		public Int64? medical_item_segmentation_id { get; set; }

		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string manufacturer { get; set; }
		public string indication { get; set; }
		public string dosage { get; set; }
		public string directions { get; set; }
		public string contraindication { get; set; }
		public string caution { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string packaging { get; set; }
		public Int64? price_max { get; set; }
		public Int64? price_min { get; set; }
		public byte[] image { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string image_path { get; set; }
		public Int64 med_item_cat_id { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string med_item_cat_name { get; set; }
		public Int64 med_item_seg_id { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string med_item_seg_name { get; set; }
		public Int64 created_by { get; set; }
		public DateTime created_on { get; set; }
		public Int64? modified_by { get; set; }
		public DateTime? modified_on { get; set; }
		public Int64? deleted_by { get; set; }
		public DateTime? deleted_on { get; set; }

		[DefaultValue(false)]
		public bool is_delete { get; set; }
	}
}
