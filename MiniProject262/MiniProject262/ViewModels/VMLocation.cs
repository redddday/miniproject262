﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMLocation
    {
        public Int64 id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string nameLocation { get; set; }
        public string nameLocationLevel { get; set; }
        public Int64? parent_id { get; set; }
        public Int64? location_level_id { get; set; }
        public string abbreviation { get; set; }
        public string wilayah { get; set; }
        public string allwil { get; set; }

        public string parentname { get; set; }

        public Int64? locationlevelparent { get; set; }
        public string abbreviationparent { get; set; }

        public Int64 created_by { get; set; }
        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
