﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMUser
    {
        public Int64 id { get; set; }
        public Int64 biodata_id { get; set; }
        public Int64 dokter_id { get; set; }

        public Int64 role_id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string email { get; set; }

        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string password { get; set; }
        public int login_attempt { get; set; }
        public bool is_locked { get; set; }
        public DateTime last_login { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string name { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string code { get; set; }

        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string fullname { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string mobile_phone { get; set; }
        public byte[] image { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string image_path { get; set; }

        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string old_password { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string new_password { get; set; }
        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string reset_for { get; set; }

        public int? nominal { get; set; }


        public Int64 created_by { get; set; }
        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
        public int? otp { get; set; }
    }
}
