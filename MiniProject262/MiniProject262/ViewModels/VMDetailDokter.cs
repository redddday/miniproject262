﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMDetailDokter
    {
        public Int64 id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string name { get; set; }
        public Int64? parent_id { get; set; }
        public Int64? location_level_id { get; set; }
        public Int64? medical_facility_category_id { get; set; }
        public Int64? location_id { get; set; }
        public string full_address { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string email { get; set; }
        [StringLength(10, ErrorMessage = "String exceed max value")]
        public string phone_code { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string phone { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string fax { get; set; }
        public string abbreviation { get; set; }
        public Int64? doctor_id { get; set; }
        public Int64 mdeical_facility_id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        [Required]
        public string specialization { get; set; }
        public Int64? biodata_id { get; set; }
        [StringLength(50, ErrorMessage = "String exceed max value")]
        public string str { get; set; }
        public string fullname { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string mobile_phone { get; set; }
        public byte[] image { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string image_path { get; set; }
        public Int64? specialization_id { get; set; }
        public string namaLokasi { get; set; }
        public string spesialisasi { get; set; }
        public string tindakanMedis { get; set; }
        public Int64? specId { get; set; }
        public bool is_delete { get; set; }
        public Int64? treatId { get; set; }
        public string rumahSakit { get; set; }
        public Int64? DoctorOffice_id { get; set; }
        public Int64? DoctorBackup_id { get; set; }
        public int pengalaman { get; set; }
        public DateTime created_on { get; set; }
        public string day { get; set; }
        [StringLength(10, ErrorMessage = "String exceed max value")]
        public string time_schedule_start { get; set; }
        [StringLength(10, ErrorMessage = "String exceed max value")]
        public string time_schedule_end { get; set; }

        public Int64 created_by { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        public int? slot { get; set; }
        public decimal? price { get; set; }
        public decimal? price_start_from { get; set; }
        public decimal? price_until_from { get; set; }
        public Int64? education_level_id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string institution_name { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string major { get; set; }
        [StringLength(4, ErrorMessage = "String exceed max value")]
        public string start_year { get; set; }
        [StringLength(4, ErrorMessage = "String exceed max value")]
        public string end_year { get; set; }
        [DefaultValue(false)]
        public bool? is_last_education { get; set; }

        public int tahun { get; set; }
        public int endingYear { get; set; }
        public int jamAwal { get; set; }
        public int jamAkhir { get; set; }
        public TimeSpan jamAwal1 { get; set; }
        public TimeSpan jamAkhir1 { get; set; }
    }
}
