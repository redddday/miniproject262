﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMProfileDokter
    {
        public int id_biodata { get; set; }
        public string fullname { get; set; }
        public int id_doctor { get; set; }
        public int id_mdoctor_edu { get; set; }
        public string institution_name { get; set; }
        public string major { get; set; }
        public DateTime? start_year { get; set; }
        public DateTime? end_year { get; set; }
        public bool is_last_edu { get; set; }
        public int id_edu_level { get; set; }
        public string name_edu_lv { get; set; }
        public int id_medic_facility { get; set; }
        public string name_medic_facility { get; set; }
        public string day_mf_schedule { get; set; }
        public string code_role { get; set; }
        public string specialization { get; set; }
        public string doctor_office_specialization { get; set; }
            


    }
}
