﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.ViewModels
{
    public class VMBlood
    {
		public Int64 id { get; set; }
		[StringLength(5, ErrorMessage = "String exceed max value")]
		public string code { get; set; }
		[StringLength(255, ErrorMessage = "String exceed max value")]
		public string description { get; set; }
		public Int64 created_by { get; set; }
		public bool is_delete { get; set; }
		public Int64 id_user { get; set; }
		public Int64 biodata_id { get; set; }

		public Int64 id_biodata { get; set; }
		[StringLength(255, ErrorMessage = "String exceed max value")]
		public string fullname { get; set; }
		
	}
}
