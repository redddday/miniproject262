﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MiniProject262.ViewModels
{
	public class VMJanji
	{
        //t_appointment
        public Int64 id { get; set; }
        public Int64? customer_id { get; set; }
        public Int64? doctor_office_id { get; set; }
        public Int64? doctor_office_schedule_id { get; set; }
        public Int64? doctor_office_treatment_id { get; set; }
        public DateTime? appointment_date { get; set; }

		//m_customer
		public Int64 id_cust { get; set; }
		public Int64? biodata_id { get; set; }
		public DateTime? dob { get; set; }
		[StringLength(1, ErrorMessage = "String exceed max value")]
		public string gender { get; set; }
		public Int64? blood_group_id { get; set; }
		[StringLength(5, ErrorMessage = "String exceed max value")]
		public string rhesus_type { get; set; }
		public decimal? height { get; set; }
		public decimal? weight { get; set; }

		//m_biodata
		public Int64 id_bio { get; set; }
		[StringLength(255, ErrorMessage = "String exceed max value")]
		public string fullname { get; set; }
		[StringLength(15, ErrorMessage = "String exceed max value")]
		public string mobile_phone { get; set; }
		public byte[] image { get; set; }
		[StringLength(255, ErrorMessage = "String exceed max value")]
		public string image_path { get; set; }

		//m_blood_group
		public Int64 id_blood_group { get; set; }
		[StringLength(5, ErrorMessage = "String exceed max value")]
		public string code { get; set; }
		[StringLength(255, ErrorMessage = "String exceed max value")]
		public string description { get; set; }

		//t_doctor_office
		public Int64 id_doc_office { get; set; }
		public Int64? doctor_id { get; set; }
		public Int64 mdeical_facility_id { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string specialization { get; set; }

		//m_doctor
		public Int64 id_doc { get; set; }
		public Int64? biodata_id_doc { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string str { get; set; }

		//m_specialization
		public Int64 id_spec { get; set; }
		public Int64? biodata_id_spec { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string str_spec { get; set; }

		//m_medical_facility
		public Int64 id_med_fac { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string name { get; set; }
		public Int64? medical_facility_category_id { get; set; }
		public Int64? location_id { get; set; }
		public string full_address { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string email { get; set; }
		[StringLength(10, ErrorMessage = "String exceed max value")]
		public string phone_code { get; set; }
		[StringLength(15, ErrorMessage = "String exceed max value")]
		public string phone { get; set; }
		[StringLength(15, ErrorMessage = "String exceed max value")]
		public string fax { get; set; }

		//m_medical_facility_category
		public Int64 id_med_fac_cat { get; set; }

		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string name_med_fac_cat { get; set; }

		//m_location
		public Int64 id_loc { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string name_loc { get; set; }
		public Int64? parent_id { get; set; }
		public Int64? location_level_id { get; set; }

		//m_customer_member
		public Int64 id_cust_mem { get; set; }
		public Int64? parent_biodata_id { get; set; }
		public Int64? customer_id_cust_mem { get; set; }
		public Int64? customer_relation_id { get; set; }

		//m_location_level
		public Int64 id_loc_lev { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string name_loc_lev { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string abbreviation { get; set; }

		//m_customer_relation
		public Int64 id_cust_rel { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string name_cust_rel { get; set; }

		//t_doctor_office_schedule
		public Int64 id_doc_off_sched { get; set; }
		public Int64? doctor_id_doc_off_sched { get; set; }
		public Int64? medical_facility_schedule_id { get; set; }
		public int? slot { get; set; }

		//t_doctor_office_treatment
		public Int64 id_doc_off_treat { get; set; }
		public Int64? doctor_treatment_id { get; set; }
		public Int64? doctor_office_id_doc_off_treat { get; set; }

		//t_doctor_treatment
		public Int64 id_doc_treat { get; set; }
		public Int64? doctor_id_doc_treat { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string name_doc_treat { get; set; }

		//m_medical_facility_schedule
		public Int64 id_med_facc_sched { get; set; }
		public Int64? medical_facility_id { get; set; }
		[StringLength(10, ErrorMessage = "String exceed max value")]
		public string day { get; set; }
		[StringLength(10, ErrorMessage = "String exceed max value")]
		public string time_schedule_start { get; set; }
		[StringLength(10, ErrorMessage = "String exceed max value")]
		public string time_schedule_end { get; set; }

		public Int64 created_by { get; set; }
		public DateTime created_on { get; set; }
		public Int64? modified_by { get; set; }
		public DateTime? modified_on { get; set; }
		public Int64? deleted_by { get; set; }
		public DateTime? deleted_on { get; set; }
		[DefaultValue(false)]
		public bool is_delete { get; set; }

		//t_current_doctor_specialization
		public Int64 id_cur_spec { get; set; }
		public Int64? doctor_id_cur_spec { get; set; }
		public Int64? specialization_id { get; set; }
	}
}