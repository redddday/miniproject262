﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class m_user
    {
        public Int64 id { get; set; }
        public Int64 biodata_id { get; set; }
        public Int64 role_id { get; set; }

        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string email { get; set; }

        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string password { get; set; }
       
        public  int login_attempt { get; set; }
        public bool is_locked { get; set; }
        public DateTime last_login { get; set; }

        public Int64 created_by { get; set; }

        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
