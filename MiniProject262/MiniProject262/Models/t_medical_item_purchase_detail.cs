﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class t_medical_item_purchase_detail
    {
		public Int64 id { get; set; }
		public Int64? medical_item_purchase_id { get; set; }
		public Int64? medical_item_id { get; set; }
		public int? qty { get; set; }
		public Int64? medical_facility_id { get; set; }
		public Int64? courir_id { get; set; }
		public decimal? sub_total{ get; set; }

		public Int64 created_by { get; set; }

		public DateTime created_on { get; set; }
		public Int64? modified_by { get; set; }
		public DateTime? modified_on { get; set; }
		public Int64? deleted_by { get; set; }
		public DateTime? deleted_on { get; set; }
		[DefaultValue(false)]
		public bool is_delete { get; set; }
	}
}
