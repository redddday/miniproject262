﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class t_treatment_discount
    {
        public Int64 id { get; set; }
        public Int64? doctor_office_treatment_price_id { get; set; }
        public decimal? value { get; set; }
        public Int64 created_by { get; set; }

        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
