﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
	public class m_biodata_address
	{
		public Int64 id { get; set; }
		public Int64? biodata_id { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string label { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string recipient { get; set; }
		[StringLength(15, ErrorMessage = "String exceed max value")]
		public string recipient_phone_number { get; set; }
		public Int64? location_id { get; set; }
		[StringLength(10, ErrorMessage = "String exceed max value")]
		public string postal_code { get; set; }
		public string address { get; set; }
		public Int64 created_by { get; set; }

		public DateTime created_on { get; set; }
		public Int64? modified_by { get; set; }
		public DateTime? modified_on { get; set; }
		public Int64? deleted_by { get; set; }
		public DateTime? deleted_on { get; set; }
		[DefaultValue(false)]
		public bool is_delete { get; set; }
	}
}
