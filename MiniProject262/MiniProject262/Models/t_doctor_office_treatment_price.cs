﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class t_doctor_office_treatment_price
    {
		public Int64 id { get; set; }
		public Int64? doctor_office_treatment_id { get; set; }
		public decimal? price { get; set; }
		public decimal? price_start_from { get; set; }
		public decimal? price_until_from { get; set; }

		public Int64 created_by { get; set; }

		public DateTime created_on { get; set; }
		public Int64? modified_by { get; set; }
		public DateTime? modified_on { get; set; }
		public Int64? deleted_by { get; set; }
		public DateTime? deleted_on { get; set; }
		[DefaultValue(false)]
		public bool is_delete { get; set; }
	}
}
