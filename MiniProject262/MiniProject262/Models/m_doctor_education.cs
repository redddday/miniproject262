﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class m_doctor_education
    {
        public Int64 id { get; set; }
        public Int64? doctor_id { get; set; }
        public Int64? education_level_id { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string institution_name { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string major { get; set; }
        [StringLength(4, ErrorMessage = "String exceed max value")]
        public string start_year { get; set; }
        [StringLength(4, ErrorMessage = "String exceed max value")]
        public string end_year { get; set; }
        [DefaultValue(false)]
        public bool? is_last_education { get; set; }
        public Int64 created_by { get; set; }

        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
