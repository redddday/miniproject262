﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MiniProject262.ViewModels;

namespace MiniProject262.Models
{
	public class MiniProjectContext : DbContext
	{
		private readonly DbContextOptions _options;

		public MiniProjectContext(DbContextOptions<MiniProjectContext> options) : base(options)
		{
			_options = options;
		}

		public virtual DbSet<m_admin> m_admin { get; set; }
		public virtual DbSet<m_bank> m_bank { get; set; }
		public virtual DbSet<m_biodata> m_biodata { get; set; }
		public virtual DbSet<m_biodata_address> m_biodata_address { get; set; }
		public virtual DbSet<m_blood_group> m_blood_group { get; set; }
		public virtual DbSet<m_courier> m_courier { get; set; }
		public virtual DbSet<m_customer> m_customer { get; set; }
		public virtual DbSet<m_customer_member> m_customer_member { get; set; }
		public virtual DbSet<m_customer_relation> m_customer_relation { get; set; }
		public virtual DbSet<m_doctor> m_doctor { get; set; }
		public virtual DbSet<m_doctor_education> m_doctor_education { get; set; }
		public virtual DbSet<m_education_level> m_education_level { get; set; }
		public virtual DbSet<m_location> m_location { get; set; }
		public virtual DbSet<m_location_level> m_location_level { get; set; }
		public virtual DbSet<m_medical_facility> m_medical_facility { get; set; }
		public virtual DbSet<m_medical_facility_category> m_medical_facility_category { get; set; }
		public virtual DbSet<m_medical_facility_schedule> m_medical_facility_schedule { get; set; }
		public virtual DbSet<m_medical_item> m_medical_item { get; set; }
		public virtual DbSet<m_medical_item_category> m_medical_item_category { get; set; }
		public virtual DbSet<m_medical_item_segmentation> m_medical_item_segmentation { get; set; }
		public virtual DbSet<m_menu> m_menu { get; set; }
		public virtual DbSet<m_menu_role> m_menu_role { get; set; }
		public virtual DbSet<m_payment_method> m_payment_method { get; set; }
		public virtual DbSet<m_role> m_role { get; set; }
		public virtual DbSet<m_specialization> m_specialization { get; set; }
		public virtual DbSet<m_user> m_user { get; set; }
		public virtual DbSet<m_wallet_default_nominal> m_wallet_default_nominal { get; set; }
		public virtual DbSet<t_appointment> t_appointment { get; set; }
		public virtual DbSet<t_appointment_cancellation> t_appointment_cancellation { get; set; }
		public virtual DbSet<t_appointment_done> t_appointment_done { get; set; }
		public virtual DbSet<t_appointment_reschedule_history> t_appointment_reschedule_history { get; set; }
		public virtual DbSet<t_courier_discount> t_courier_discount { get; set; }
		public virtual DbSet<t_current_doctor_specialization> t_current_doctor_specialization { get; set; }
		public virtual DbSet<t_customer_chat> t_customer_chat { get; set; }
		public virtual DbSet<t_customer_chat_history> t_customer_chat_history { get; set; }
		public virtual DbSet<t_customer_custom_nominal> t_customer_custom_nominal { get; set; }
		public virtual DbSet<t_customer_registered_card> t_customer_registered_card { get; set; }
		public virtual DbSet<t_customer_va> t_customer_va { get; set; }
		public virtual DbSet<t_customer_va_history> t_customer_va_history { get; set; }
		public virtual DbSet<t_customer_wallet> t_customer_wallet { get; set; }
		public virtual DbSet<t_customer_wallet_top_up> t_customer_wallet_top_up { get; set; }
		public virtual DbSet<t_customer_wallet_withdraw> t_customer_wallet_withdraw { get; set; }
		public virtual DbSet<t_doctor_office> t_doctor_office { get; set; }
		public virtual DbSet<t_doctor_office_schedule> t_doctor_office_schedule { get; set; }
		public virtual DbSet<t_doctor_office_treatment> t_doctor_office_treatment { get; set; }
		public virtual DbSet<t_doctor_office_treatment_price> t_doctor_office_treatment_price { get; set; }
		public virtual DbSet<t_doctor_treatment> t_doctor_treatment { get; set; }
		public virtual DbSet<t_medical_item_purchase> t_medical_item_purchase { get; set; }
		public virtual DbSet<t_medical_item_purchase_detail> t_medical_item_purchase_detail { get; set; }
		public virtual DbSet<t_reset_password> t_reset_password { get; set; }
		public virtual DbSet<t_token> t_token { get; set; }
		public virtual DbSet<t_treatment_discount> t_treatment_discount { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<m_admin>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_bank>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_biodata>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_biodata_address>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_blood_group>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_courier>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_customer>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_customer_member>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_customer_relation>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_doctor>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_doctor_education>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_education_level>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_location>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_location_level>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_medical_facility>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_medical_facility_category>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_medical_facility_schedule>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_medical_item>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_medical_item_category>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_medical_item_segmentation>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_menu>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_menu_role>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_payment_method>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_role>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_specialization>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_user>().HasKey(e => new { e.id });
			modelBuilder.Entity<m_wallet_default_nominal>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_appointment>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_appointment_cancellation>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_appointment_done>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_appointment_reschedule_history>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_courier_discount>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_current_doctor_specialization>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_chat>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_chat_history>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_custom_nominal>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_registered_card>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_va>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_va_history>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_wallet>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_wallet_top_up>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_customer_wallet_withdraw>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_doctor_office>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_doctor_office_schedule>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_doctor_office_treatment>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_doctor_office_treatment_price>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_doctor_treatment>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_medical_item_purchase>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_medical_item_purchase_detail>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_reset_password>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_token>().HasKey(e => new { e.id });
			modelBuilder.Entity<t_treatment_discount>().HasKey(e => new { e.id });
		}

		public DbSet<MiniProject262.ViewModels.VMCariDokter> VMCariDokter { get; set; }
		public DbSet<MiniProject262.ViewModels.VMLocation> VMLocation { get; set; }

		public DbSet<MiniProject262.ViewModels.VMDetailDokter> VMDetailDokter { get; set; }
	}
}
