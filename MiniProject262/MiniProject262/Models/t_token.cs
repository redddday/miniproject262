﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class t_token
    {
		public Int64 id { get; set; }
		[StringLength(100, ErrorMessage = "String exceed max value")]
		public string email { get; set; }
		public Int64? user_id { get; set; }
		[StringLength(50, ErrorMessage = "String exceed max value")]
		public string token { get; set; }
		public DateTime? expired_on { get; set; }
		public bool? is_expired { get; set; }
		[StringLength(20, ErrorMessage = "String exceed max value")]
		public string used_for { get; set; }
		public Int64 created_by { get; set; }

		public DateTime created_on { get; set; }
		public Int64? modified_by { get; set; }
		public DateTime? modified_on { get; set; }
		public Int64? deleted_by { get; set; }
		public DateTime? deleted_on { get; set; }
		[DefaultValue(false)]
		public bool is_delete { get; set; }
	}
}
