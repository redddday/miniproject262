﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class m_medical_facility
    {
        public Int64 id { get; set; }
        [StringLength(50, ErrorMessage = "String exceed max value")]
        public string name { get; set; }
        public Int64? medical_facility_category_id { get; set; }
        public Int64? location_id { get; set; }
        public string full_address { get; set; }
        [StringLength(100, ErrorMessage = "String exceed max value")]
        public string email { get; set; }
        [StringLength(10, ErrorMessage = "String exceed max value")]
        public string phone_code { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string phone { get; set; }
        [StringLength(15, ErrorMessage = "String exceed max value")]
        public string fax { get; set; }

        public Int64 created_by { get; set; }

        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
