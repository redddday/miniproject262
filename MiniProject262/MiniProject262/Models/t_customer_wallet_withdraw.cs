﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class t_customer_wallet_withdraw
    {
        public Int64 id { get; set; }
        public Int64? customer_id { get; set; }
        public Int64? wallet_default_nominal_id { get; set; }
        public int amount { get; set; }
        [StringLength(50, ErrorMessage = "String exceed max value")]
        public string bank_name { get; set; }
        [StringLength(50, ErrorMessage = "String exceed max value")]
        public string account_number { get; set; }
        [StringLength(255, ErrorMessage = "String exceed max value")]
        public string account_name { get; set; }
        public int otp { get; set; }

        public Int64 created_by { get; set; }

        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
