﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
    public class m_role
    {
        public Int64 id { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string name { get; set; }

        [StringLength(20, ErrorMessage = "String exceed max value")]
        public string code { get; set; }
        
        public Int64 created_by { get; set; }

        public DateTime created_on { get; set; }
        public Int64? modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public Int64? deleted_by { get; set; }
        public DateTime? deleted_on { get; set; }
        [DefaultValue(false)]
        public bool is_delete { get; set; }
    }
}
