﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Models
{
	public class m_biodata
	{
		public Int64 id { get; set; }
		[StringLength(255, ErrorMessage = "String exceed max value")]
		public string fullname { get; set; }
		[StringLength(15, ErrorMessage = "String exceed max value")]
		public string mobile_phone { get; set; }
		public byte[] image { get; set; }
		[StringLength(255, ErrorMessage = "String exceed max value")]
		public string image_path { get; set; }
		public Int64 created_by { get; set; }

		public DateTime created_on { get; set; }
		public Int64? modified_by { get; set; }
		public DateTime? modified_on { get; set; }
		public Int64? deleted_by { get; set; }
		public DateTime? deleted_on { get; set; }
		[DefaultValue(false)]
		public bool is_delete { get; set; }
	}
}
