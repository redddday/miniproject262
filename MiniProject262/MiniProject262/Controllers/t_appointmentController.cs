﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;

namespace MiniProject262.Controllers
{
	public class t_appointmentController : Controller
	{
		private readonly MiniProjectContext _context;

		public t_appointmentController(MiniProjectContext context)
		{
			_context = context;
		}

		// GET: t_appointment
		public async Task<IActionResult> Index(int id)
		{
			VMJanji dokters = await
					   (
					   from doc in _context.m_doctor
					   join bio in _context.m_biodata
					   on doc.biodata_id equals bio.id

					   join doc_spec in _context.t_current_doctor_specialization
					   on doc.id equals doc_spec.doctor_id

					   join spec in _context.m_specialization
					   on doc_spec.specialization_id equals spec.id

					   select new VMJanji
					   {
						   doctor_id = doc.id,
						   fullname = bio.fullname,
						   image = bio.image,
						   image_path = bio.image_path,
						   specialization = spec.name
					   }).FirstOrDefaultAsync(m => m.doctor_id == id);

			var pengalaman =
						(
						from medfac in _context.m_medical_facility

						join docof in _context.t_doctor_office
						on medfac.id equals docof.mdeical_facility_id

						join doc in _context.m_doctor
						on docof.doctor_id equals doc.id

						join bio in _context.m_biodata
						on doc.biodata_id equals bio.id

						join tdos in _context.t_doctor_office_schedule
						on doc.id equals tdos.doctor_id

						join medfacsche in _context.m_medical_facility_schedule
						on medfac.id equals medfacsche.medical_facility_id

						where tdos.doctor_id == doc.id && doc.is_delete == false
						orderby tdos.created_by descending

						select new VMDetailDokter
						{
							 id = medfac.id,
							 doctor_id = doc.id,
							 tahun = CalculateAge(tdos.created_on),
							 created_on = tdos.created_on
						}).ToList();
			ViewBag.pengalaman = pengalaman.OrderByDescending(a => a.tahun);

			return View(dokters);
		}

		public async Task<IActionResult> IndexDetails(int id)
		{
			VMJanji dokters = await
					   (
					   from doc in _context.m_doctor
					   join bio in _context.m_biodata
					   on doc.biodata_id equals bio.id

					   join doc_spec in _context.t_current_doctor_specialization
					   on doc.id equals doc_spec.doctor_id

					   join spec in _context.m_specialization
					   on doc_spec.specialization_id equals spec.id

					   select new VMJanji
					   {
						   doctor_id = doc.id,
						   fullname = bio.fullname,
						   image = bio.image,
						   image_path = bio.image_path,
						   specialization = spec.name
					   }).FirstOrDefaultAsync(m => m.doctor_id == id);

			var pengalaman =
						(
						from medfac in _context.m_medical_facility

						join docof in _context.t_doctor_office
						on medfac.id equals docof.mdeical_facility_id

						join doc in _context.m_doctor
						on docof.doctor_id equals doc.id

						join bio in _context.m_biodata
						on doc.biodata_id equals bio.id

						join tdos in _context.t_doctor_office_schedule
						on doc.id equals tdos.doctor_id

						join medfacsche in _context.m_medical_facility_schedule
						on medfac.id equals medfacsche.medical_facility_id

						where tdos.doctor_id == doc.id && doc.is_delete == false
						orderby tdos.created_by descending

						select new VMDetailDokter
						{
							id = medfac.id,
							doctor_id = doc.id,
							tahun = CalculateAge(tdos.created_on),
							created_on = tdos.created_on
						}).ToList();
			ViewBag.pengalaman = pengalaman.OrderByDescending(a => a.tahun);

			return View(dokters);
		}

		// GET: t_appointment/Details/5
		public async Task<IActionResult> Details(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var t_appointment = await _context.t_appointment
				.FirstOrDefaultAsync(m => m.id == id);
			if (t_appointment == null)
			{
				return NotFound();
			}

			return View(t_appointment);
		}

		// GET: t_appointment/Create
		public async Task<IActionResult> Create()
		{
			return View();
		}

		// POST: t_appointment/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create(t_appointment appointments)
		{
			var date = Convert.ToString(appointments.appointment_date);
			DateTime hariFixx = Convert.ToDateTime(date);

			CultureInfo indo = new CultureInfo("id-ID");

			var namaHari = hariFixx.DayOfWeek;
			string hariTrans = indo.DateTimeFormat.DayNames[(int)namaHari];

			List<t_appointment> appointed = await
				(
				from c in _context.t_appointment

				where c.appointment_date == DateTime.Parse(date)

				select c
				).ToListAsync();

			var count = appointed.Count();

			var slotAvail = await
				(
				from doc_off_sched in _context.t_doctor_office_schedule
				join doc in _context.m_doctor
				on doc_off_sched.doctor_id equals doc.id

				join med_fac_sched in _context.m_medical_facility_schedule
				on doc_off_sched.medical_facility_schedule_id equals med_fac_sched.medical_facility_id

				join med_fac in _context.m_medical_facility
				on med_fac_sched.medical_facility_id equals med_fac.id
				where med_fac.id == appointments.doctor_office_id && med_fac_sched.day == hariTrans

				select new VMJanji
				{
					slot = doc_off_sched.slot,
				}).FirstOrDefaultAsync();

			if (count < slotAvail.slot)
			{
				_context.Add(appointments);
				await _context.SaveChangesAsync();
				return Json(new { status = "success" });
			}
			else
			{
				return Json(new { status = "error" });
			}

			//t_appointment newApp = t_appoint;
			//List<VMJanji> listJanji = await GetSchedule(newApp.);

			//if (ModelState.IsValid)
			//{
			//	_context.Add(newApp);
			//	await _context.SaveChangesAsync();
			//	return Json(new { status = "success" });
			//	//return RedirectToAction("Index", "Home");
			//}
			//return Json(new { status = "error" });
		}

		// GET: t_appointment/Edit/5
		public async Task<IActionResult> Edit(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var t_appointment = await _context.t_appointment.FindAsync(id);
			if (t_appointment == null)
			{
				return NotFound();
			}
			return View(t_appointment);
		}

		// POST: t_appointment/Edit/5
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(long id, [Bind("id,customer_id,doctor_office_id,doctor_office_schedule_id,doctor_office_treatment_id,appointment_date,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] t_appointment t_appointment)
		{
			if (id != t_appointment.id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(t_appointment);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!t_appointmentExists(t_appointment.id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(t_appointment);
		}

		// GET: t_appointment/Delete/5
		public async Task<IActionResult> Delete(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var t_appointment = await _context.t_appointment
				.FirstOrDefaultAsync(m => m.id == id);
			if (t_appointment == null)
			{
				return NotFound();
			}

			return View(t_appointment);
		}

		// POST: t_appointment/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(long id)
		{
			var t_appointment = await _context.t_appointment.FindAsync(id);
			_context.t_appointment.Remove(t_appointment);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool t_appointmentExists(long id)
		{
			return _context.t_appointment.Any(e => e.id == id);
		}

		public async Task<List<VMJanji>> listDokter()
		{
			List<VMJanji> dokters = await
					   (
					   from doc in _context.m_doctor
					   join bio in _context.m_biodata
					   on doc.biodata_id equals bio.id

					   join doc_spec in _context.t_current_doctor_specialization
					   on doc.id equals doc_spec.doctor_id

					   join spec in _context.m_specialization
					   on doc_spec.specialization_id equals spec.id

					   select new VMJanji
					   {
						   doctor_id = doc.id,
						   fullname = bio.fullname + ", " + doc.str,
						   image = bio.image,
						   image_path = bio.image_path
					   }).ToListAsync();

			return dokters;
		}

		public async Task<JsonResult> GetKeluarga(int id)
		{
			List<VMJanji> keluarga = await
				(
				from bio in _context.m_biodata
				join cust in _context.m_customer
				on bio.id equals cust.biodata_id

				join cust_mem in _context.m_customer_member
				on cust.id equals cust_mem.customer_id

				join cust_rel in _context.m_customer_relation
				on cust_mem.customer_relation_id equals cust_rel.id

				join user in _context.m_user
				on cust_mem.parent_biodata_id equals user.biodata_id
				where user.id == id
				select new VMJanji
				{
					id = bio.id,
					fullname = bio.fullname + ", " + cust_rel.name
				}).ToListAsync();

			keluarga.Insert(0, new VMJanji { id = 0, fullname = "--Pilih Pasien--" });

			return Json(keluarga);
		}

		public async Task<JsonResult> GetFaskes(int id)
		{
			List<VMJanji> faskes = await
				(
				from doc_off in _context.t_doctor_office
				join med_fac in _context.m_medical_facility
				on doc_off.mdeical_facility_id equals med_fac.id

				join doc in _context.m_doctor
				on doc_off.doctor_id equals doc.id

				join med_fac_cat in _context.m_medical_facility_category
				on med_fac.medical_facility_category_id equals med_fac_cat.id

				join loc in _context.m_location
				on med_fac.location_id equals loc.id

				join loc_lev in _context.m_location_level
				on loc.location_level_id equals loc_lev.id
				where doc_off.doctor_id == id

				select new VMJanji
				{
					id = med_fac.id,
					name = med_fac.name
				}).ToListAsync();

			faskes.Insert(0, new VMJanji { id = 0, name = "--Pilih Faskes--" });
			return Json(faskes);
		}

		public async Task<JsonResult> GetFaskesDetails(int id, int rsId)
		{
			List<VMJanji> faskes = await
				(
				from doc_off in _context.t_doctor_office
				join med_fac in _context.m_medical_facility
				on doc_off.mdeical_facility_id equals med_fac.id

				join doc in _context.m_doctor
				on doc_off.doctor_id equals doc.id

				join med_fac_cat in _context.m_medical_facility_category
				on med_fac.medical_facility_category_id equals med_fac_cat.id

				join loc in _context.m_location
				on med_fac.location_id equals loc.id

				join loc_lev in _context.m_location_level
				on loc.location_level_id equals loc_lev.id
				where doc_off.doctor_id == id && doc_off.mdeical_facility_id == rsId

				select new VMJanji
				{
					id = med_fac.id,
					name = med_fac.name
				}).ToListAsync();

			faskes.Insert(0, new VMJanji { id = 0, name = "--Pilih Faskes--" });
			return Json(faskes);
		}

		public async Task<JsonResult> GetTreat(int id)
		{
			List<VMJanji> treat = await
				(
				from doc_off_treat in _context.t_doctor_office_treatment
				join doc_treat in _context.t_doctor_treatment
				on doc_off_treat.doctor_treatment_id equals doc_treat.id

				join doc_off in _context.t_doctor_office
				on doc_off_treat.doctor_office_id equals doc_off.id

				join doc in _context.m_doctor
				on doc_treat.doctor_id equals doc.id
				where doc.id == id

				select new VMJanji
				{
					id = doc_treat.id,
					name = doc_treat.name
				}).ToListAsync();

			treat.Insert(0, new VMJanji { id = 0, name = "--Pilih Faskes--" });
			return Json(treat);
		}

		public async Task<JsonResult> GetSchedule(int id,int rsId)
		{
			//List<VMJanji> sched = await
			//	(
			//	from doc_off_sched in _context.t_doctor_office_schedule
			//	join doc in _context.m_doctor
			//	on doc_off_sched.doctor_id equals doc.id

			//	join med_fac_sched in _context.m_medical_facility_schedule
			//	on doc_off_sched.medical_facility_schedule_id equals med_fac_sched.medical_facility_id

			//	join med_fac in _context.m_medical_facility
			//	on med_fac_sched.medical_facility_id equals med_fac.id
			//	where doc.id == id

			//	select new VMJanji
			//	{
			//		medical_facility_schedule_id = med_fac_sched.id,
			//		time_schedule_start = med_fac_sched.time_schedule_start,
			//		time_schedule_end = med_fac_sched.time_schedule_end,
			//		day = med_fac_sched.day,
			//		slot = doc_off_sched.slot,
			//	}).ToListAsync();

			List<VMDetailDokter> cde = await
			   (

					   from medfac in _context.m_medical_facility

					   join medfacsched in _context.m_medical_facility_schedule
					   on medfac.id equals medfacsched.medical_facility_id

					   join docofsched in _context.t_doctor_office_schedule
					   on medfacsched.id equals docofsched.medical_facility_schedule_id


					   where docofsched.doctor_id == id && medfac.medical_facility_category_id != 2 && medfac.id== rsId

			select new VMDetailDokter

					   {
							//id = doc.id,
							doctor_id = docofsched.doctor_id,
						   id = medfac.id,
						   day = medfacsched.day,
						   time_schedule_start = medfacsched.time_schedule_start,
						   time_schedule_end = medfacsched.time_schedule_end,
						   DoctorBackup_id = docofsched.medical_facility_schedule_id

					   }).ToListAsync();

			return Json(cde);
		}

		public async Task<IActionResult> CheckSlot(t_appointment appointments)
		{
			var date = Convert.ToString(appointments.appointment_date);
			DateTime hariFixx = Convert.ToDateTime(date);

			var tgl = Convert.ToDateTime(date).ToShortDateString();

			CultureInfo indo = new CultureInfo("id-ID");

			var namaHari = hariFixx.DayOfWeek;
			string hariTrans = indo.DateTimeFormat.DayNames[(int)namaHari];

			List<t_appointment> appointed = await
				(
				from c in _context.t_appointment

				//where c.appointment_date == Convert.ToDateTime(tgl)

				select new t_appointment
				{
					appointment_date = c.appointment_date
				}
				).ToListAsync();

			var count = appointed.Count();
			var counter = 0;

			foreach(var x in appointed)
			{
				if(Convert.ToDateTime(x.appointment_date).ToShortDateString() == tgl)
				{
					counter++;
				}
			}

			var slotAvail = await
				(
				from doc_off_sched in _context.t_doctor_office_schedule
				join doc in _context.m_doctor
				on doc_off_sched.doctor_id equals doc.id

				join med_fac_sched in _context.m_medical_facility_schedule
				on doc_off_sched.medical_facility_schedule_id equals med_fac_sched.medical_facility_id

				join med_fac in _context.m_medical_facility
				on med_fac_sched.medical_facility_id equals med_fac.id
				where med_fac.id == appointments.doctor_office_id && med_fac_sched.day == hariTrans

				select new VMJanji
				{
					slot = doc_off_sched.slot,
				}).FirstOrDefaultAsync();

			if (counter < slotAvail.slot)
			{
				_context.Add(appointments);
				await _context.SaveChangesAsync();
				return Json(new { status = "success" });
			}
			else
			{
				return Json(new { status = "error" });
			}
		}
		public static int CalculateAge(DateTime? dob)
		{
			int age = 0;
			DateTime doba = Convert.ToDateTime(dob);

			age = DateTime.Now.Year - doba.Year;
			if (DateTime.Now.DayOfYear < doba.DayOfYear)
			{
				age = age - 1;
			}

			return age;
		}
	}
}
