﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;

namespace MiniProject262.Controllers
{
	public class m_bankController : Controller
	{
		private readonly MiniProjectContext _context;

		public m_bankController(MiniProjectContext context)
		{
			_context = context;
		}

		// GET: m_bank
		public IActionResult Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pSize)
		{
			ViewData["CurrentSort"] = sortOrder;
			ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
			ViewData["CurrentpSize"] = pSize;
			ViewBag.pageSize = PageinatedList<m_bank>.PageSizeList();
			if (searchString != null)
			{
				pageNumber = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["CurrentFilter"] = searchString;
			var banks = from c in _context.m_bank.Where(m => m.is_delete == false) select c;
			if (!string.IsNullOrEmpty(searchString))
			{
				banks = banks.Where(c => c.name.Contains(searchString));
			}

			switch (sortOrder)
			{
				case "name_desc":
					banks = banks.OrderByDescending(c => c.name);
					break;
				default:
					banks = banks.OrderBy(c => c.name);
					break;
			}

			int counter = banks.Count();
			ViewBag.counter = counter;

			int pageSize = pSize ?? 5;

			return View(PageinatedList<m_bank>.CreateAsync(banks.AsNoTracking().Where(a => a.is_delete == false).ToList(), pageNumber ?? 1, pageSize));
		}

		// GET: m_bank/Details/5
		public async Task<IActionResult> Details(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var m_bank = await _context.m_bank
				.FirstOrDefaultAsync(m => m.id == id);
			if (m_bank == null)
			{
				return NotFound();
			}

			return View(m_bank);
		}

		// GET: m_bank/Create
		public IActionResult Create()
		{
			return View();
		}

		// POST: m_bank/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("id,name,va_code,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_bank m_bank)
		{
			var createdBy = HttpContext.Session.GetInt32("iduser");
			var dateTime = DateTime.Now;
			var banks = from c in _context.m_bank select c;
			var findBanks = await _context.m_bank.FirstOrDefaultAsync(m => m.name == m_bank.name && m.is_delete == false);
			var va_num = await _context.m_bank.FirstOrDefaultAsync(n => n.va_code == m_bank.va_code && n.is_delete == false);

			if (ModelState.IsValid)
			{
				if (findBanks == null && va_num == null)
				{
					m_bank.created_by = Convert.ToInt64(createdBy);
					m_bank.created_on = dateTime;
					_context.Add(m_bank);
					await _context.SaveChangesAsync();
					return RedirectToAction(nameof(Index));
				}
				return RedirectToAction(nameof(Index));
			}

			return View(m_bank);
		}

		// GET: m_bank/Edit/5
		public async Task<IActionResult> Edit(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var m_bank = await _context.m_bank.FindAsync(id);
			if (m_bank == null)
			{
				return NotFound();
			}
			return View(m_bank);
		}

		// POST: m_bank/Edit/5
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(long id, [Bind("id,name,va_code,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_bank m_bank)
		{
			var modifiedBy = HttpContext.Session.GetInt32("iduser");
			var dateTime = DateTime.Now;
			var findBanks = await _context.m_bank.FirstOrDefaultAsync(m => m.name == m_bank.name && m.id != m_bank.id && m.is_delete == false);
			var va_num = await _context.m_bank.FirstOrDefaultAsync(n => n.va_code == m_bank.va_code && n.id != m_bank.id && n.is_delete == false);
			if (id != m_bank.id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					if(findBanks == null && va_num == null)
					{
						m_bank.modified_by = modifiedBy;
						m_bank.modified_on = dateTime;
						_context.Update(m_bank);
						await _context.SaveChangesAsync();
					}
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!m_bankExists(m_bank.id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(m_bank);
		}

		// GET: m_bank/Delete/5
		public async Task<IActionResult> Delete(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var m_bank = await _context.m_bank
				.FirstOrDefaultAsync(m => m.id == id);
			if (m_bank == null)
			{
				return NotFound();
			}

			return View(m_bank);
		}

		// POST: m_bank/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(long id)
		{
			var deletedBy = HttpContext.Session.GetInt32("iduser");
			var dateTime = DateTime.Now;
			var m_bank = await _context.m_bank.FindAsync(id);
			m_bank.is_delete = true;
			m_bank.deleted_by = deletedBy;
			m_bank.deleted_on = dateTime;

			_context.m_bank.Update(m_bank);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool m_bankExists(long id)
		{
			return _context.m_bank.Any(e => e.id == id);
		}
	}
}
