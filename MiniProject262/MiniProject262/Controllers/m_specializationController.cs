﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;

namespace MiniProject262.Controllers
{
    public class m_specializationController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_specializationController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: m_specialization
        public async Task<IActionResult> Index(string searchString, string currentFilter, int? pageNumber, int? pSize)
        {
            ViewData["CurrentFilter"] = searchString;

            var students = from s in _context.m_specialization
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.name.Contains(searchString));
            }

            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<m_specialization>.PageSizeList();
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            int pageSize = pSize ?? 5;


            return View(PageinatedList<m_specialization>.CreateAsync(students.AsNoTracking().Where(a => a.is_delete == false).ToList(), pageNumber ?? 1, pageSize));
        }

        // GET: m_specialization/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_specialization = await _context.m_specialization
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_specialization == null)
            {
                return NotFound();
            }

            return View(m_specialization);
        }

        // GET: m_specialization/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: m_specialization/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_specialization m_specialization)
        {
            if (ModelState.IsValid)
            {
                m_specialization.created_by = (long)HttpContext.Session.GetInt32("iduser");

                var ceking = _context.m_specialization.FirstOrDefault(a => a.name == m_specialization.name && a.is_delete == false);
                if (ceking != null)
                {

                    return Json(new { status = "error", message = "*data sudah ada" });
                }
                DateTime dt = DateTime.Now;
                m_specialization.created_on = dt;

                _context.Add(m_specialization);
                await _context.SaveChangesAsync();
                return Json(new { status = "success" });
            }
            return View(m_specialization);
        }

        // GET: m_specialization/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_specialization = await _context.m_specialization.FindAsync(id);
            if (m_specialization == null)
            {
                return NotFound();
            }
            return View(m_specialization);
        }

        // POST: m_specialization/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_specialization m_specialization)
        {
            if (id != m_specialization.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                try
                {
                    m_specialization.modified_by = (long)HttpContext.Session.GetInt32("iduser");

                    var ceking = _context.m_specialization.FirstOrDefault(a => a.name == m_specialization.name && a.is_delete == false);
                    if (ceking != null)
                    {

                        return Json(new { status = "error", message = "*data sudah ada" });
                    }
                    DateTime dt = DateTime.Now;
                    m_specialization.modified_on = dt;

                    _context.Update(m_specialization);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_specializationExists(m_specialization.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(new { status = "success" });
            }
            return View(m_specialization);
        }

        // GET: m_specialization/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_specialization = await _context.m_specialization
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_specialization == null)
            {
                return NotFound();
            }

            return View(m_specialization);
        }

        // POST: m_specialization/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_specialization m_specialization)
        {
            if (id != m_specialization.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                m_specialization.is_delete = true;
                try
                {
                    m_specialization.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    m_specialization.deleted_on = dt;

                    _context.Update(m_specialization);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_specializationExists(m_specialization.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_specialization);
        }

        private bool m_specializationExists(long id)
        {
            return _context.m_specialization.Any(e => e.id == id);
        }
    }
}
