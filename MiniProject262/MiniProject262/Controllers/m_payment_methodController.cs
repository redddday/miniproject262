﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;

namespace MiniProject262.Controllers
{
    public class m_payment_methodController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_payment_methodController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: m_payment_method
        public IActionResult Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pSize)
        {
            m_admin dummy = new m_admin
            {
                id = 5
            };
            ViewBag.dummy = dummy.id;
            //countCart();
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<m_payment_method>.PageSizeList();
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;
            var mpm = from c in _context.m_payment_method select c;
            if (!string.IsNullOrEmpty(searchString))
            {
                mpm = mpm.Where(c => c.name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    mpm = mpm.OrderByDescending(c => c.name);
                    break;
                default:
                    mpm = mpm.OrderBy(c => c.name);
                    break;
            }

            int pageSize = pSize ?? 5;
            
            return View(PageinatedList<m_payment_method>.CreateAsync(mpm.AsNoTracking().Where(a=> a.is_delete==false).ToList(), pageNumber ?? 1, pageSize));
        }
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_payment_method = await _context.m_payment_method
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_payment_method == null)
            {
                return NotFound();
            }

            return View(m_payment_method);
        }

        // GET: m_payment_method/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: m_payment_method/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_payment_method m_payment_method)
        {
            if (ModelState.IsValid)
            {
                m_payment_method.created_by = (long)HttpContext.Session.GetInt32("iduser");
                DateTime dt = DateTime.Now;
                m_payment_method.created_on = dt;

                _context.Add(m_payment_method);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(m_payment_method);
        }

        // GET: m_payment_method/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_payment_method = await _context.m_payment_method.FindAsync(id);
            if (m_payment_method == null)
            {
                return NotFound();
            }
            return View(m_payment_method);
        }

        // POST: m_payment_method/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_payment_method m_payment_method)
        {
            if (id != m_payment_method.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_payment_method.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    //DateTime dt = DateTime.Now;
                    m_payment_method.modified_on = DateTime.Now;

                    _context.Update(m_payment_method);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_payment_methodExists(m_payment_method.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_payment_method);
        }

        // GET: m_payment_method/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_payment_method = await _context.m_payment_method
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_payment_method == null)
            {
                return NotFound();
            }

            return View(m_payment_method);
        }

        // POST: m_payment_method/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_payment_method mpm)
        {
            if (id != mpm.id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                mpm.is_delete = true;
                try
                {
                    mpm.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    //DateTime dt = DateTime.Now;
                    mpm.deleted_on = DateTime.Now;
                    

                    _context.Update(mpm);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_payment_methodExists(mpm.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }                    
                }
                return RedirectToAction(nameof(Index));
            }
            return View();
            //var m_payment_method = await _context.m_payment_method.FindAsync(id);
            //_context.m_payment_method.Remove(m_payment_method);
            //await _context.SaveChangesAsync();
            //return RedirectToAction(nameof(Index));
        }

        private bool m_payment_methodExists(long id)
        {
            return _context.m_payment_method.Any(e => e.id == id);
        }
    }
}
