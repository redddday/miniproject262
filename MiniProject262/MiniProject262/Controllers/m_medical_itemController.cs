﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;

namespace MiniProject262.Controllers
{
	public class m_medical_itemController : Controller
	{
		private readonly MiniProjectContext _context;

		public m_medical_itemController(MiniProjectContext context)
		{
			_context = context;
		}

		// GET: m_medical_item
		public async Task<IActionResult> Index()
		{
			return View(await _context.m_medical_item.ToListAsync());
		}

		// GET: m_medical_item/Details/5
		public async Task<IActionResult> Details(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var m_medical_item = await _context.m_medical_item
				.FirstOrDefaultAsync(m => m.id == id);
			if (m_medical_item == null)
			{
				return NotFound();
			}

			return View(m_medical_item);
		}

		// GET: m_medical_item/Create
		public IActionResult Create()
		{
			return View();
		}

		// POST: m_medical_item/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("id,name,medical_item_category_id,composition,medical_item_segmentation_id,manufacturer,indication,dosage,directions,contraindication,caution,packaging,price_max,price_min,image,image_path,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_medical_item m_medical_item)
		{
			if (ModelState.IsValid)
			{
				_context.Add(m_medical_item);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(m_medical_item);
		}

		// GET: m_medical_item/Edit/5
		public async Task<IActionResult> Edit(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var m_medical_item = await _context.m_medical_item.FindAsync(id);
			if (m_medical_item == null)
			{
				return NotFound();
			}
			return View(m_medical_item);
		}

		// POST: m_medical_item/Edit/5
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(long id, [Bind("id,name,medical_item_category_id,composition,medical_item_segmentation_id,manufacturer,indication,dosage,directions,contraindication,caution,packaging,price_max,price_min,image,image_path,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_medical_item m_medical_item)
		{
			if (id != m_medical_item.id)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(m_medical_item);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!m_medical_itemExists(m_medical_item.id))
					{
						return NotFound();
					}
					else
					{
						throw;
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(m_medical_item);
		}

		// GET: m_medical_item/Delete/5
		public async Task<IActionResult> Delete(long? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var m_medical_item = await _context.m_medical_item
				.FirstOrDefaultAsync(m => m.id == id);
			if (m_medical_item == null)
			{
				return NotFound();
			}

			return View(m_medical_item);
		}

		// POST: m_medical_item/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(long id)
		{
			var m_medical_item = await _context.m_medical_item.FindAsync(id);
			_context.m_medical_item.Remove(m_medical_item);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool m_medical_itemExists(long id)
		{
			return _context.m_medical_item.Any(e => e.id == id);
		}

		public async Task<IActionResult> SearchObat(string searchString)
		{
			List<m_medical_item_category> medItem = await DataKate();
			ViewBag.KateObat = medItem;

			//List<m_location> cd = await DataLoc();
			//ViewBag.ListLoc = cd;

			//List<t_doctor_treatment> ck = await DataTreat();
			//ViewBag.LisDocTreat = ck;

			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> SearchObat(VMCariObat vmCariObat)
		{

			//VMCariObat newObat = await DataCariObat(vmCariObat.id);

			List<m_specialization> cl = new List<m_specialization>();
			cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
			ViewBag.ListSpec = cl;

			//List<m_location> cd = new List<m_location>();
			//cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
			//ViewBag.ListLoc = cd;

			//List<t_doctor_treatment> ck = new List<t_doctor_treatment>();
			//ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
			//ViewBag.LisDocTreat = ck;

			//return View(newObat);
			return View();
		}

		public async Task<IActionResult> HasilObat(string kateObat, string namaObat, string jenisObat, string hargaMin, string hargaMax, string searchString, int? pageNumber, int? pSize, string currentFilter)
		{
			var obat = from obatItem in _context.m_medical_item
					   join obatCate in _context.m_medical_item_category
					   on obatItem.medical_item_category_id equals obatCate.id

					   join obatSegme in _context.m_medical_item_segmentation
					   on obatItem.medical_item_segmentation_id equals obatSegme.id

					   select new VMCariObat
					   {
						   id = obatItem.id,
						   name = obatItem.name,
						   composition = obatItem.composition,
						   manufacturer = obatItem.manufacturer,
						   indication = obatItem.indication,
						   dosage = obatItem.dosage,
						   directions = obatItem.directions,
						   contraindication = obatItem.contraindication,
						   caution = obatItem.caution,
						   packaging = obatItem.packaging,
						   price_min = obatItem.price_min,
						   price_max = obatItem.price_max,
						   med_item_cat_name = obatCate.name,
						   med_item_seg_name = obatSegme.name
					   };

			ViewBag.ResultObat = obat;

			List<m_medical_item_category> listObat = new List<m_medical_item_category>();
			listObat.Insert(0, new m_medical_item_category { id = 0, name = "", is_delete = false });
			ViewBag.ListCate = listObat;

			ViewData["CurrentpSize"] = pSize;
			ViewBag.pageSize = PageinatedList<m_payment_method>.PageSizeList();
			if (searchString != null)
			{
				pageNumber = 1;
			}
			else
			{
				searchString = currentFilter;
			}

			ViewData["CurrentFilter1"] = kateObat;
			ViewData["CurrentFilter2"] = namaObat;
			ViewData["CurrentFilter3"] = jenisObat;
			ViewData["CurrentFilter4"] = hargaMin;
			ViewData["CurrentFilter5"] = hargaMax;
			ViewBag.Filter1 = kateObat;
			ViewBag.Filter2 = namaObat;
			ViewBag.Filter3 = jenisObat;
			ViewBag.Filter4 = hargaMin;
			ViewBag.Filter5 = hargaMax;

			if(jenisObat == "Bebas")
			{
				if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin));
				}

				else if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat));
				}

				else if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin));
				}

				else if (!String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin))
				{
					obat = obat.Where(s => s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin));
				}

				else if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(kateObat) && !String.IsNullOrEmpty(jenisObat))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(kateObat) && s.med_item_cat_name.Contains(jenisObat));
				}

				else if (!String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMax))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(jenisObat) && s.price_max <= Convert.ToInt32(hargaMax));
				}

				else if (!String.IsNullOrEmpty(jenisObat) && !String.IsNullOrEmpty(hargaMin))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(jenisObat) && s.price_min >= Convert.ToInt32(hargaMin));
				}

				else if (!String.IsNullOrEmpty(namaObat) && !String.IsNullOrEmpty(jenisObat))
				{
					obat = obat.Where(s => s.name.Contains(namaObat) && s.med_item_cat_name.Contains(jenisObat));
				}
				else if (!String.IsNullOrEmpty(jenisObat))
				{
					obat = obat.Where(s => s.med_item_cat_name.Contains(jenisObat));
				}
			}

			int pageSize = pSize ?? 6;
			ViewBag.pagsize = ViewData["CurrentpSize"];
			return View(PageinatedList<VMCariObat>.CreateAsync(obat.AsNoTracking().ToList(), pageNumber ?? 1, pageSize));
		}

		public async Task<List<m_medical_item_category>> DataKate()
		{
			List<m_medical_item_category> listObat = await (from c in _context.m_medical_item_category select c).ToListAsync();
			listObat.Insert(0, new m_medical_item_category { id = 0, name = "", is_delete = false });
			ViewBag.DataObat = listObat;

			return listObat;
		}
	}
}
