﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniProject262.Controllers
{
    public class HakAksesController : Controller
    {
        private readonly MiniProjectContext _context;

        public HakAksesController(MiniProjectContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index(string searchString, string currentFilter, int? pageNumber, int? pSize)
        {
            ViewData["CurrentFilter"] = searchString;

            var role = from r in _context.m_role
                       select r;

            if (!String.IsNullOrEmpty(searchString))
            {
                role = role.Where(s => s.name.Contains(searchString));
            }

            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<m_specialization>.PageSizeList();
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            int pageSize = pSize ?? 1;
            return View(PageinatedList<m_role>.CreateAsync(role.AsNoTracking().ToList(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> AturAkses(int Id)
        {
            ViewBag.idrole = Id;
            List<VMMenuRole> ListMenu = await (from a in _context.m_menu
                                               select new VMMenuRole
                                               {
                                                   m_menu_id = a.id,
                                                   m_menu_parent_id = a.parent_id,
                                                   m_menu_name = a.name
                                               }).ToListAsync();

            VMMenuRole cek = await CekRoleExist(Id);

            if (cek != null)
            {
                ViewBag.dataMenu = await GetMenuMaster(Id);

            }

            return View(ListMenu);
        }

        [HttpPost]
        public async Task<IActionResult> AturAkses(int? id, IEnumerable<int> terpilih)
        {

            if (ModelState.IsValid)
            {

                VMMenuRole cek = await CekRoleExist(id);
                var hasil = await (from r in _context.m_menu_role
                                   where r.role_id == id
                                   select r).ToListAsync();

                if (cek != null && terpilih.Count() == 0)
                {
                    foreach (var item in hasil)
                    {
                        item.is_delete = true;
                        item.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                        item.deleted_on = DateTime.Now;
                        _context.Update(item);
                        await _context.SaveChangesAsync();
                    }

                }
                else if (cek != null && terpilih.Count() != 0)
                {
                    foreach (var item in hasil)
                    {
                        item.is_delete = true;
                        item.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                        item.deleted_on = DateTime.Now;
                        _context.Update(item);
                        await _context.SaveChangesAsync();

                        foreach (var pilih in terpilih)
                        {
                            var ceking = _context.m_menu_role.FirstOrDefault(a => a.menu_id == pilih && a.role_id == id);
                          //  var cekinglah = hasil.FirstOrDefault(a => a.role_id == id);
                            if (item.menu_id == pilih)
                            {
                                item.is_delete = false;
                                item.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                                item.modified_on = DateTime.Now;
                                _context.Update(item);
                                await _context.SaveChangesAsync();
                            }   
                            //foreach (var item in hasil)
                            //{
                            //    foreach (var dangdut in terpilih)
                            //    {
                            //        if (item.menu_id != dangdut)
                            //        {
                            //            item.is_delete = true;
                            //            _context.Update(item);
                            //            await _context.SaveChangesAsync();
                            //        }
                            //    }


                            //}

                            //foreach (var item in hasil)
                            //{
                            //    if (item.menu_id == pilih)
                            //    {
                            //        item.is_delete = false;
                            //        _context.Update(item);
                            //        await _context.SaveChangesAsync();
                            //    }


                            //}

                            if (ceking == null)
                            {                          
                                DateTime dt = DateTime.Now;
                                m_menu_role menu_role = new m_menu_role();
                                menu_role.role_id = id;
                                menu_role.menu_id = pilih;
                                _context.Add(menu_role);
                                menu_role.created_by = (long)HttpContext.Session.GetInt32("iduser");
                                menu_role.created_on = dt;
                                await _context.SaveChangesAsync();
                            }

                        }

                    }


                }
                else
                {
                    foreach (var item in terpilih)
                    {
                        DateTime dt = DateTime.Now;
                        m_menu_role menu_role = new m_menu_role();
                        menu_role.role_id = id;
                        menu_role.menu_id = item;
                        menu_role.created_by = (long)HttpContext.Session.GetInt32("iduser");
                        menu_role.created_on = dt;
                        _context.Add(menu_role);
                        await _context.SaveChangesAsync();

                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(terpilih);


        }

        //[HttpPost]
        //public async Task<IActionResult> AturAkses(string data, int idrole)
        //{
        //    string[] arr = data.Split(",");
        //    if (ModelState.IsValid)
        //    {
        //        foreach (var item in arr)
        //        {
        //            m_menu_role mr = new m_menu_role();
        //            mr.menu_id = int.Parse(item);
        //            mr.role_id = idrole;

        //            _context.Add(mr);
        //            await _context.SaveChangesAsync();

        //        }
        //    }
        // //   List<m_menu> ListMenu = await _context.m_menu.ToListAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        public async Task<VMMenuRole> CekRoleExist(int? role)
        {
            var result = await (from a in _context.m_menu
                                join b in _context.m_menu_role
                                  on a.id equals b.menu_id
                                join c in _context.m_role
                                on b.role_id equals c.id
                                where c.id == role
                                select new VMMenuRole
                                {
                                    m_menu_id = a.id,
                                    m_menu_name = a.name,
                                    menu_role_role_id = c.id,
                                    role_name = c.name,
                                    m_menu_parent_id = a.parent_id
                                    //MenuAction = a.MenuAction,
                                    //MenuController = a.MenuController

                                }).FirstOrDefaultAsync();

            return result;
        }

        public async Task<List<VMMenuRole>> GetMenuMaster(int? role)
        {
            var result = await (from a in _context.m_menu
                                join b in _context.m_menu_role
                                  on a.id equals b.menu_id
                                join c in _context.m_role
                                on b.role_id equals c.id
                                where c.id == role && b.is_delete == false
                                select new VMMenuRole
                                {
                                    m_menu_id = a.id,
                                    m_menu_name = a.name,
                                    menu_role_role_id = c.id,
                                    role_name = c.name,
                                    m_menu_parent_id = a.parent_id
                                    //MenuAction = a.MenuAction,
                                    //MenuController = a.MenuController

                                }).ToListAsync();

            return result;
        }


    }
}
