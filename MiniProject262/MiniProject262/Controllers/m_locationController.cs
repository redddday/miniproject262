﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;

namespace MiniProject262.Controllers
{
    public class m_locationController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_locationController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: m_location
        public async Task<IActionResult> Index(string nama, string abb, string level, string SortOrder, string SearchString, int SearchString1, int? pageNumber, int? pSize, string CurrentFilter, string CurrentFilter1)
        {
            //m_admin dummy = new m_admin
            //{
            //    id = 200
            //};

            var locations = from loc in _context.m_location
                            join loclev in _context.m_location_level
                            on loc.location_level_id equals loclev.id
                            join locp in _context.m_location
                            on loc.parent_id equals locp.id
                            join loclevp in _context.m_location_level
                            on locp.location_level_id equals loclevp.id
                            where loc.is_delete == false && loc.parent_id != 2

                            select new VMLocation
                            {
                                id = loc.id,
                                location_level_id = loclev.id,

                                parent_id = loc.parent_id,
                                nameLocation = loc.name,
                                nameLocationLevel = loclev.name,
                                abbreviation = loclev.abbreviation,
                                wilayah = loclevp.abbreviation + " " + locp.name,

                                created_by = loc.created_by,
                                created_on = loc.created_on,
                                modified_by = loc.modified_by,
                                modified_on = loc.modified_on,
                                deleted_by = loc.deleted_by,
                                deleted_on = loc.deleted_on,
                                is_delete = loc.is_delete

                            };

            //List<m_location_level> ll = new List<m_location_level>();
            //ll = (from r in _context.m_location_level
            //      where r.is_delete == false
            //      select r).ToList();
            //ll.Insert(0, new m_location_level { id = 0, name = "Level Lokasi", is_delete = false });
            //ViewBag.ListLocLevel = ll;



            if (SearchString1 == 0)
            {
                List<m_location_level> ll = new List<m_location_level>();
                ll = (from r in _context.m_location_level
                      where r.is_delete == false
                      select r).ToList();
                ll.Insert(0, new m_location_level { id = 0, name = "Select All", is_delete = false });
                ViewBag.ListLocLevel = ll;

            }
            else if (SearchString1 != 0)
            {

                //ll.Select(SearchString1, new m_location_level { id = 0, name = "Level Lokasi", is_delete = false });
                var locc = await (from l in _context.m_location_level
                                  where l.id == SearchString1
                                  select l.name).FirstOrDefaultAsync();

                List<m_location_level> list = await (from c in _context.m_location_level select c).ToListAsync();
                //list.Select({ id = SearchString1, name = locc });
                list.Insert(0, new m_location_level { id = SearchString1, name = locc });
                list.RemoveAt(SearchString1);
                list.Insert(SearchString1, new m_location_level { id = 0, name = "Select All", is_delete = false });
                ViewBag.ListLocLevel = list;
            }

            ViewData["CurrentSort"] = SortOrder;
            ViewData["CurrentFilter"] = SearchString;
            ViewData["CurrentFilter1"] = SearchString1;
            //ViewData["NameSortParm"] = String.IsNullOrEmpty(SortOrder) ? "name_desc" : "";
            ViewBag.sort = sortingLocation();

            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<VMLocation>.PageSizeList();

            if (SearchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                SearchString = CurrentFilter;
            }
            if (SearchString1 != 0)
            {
                pageNumber = 1;
            }
            else
            {
                SearchString1 = Convert.ToInt32(CurrentFilter1);
            }

            //ViewData["CurrentFilter"] = nama;
            //ViewData["CurrentFilter1"] = level;
            //ViewBag.Filter = nama;
            //ViewBag.Filter1 = level;

            //var loc = from l in _context.VMLocation
            //          select l;
            //if (!string.IsNullOrEmpty(nama))
            //{
            //    locations = locations.Where(l => l.nameLocation.Contains(nama));
            //}

            //var locLvl = from v in _context.VMLocation
            //          select v;
            //if (!string.IsNullOrEmpty(SearchString))
            //{
            //    locations = locations.Where(l => l.nameLocationLevel.Contains(SearchString));
            //}

            if ( SearchString1 == 0)
            {
                SearchString1 = 0;
            }

            if (!string.IsNullOrEmpty(SearchString) && SearchString1 != 0)
            {
                locations = locations.Where(l => l.nameLocation.Contains(SearchString) && l.location_level_id==SearchString1);
            }
            else if (!string.IsNullOrEmpty(SearchString))
            {
                locations = locations.Where(l => l.nameLocation.Contains(SearchString));
            }

            //    switch (SortOrder)
            //{
            //    case "name_desc":
            //        locations = locations.OrderByDescending(c => c.nameLocation);
            //        break;
            //    case "name_asc":
            //        locations = locations.OrderBy(c => c.nameLocation);
            //        break;
            //    case "level_asc":
            //        locations = locations.OrderByDescending(c => c.nameLocationLevel);
            //        break;
            //    default:
            //        locations = locations.OrderBy(c => c.nameLocationLevel);
            //        break;
            //}

            int pageSize = pSize ?? 5;
            return View(PageinatedList<VMLocation>.CreateAsync(locations.AsNoTracking().ToList(), pageNumber ?? 1, pageSize));

            //return View(await _context.m_role.ToListAsync());

        }

        public static IEnumerable<SelectListItem> sortingLocation()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Level Lokasi", Value = "level_asc"},
                new SelectListItem{Text = "Nama", Value = "name_asc"}
            };
            return items;
        }

        // GET: m_location/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_location = await _context.m_location
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_location == null)
            {
                return NotFound();
            }

            return View(m_location);
        }

        // GET: m_location/Create
        public IActionResult Create()
        {
            List<m_location_level> listLev = new List<m_location_level>();
            listLev = (from r in _context.m_location_level
                       where r.is_delete == false
                       select r).ToList();
            listLev.Insert(0, new m_location_level { id = 0, name = "--Pilih--" });
            ViewBag.ld = listLev;

            List<VMLocation> vw = new List<VMLocation>();
            vw = (from loc in _context.m_location
                  join lvl in _context.m_location_level
                  on loc.location_level_id equals lvl.id
                  join locp in _context.m_location
                  on loc.parent_id equals locp.id
                  join lvlp in _context.m_location_level
                  on locp.location_level_id equals lvlp.id
                  where loc.is_delete == false && lvl.is_delete == false
                  //loc.parent_id == loc.id && loc.location_level_id <= lvl.id
                  select new VMLocation
                  {
                      id = loc.id,
                      parent_id = loc.parent_id,
                      nameLocation = loc.name,
                      nameLocationLevel = lvl.name,
                      abbreviation = lvl.abbreviation,
                      created_by = loc.created_by,
                      created_on = loc.created_on,
                      modified_by = loc.modified_by,
                      modified_on = loc.modified_on,
                      deleted_by = loc.deleted_by,
                      deleted_on = loc.deleted_on,
                      is_delete = loc.is_delete,
                      wilayah = lvl.abbreviation + " " + loc.name
                  }
                   ).ToList();
            vw.Insert(0, new VMLocation { id = 0, wilayah = "--Pilih--" });
            //ViewBag.lc = vw;



            //List<m_location_level> listLevel = await DataLevel();
            //List<m_location> listWil = new List<m_location>();
            //listWil.Insert(0, new m_location { id = 0, name = "--Pilih lokasi lu--" });
            //ViewBag.ld = listLevel;
            //ViewBag.lc = listWil;
            //return View();




            //List<VMLocation> wil = new List<VMLocation>();
            //wil = (from w in _context.VMLocation select w).ToList();
            //wil.Insert(0, new VMLocation { id = 0, nameLocationLevel = });
            //ViewBag.wl = wil;
            return View();
        }

        // POST: m_location/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VMLocation location)
        {
            if (ModelState.IsValid)
            {
                var cekLokasi = LocationExist(location.nameLocation, location.id);

                if (cekLokasi != null)
                {
                    return Json(new { status = "error", message = "*Nama sudah ada untuk wilayah tersebut!" });
                }

                m_location locnew = new m_location();
                locnew.name = location.nameLocation;
                locnew.parent_id = location.id;
                locnew.location_level_id = location.location_level_id;
                locnew.created_by = (long)HttpContext.Session.GetInt32("iduser");
                locnew.created_on = DateTime.Now;

                _context.Add(locnew);
                await _context.SaveChangesAsync();
                //return RedirectToAction(nameof(Index));
                return Json(new { status = "sucess" });
            }


            List<m_location_level> listLev = new List<m_location_level>();
            listLev = (from r in _context.m_location_level
                       where r.is_delete == false
                       select r).ToList();
            listLev.Insert(0, new m_location_level { id = 0, name = "--Pilih--" });
            ViewBag.ld = listLev;
            return View(location);
        }

        private m_location LocationExist(string nameLocation, long id)
        {
            return _context.m_location.FirstOrDefault(a => a.name == nameLocation && a.parent_id == id);
        }

        // GET: m_location/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            List<m_location_level> listLev = new List<m_location_level>();
            listLev = (from r in _context.m_location_level
                       where r.is_delete == false
                       select r).ToList();
            listLev.Insert(0, new m_location_level { id = 0, name = "--Pilih--" });
            ViewBag.ld = listLev;

            if (id == 0)
            {
                return NotFound();
            }

            List<VMLocation> vw = new List<VMLocation>();
            vw = (from loc in _context.m_location
                  join lvl in _context.m_location_level
                  on loc.location_level_id equals lvl.id
                  join locp in _context.m_location
                  on loc.parent_id equals locp.id
                  join loclevp in _context.m_location_level
                  on locp.location_level_id equals loclevp.id

                  //where loc.parent_id == loc.id && loc.location_level_id <= lvl.id
                  select new VMLocation
                  {
                      id = loc.id,
                      parent_id = locp.id,
                      nameLocation = loc.name,
                      nameLocationLevel = lvl.name,
                      location_level_id = loc.location_level_id,
                      wilayah = lvl.abbreviation + " " + loc.name,

                      created_by = loc.created_by,
                      created_on = loc.created_on,
                      modified_by = loc.modified_by,
                      modified_on = loc.modified_on,
                      deleted_by = loc.deleted_by,
                      deleted_on = loc.deleted_on,
                      is_delete = loc.is_delete,
                  }
                   ).ToList();
            //vw.Insert(0, new VMLocation { id = 0, wilayah = "--Pilih--" });

            VMLocation vloc = (from loc in _context.m_location
                               join loclev in _context.m_location_level
                               on loc.location_level_id equals loclev.id
                               join locp in _context.m_location
                               on loc.parent_id equals locp.id
                               join loclevp in _context.m_location_level
                               on locp.location_level_id equals loclevp.id
                               where loc.is_delete == false

                               select new VMLocation
                               {
                                   id = loc.id,
                                   location_level_id = loclev.id,
                                   parent_id = locp.id,
                                   nameLocation = loc.name,
                                   nameLocationLevel = loclev.name,
                                   abbreviation = loclev.abbreviation,
                                   created_by = loc.created_by,
                                   created_on = loc.created_on,
                                   modified_by = loc.modified_by,
                                   modified_on = loc.modified_on,
                                   deleted_by = loc.deleted_by,
                                   deleted_on = loc.deleted_on,
                                   is_delete = loc.is_delete,
                                   wilayah = loclev.abbreviation + " " + loc.name
                               }
                                ).FirstOrDefault(a => a.id == id);

            if (vloc == null)
            {
                return NotFound();
            }

            ViewBag.wil = await DataWilayah((int)vloc.parent_id);

            return View(vloc);
        }

        // POST: m_location/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(VMLocation location)
        {
            m_location locnew = new m_location();

            if (ModelState.IsValid)
            {
                var cekLokasi = _context.m_location.FirstOrDefault(a => a.name == location.nameLocation && a.parent_id == location.parent_id);

                if (cekLokasi != null)
                {
                    return Json(new { status = "error", message = "*Nama sudah ada untuk wilayah tersebut!" });
                }
                try
                {
                    DateTime date = DateTime.Now;
                    locnew.id = location.id;
                    locnew.name = location.nameLocation;
                    locnew.parent_id = location.parent_id;
                    locnew.location_level_id = location.location_level_id;
                    locnew.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    locnew.modified_on = date;
                    _context.Update(locnew);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_locationExists(locnew.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                //return RedirectToAction(nameof(Index));
                return Json(new { status = "sucess" });
            }
            return View(location);
        }

        // GET: m_location/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var cek = await (from loc in _context.m_location
                             join loclev in _context.m_location_level
                             on loc.location_level_id equals loclev.id
                             join locp in _context.m_location
                             on loc.parent_id equals locp.id
                             join loclevp in _context.m_location_level
                             on locp.location_level_id equals loclevp.id
                             //where loc.is_delete == false 

                             select new VMLocation
                             {
                                 id = loc.id,
                                 location_level_id = loclev.id,
                                 parent_id = locp.id,
                                 nameLocation = loc.name,
                                 nameLocationLevel = loclev.name,
                                 abbreviation = loclev.abbreviation,
                                 created_by = loc.created_by,
                                 created_on = loc.created_on,
                                 modified_by = loc.modified_by,
                                 modified_on = loc.modified_on,
                                 deleted_by = loc.deleted_by,
                                 deleted_on = loc.deleted_on,
                                 is_delete = loc.is_delete,
                                 wilayah = loclev.abbreviation + " " + loc.name + ", " + loclevp.abbreviation + " " + locp.name

                             }).Where(a => a.id == id).FirstOrDefaultAsync();

            //var m_location = await _context.m_location
            //    .FirstOrDefaultAsync(m => m.id == id);
            if (cek == null)
            {
                return NotFound();
            }

            return View(cek);
        }

        // POST: m_location/Delete/5
        [HttpPost]

        public async Task<IActionResult> Delete(VMLocation location)
        {
            m_location locnew = new m_location();
            if (ModelState.IsValid)
            {
                var ceking = CheckLocation(location);
                if (ceking == "0")
                {
                    return Json(new { status = "error", message = "*data sudah ada" });
                }

                try
                {

                    DateTime date = DateTime.Now;

                    locnew.id = location.id;
                    locnew.name = location.nameLocation;
                    locnew.parent_id = location.parent_id;
                    locnew.location_level_id = location.location_level_id;
                    locnew.created_by = location.created_by;
                    locnew.created_on = location.created_on;
                    locnew.modified_by = location.modified_by;
                    locnew.modified_on = location.modified_on;
                    locnew.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    locnew.deleted_on = date;
                    locnew.is_delete = true;
                    _context.Update(locnew);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_locationExists(locnew.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(new { status = "succes" });
            }
            return View(locnew);
        }


        public string CheckLocation(VMLocation mloc)
        {

            //var dloc = _context.m_location.Where(a => a.parent_id == mloc.created_by && a.is_delete == false).FirstOrDefault();
            VMLocation vloc = (from loc in _context.m_location
                               join loclev in _context.m_location_level
                               on loc.location_level_id equals loclev.id
                               join locp in _context.m_location
                               on loc.parent_id equals locp.id
                               join loclevp in _context.m_location_level
                               on locp.location_level_id equals loclevp.id
                               where loc.is_delete == false

                               select new VMLocation
                               {
                                   id = loc.id,
                                   location_level_id = loclev.id,
                                   parent_id = locp.id,
                                   nameLocation = loc.name,
                                   nameLocationLevel = loclev.name,
                                   abbreviation = loclev.abbreviation,
                                   created_by = loc.created_by,
                                   created_on = loc.created_on,
                                   modified_by = loc.modified_by,
                                   modified_on = loc.modified_on,
                                   deleted_by = loc.deleted_by,
                                   deleted_on = loc.deleted_on,
                                   is_delete = loc.is_delete,
                                   wilayah = loclev.abbreviation + " " + loc.name
                               }
                                ).FirstOrDefault(a => a.parent_id == mloc.id);

            if (vloc != null)
            {
                return "0";//GabisaDihapus
            }
            else
            {
                return "1";//BisaDihapus
            }
        }


        public async Task<List<m_location_level>> DataLevel()
        {
            List<m_location_level> list = await (from c in _context.m_location_level select c).ToListAsync();
            list.Insert(0, new m_location_level { id = 0, name = "--Pilih--" });

            return list;

        }

        public async Task<List<VMLocation>> DataWilayah(int location_level_id)
        {
            var relasi = await (from loc in _context.m_location
                                join loclev in _context.m_location_level
                                on loc.location_level_id equals loclev.id
                                join locp in _context.m_location
                                on loc.parent_id equals locp.id
                                join loclevp in _context.m_location_level
                                on locp.location_level_id equals loclevp.id
                                //where loc.is_delete == false 

                                select new VMLocation
                                {
                                    id = loc.id,
                                    location_level_id = loclev.id,
                                    parent_id = loc.id,
                                    nameLocation = loc.name,
                                    nameLocationLevel = loclev.name,
                                    abbreviation = loclev.abbreviation,
                                    created_by = loc.created_by,
                                    created_on = loc.created_on,
                                    modified_by = loc.modified_by,
                                    modified_on = loc.modified_on,
                                    deleted_by = loc.deleted_by,
                                    deleted_on = loc.deleted_on,
                                    is_delete = loc.is_delete,
                                    wilayah = loclev.abbreviation + " " + loc.name + ", " + loclevp.abbreviation + " " + locp.name

                                }).Where(a => location_level_id == a.id).ToListAsync();
            //relasi.Insert(0, new VMLocation { id = 0, wilayah = "--Pilih--" });
            //ViewBag.lokasi = relasi;
            return relasi;
        }

        //public async Task<List<VMLocation>> DataWilayah(int location_level_id)
        //{
        //    List<VMLocation> list = await _context.VMLocation.
        //                                Where(a => a.is_delete == false && a.id.Equals(location_level_id)).ToListAsync();
        //    list.Insert(0, new VMLocation { id = 0, nameLocation = "--Lu Pilih Dah--" });
        //    return list;
        //}

        public async Task<JsonResult> GetDataVariantAjax(int idLoc)
        {
            List<VMLocation> list = await
                (
                from loclev in _context.m_location_level
                join loc in _context.m_location
                on loclev.id equals loc.location_level_id
                where loc.location_level_id == idLoc - 1 || loc.location_level_id == idLoc - 2 /*&& loc.is_delete == false*/
                select new VMLocation
                {
                    id = loc.id,
                    location_level_id = loclev.id,
                    nameLocation = loc.name,
                    nameLocationLevel = loclev.name,
                    wilayah = loclev.abbreviation + " " + loc.name,
                    parent_id = loc.parent_id
                }).ToListAsync();

            list.Insert(0, new VMLocation { id = 0, wilayah = "--Lu Pilih Dah--" });
            return Json(list);
        }

        private bool m_locationExists(long id)
        {
            return _context.m_location.Any(e => e.id == id);
        }

        public IActionResult FalseDelete(m_location aa)
        {
            return View(aa);
        }
    }
}
