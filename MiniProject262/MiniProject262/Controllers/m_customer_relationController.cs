﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using Microsoft.AspNetCore.Http;
using MiniProject262.ViewModels;


namespace MiniProject262.Controllers
{
    public class m_customer_relationController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_customer_relationController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: m_customer_relation
        public async Task<IActionResult> Index(string searchString)
        {
            m_admin dummy = new m_admin
            {
                id = 200
            };

            ViewBag.dummy = dummy.id;
            ViewData["CurrentFilter"] = searchString;

            var students = from s in _context.m_customer_relation
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                students = students.Where(s => s.name.Contains(searchString));
            }
            return View(await students.AsNoTracking().Where(a => a.is_delete == false).ToListAsync());
        }

        // GET: m_customer_relation/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_customer_relation = await _context.m_customer_relation
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_customer_relation == null)
            {
                return NotFound();
            }

            return View(m_customer_relation);
        }

        // GET: m_customer_relation/Create
        public IActionResult Create()
        {

            return View();
        }

        // POST: m_customer_relation/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_customer_relation m_customer_relation)
        {
            if (ModelState.IsValid)
            {

                m_customer_relation.created_by = (long)HttpContext.Session.GetInt32("iduser");
                var ceking = _context.m_customer_relation.FirstOrDefault(a => a.name == m_customer_relation.name && a.is_delete == false);
                if (ceking != null)
                {

                    return Json(new { status = "error", message = "*data sudah ada" });
                }


                DateTime dt = DateTime.Now;
                m_customer_relation.created_on = dt;

                _context.Add(m_customer_relation);
                await _context.SaveChangesAsync();
                return Json(new { status = "success" });
            }
            return View(m_customer_relation);
        }

        // GET: m_customer_relation/Edit/5
        public async Task<IActionResult> Edit(long? id, string name)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_customer_relation = await _context.m_customer_relation.FindAsync(id);
            if (m_customer_relation == null)
            {
                return NotFound();
            }
            return View(m_customer_relation);
        }

        // POST: m_customer_relation/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, string name, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_customer_relation m_customer_relation)
        {
            if (id != m_customer_relation.id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {

                try
                {

                    m_customer_relation.modified_by = (long)HttpContext.Session.GetInt32("iduser");

                    var ceking = _context.m_customer_relation.FirstOrDefault(a => a.name == m_customer_relation.name && a.is_delete == false);
                    if (ceking != null)
                    {
                        return Json(new { status = "error", message = "*data sudah ada" });
                    }
                    DateTime dt = DateTime.Now;
                    m_customer_relation.modified_on = dt;

                    _context.Update(m_customer_relation);
                    await _context.SaveChangesAsync();


                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_customer_relationExists(m_customer_relation.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(new { status = "success" });
            }
            return View(m_customer_relation);
        }

        // GET: m_customer_relation/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_customer_relation = await _context.m_customer_relation.FindAsync(id);
            if (m_customer_relation == null)
            {
                return NotFound();
            }
            return View(m_customer_relation);

        }

        // POST: m_customer_relation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_customer_relation m_customer_relation)
        {

            if (id != m_customer_relation.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                m_customer_relation.is_delete = true;
                try
                {
                    m_customer_relation.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    m_customer_relation.deleted_on = dt;

                    _context.Update(m_customer_relation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_customer_relationExists(m_customer_relation.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_customer_relation);
        }

        private bool m_customer_relationExists(long id)
        {
            return _context.m_customer_relation.Any(e => e.id == id);
        }
    }
}
