﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;
using Microsoft.AspNetCore.Mvc.Routing;

namespace MiniProject262.Controllers
{
    public class ProfileController : Controller
    {
        private readonly MiniProjectContext _context;

        public ProfileController(MiniProjectContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            long prof = (long)HttpContext.Session.GetInt32("iduser");
            ViewBag.iduser = prof;
            //int prof = 4;
			VMProfil mpr = await (from mu in _context.m_user

								 join bi in _context.m_biodata
									 on mu.biodata_id equals bi.id

								 join ro in _context.m_role
									 on mu.role_id equals ro.id

								 join mc in _context.m_customer
									 on bi.id equals mc.biodata_id

                                 //join rp in _context.t_reset_password
                                 //   on mu.password equals rp.old_password
                                 join mdr in _context.m_doctor
                                     on bi.id equals mdr.biodata_id

                                 join mde in _context.m_doctor_education
                                     on mdr.id equals mde.doctor_id

                                 join medl in _context.m_education_level
                                    on mde.education_level_id equals medl.id

                                join tdrof in _context.t_doctor_office
                                    on mdr.id equals tdrof.doctor_id

                                join mmedfa in _context.m_medical_facility
                                    on tdrof.mdeical_facility_id equals mmedfa.id

                                join mmedfasc in _context.m_medical_facility_schedule
                                    on mmedfa.id equals mmedfasc.medical_facility_id

                                join cudrspe in _context.t_current_doctor_specialization
                                    on mdr.id equals cudrspe.doctor_id

                                join mspec in _context.m_specialization
                                    on cudrspe.specialization_id equals mspec.id

                                join cuchat in _context.t_customer_chat
                                    on mdr.id equals cuchat.doctor_id
      
								 where mu.id == prof && mu.is_delete == false && bi.is_delete == false
								 select new VMProfil
								 {
									 id = mu.id,
									 biodata_id = bi.id,
									 role_id = ro.id,
									 email = mu.email,
									 password = mu.password,
									 login_attempt = mu.login_attempt,
									 is_locked = mu.is_locked,
									 last_login = mu.last_login,
									 name = ro.name,
									 code = ro.code,
									 fullname = bi.fullname,
									 mobile_phone = bi.mobile_phone,
									 image = bi.image,
									 image_path = bi.image_path,
									 created_by = mu.created_by,
									 created_on = mu.created_on,
									 modified_by = mu.modified_by,
									 modified_on = mu.modified_on,
									 deleted_by = mu.deleted_by,
									 deleted_on = mu.deleted_on,
									 is_delete = mu.is_delete,
									 dob = mc.dob,
                                     customer_id = mc.id,

                                     institution_name = mde.institution_name,
                                     major = mde.major,
                                     start_year = mde.start_year,
                                     end_year = mde.end_year,
                                     is_last_edu = mde.is_last_education,
                                     name_edu_lv = medl.name,
                                     id_edu_level = medl.id,
                                     id_medic_facility = mmedfa.id,
                                     name_medic_facility = mmedfa.name,
                                     day_mf_schedule = mmedfasc.day,
                                     code_role = ro.code,
                                     specialization = mspec.name,
                                     doctor_office_specialization = tdrof.specialization


                                     //old_password=rp.old_password,
                                     //new_password=rp.new_password,
                                     //reset_for=rp.reset_for



                                 }).FirstOrDefaultAsync();

                



            var sc = await _context.m_user.FindAsync(prof);
            var lama = DateTime.Now - sc.created_on;

            //DateTime taun = new DateTime();
            DateTime Taun = sc.created_on;
            string lTaun = Taun.ToString("yyyy");
            ViewBag.since = lTaun;
            ViewBag.Lama = lama;
            return View(mpr);
        }

        public async Task<IActionResult> Pasien(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pSize)
        {
            var bioid = HttpContext.Session.GetInt32("bioid");

            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<VMPasien>.PageSizeList();
            ViewBag.sort = sortingPasien();

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var list = from bio in _context.m_biodata
                       join cus in _context.m_customer
                       on bio.id equals cus.biodata_id
                       join cusmem in _context.m_customer_member
                       on cus.id equals cusmem.customer_id
                       join rel in _context.m_customer_relation
                       on cusmem.customer_relation_id equals rel.id
                       where cusmem.is_delete == false && cusmem.parent_biodata_id == bioid

                       select new VMPasien
                       {
                           id_customer_member = cusmem.id,
                           customer_id = cus.id,
                           fullname = bio.fullname,
                           relation_name = rel.name,
                           dob = cus.dob,
                           Age = CalculateAge(cus.dob)
                       };




            if (!string.IsNullOrEmpty(searchString))
            {
                list = list.Where(c => c.fullname.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    list = list.OrderByDescending(c => c.fullname);
                    break;
                case "name_asc":
                    list = list.OrderBy(c => c.fullname);
                    break;
                case "age_asc":
                    list = list.OrderByDescending(c => c.dob);
                    break;
                default:
                    list = list.OrderBy(c => c.fullname);
                    break;
            }
            //int pageSize = 3;

            int pageSize = pSize ?? 5;
            //chat();
            //janji();
            return View(PageinatedList<VMPasien>.CreateAsync(list.AsNoTracking().ToList(), pageNumber ?? 1, pageSize));
        }

        public static IEnumerable<SelectListItem> sortingPasien()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Nama", Value = "name_asc"},
                new SelectListItem{Text = "Umur", Value = "age_asc"}
            };
            return items;
        }

        public List<VMPasien> chat()
        {
            var prodCounts = (from cvm in _context.m_customer
                              join chat in _context.t_customer_chat
                              on cvm.id equals chat.customer_id
                              join chathis in _context.t_customer_chat_history
                              on chat.id equals chathis.customer_chat_id
                              group chathis by cvm.id into catGp
                              select new VMPasien
                              {
                                  customer_id = catGp.Key,
                                  chat = catGp.Count()
                              }).ToList();
            ViewBag.ccd = prodCounts;
            return prodCounts;
        }

        public List<VMPasien> janji()
        {
            var prodCounts = (from cvm in _context.m_customer
                              join app in _context.t_appointment
                              on cvm.id equals app.customer_id
                              group app by cvm.id into catGp
                              select new VMPasien
                              {
                                  customer_id = catGp.Key,
                                  chat = catGp.Count()
                              }).ToList();
            ViewBag.aad = prodCounts;
            return prodCounts;
        }



        public async Task<IActionResult> TambahPasien()
        {
            await DropDownGolDar();
            await DropDownRelasi();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TambahPasien(VMPasien pasien)
        {
            m_biodata biodata = new m_biodata();
            m_customer customer = new m_customer();
            m_customer_member cusmem = new m_customer_member();
            if (ModelState.IsValid)
            {
                DateTime date = DateTime.Now;
                biodata.fullname = pasien.fullname;
                biodata.created_on = date;
                _context.Add(biodata);
                await _context.SaveChangesAsync();

                m_biodata bio = await (from c in _context.m_biodata where c.fullname == pasien.fullname select c).FirstOrDefaultAsync();

                customer.dob = pasien.dob;
                customer.biodata_id = bio.id;
                customer.blood_group_id = pasien.blood_group_id;
                customer.height = pasien.height;
                customer.weight = pasien.weight;
                customer.gender = pasien.gender;
                customer.rhesus_type = pasien.rhesus_type;
                customer.created_on = date;
                _context.Add(customer);
                await _context.SaveChangesAsync();
                m_customer cus = await (from c in _context.m_customer where c.biodata_id == bio.id select c).FirstOrDefaultAsync();

                cusmem.parent_biodata_id = HttpContext.Session.GetInt32("bioid");
                cusmem.customer_id = cus.id;
                cusmem.customer_relation_id = pasien.customer_relation_id;
                cusmem.created_by = (long)HttpContext.Session.GetInt32("iduser");
                cusmem.created_on = date;
                _context.Add(cusmem);
                await _context.SaveChangesAsync();

                return RedirectToAction("Pasien");
            }
            return View(pasien);
        }
        public async Task<IActionResult> EditPasien(int Id)
        {
            await DropDownGolDar();
            await DropDownRelasi();
            if (Id == 0)
            {
                return NotFound();
            }
            // var product = await _context.Products.FindAsync(Id);
            VMPasien product = await dataPasien(Id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }


        [HttpPost]
        public async Task<IActionResult> EditPasien(VMPasien pasien)
        {
            m_biodata biodata = new m_biodata();
            m_customer customer = new m_customer();
            m_customer_member cusmem = new m_customer_member();
            if (ModelState.IsValid)
            {
                DateTime date = DateTime.Now;
                biodata.id = (long)pasien.biodata_id;
                biodata.fullname = pasien.fullname;
                biodata.created_on = pasien.created_on_biodata;
                biodata.modified_on = date;
                _context.Update(biodata);
                await _context.SaveChangesAsync();

                m_biodata bio = await (from c in _context.m_biodata where c.fullname == pasien.fullname select c).FirstOrDefaultAsync();

                customer.id = (long)pasien.customer_id;
                customer.created_on = pasien.created_on_customer;

                customer.dob = pasien.dob;

                customer.biodata_id = bio.id;
                customer.blood_group_id = pasien.blood_group_id;
                customer.height = pasien.height;
                customer.weight = pasien.weight;
                customer.gender = pasien.gender;
                customer.rhesus_type = pasien.rhesus_type;
                customer.modified_on = date;

                _context.Update(customer);
                await _context.SaveChangesAsync();
                m_customer cus = await (from c in _context.m_customer where c.biodata_id == bio.id select c).FirstOrDefaultAsync();
                cusmem.id = pasien.id_customer_member;
                cusmem.created_on = pasien.created_on_cusmem;
                cusmem.parent_biodata_id = pasien.parent_biodata_id;
                cusmem.customer_id = cus.id;
                cusmem.customer_relation_id = pasien.customer_relation_id;
                cusmem.modified_on = date;
                cusmem.created_by = pasien.created_by_cusmem;
                cusmem.created_on = pasien.created_on_cusmem;
                cusmem.modified_by = (long)HttpContext.Session.GetInt32("iduser"); 
                _context.Update(cusmem);
                await _context.SaveChangesAsync();

                return RedirectToAction("Pasien");
            }
            await DropDownGolDar();
            await DropDownRelasi();
            return View(pasien);
        }
        public async Task<IActionResult> DeletePasien(int Id)
        {
            await DropDownGolDar();
            await DropDownRelasi();
            if (Id == 0)
            {
                return NotFound();
            }

            VMPasien pasien = await dataPasien(Id);

            if (pasien == null)
            {
                return NotFound();
            }

            return View(pasien);
        }

        [HttpPost]
        public async Task<IActionResult> DeletePasien(VMPasien pasien)
        {
            m_customer_member cusmem = new m_customer_member();
            if (ModelState.IsValid)
            {
                cusmem.id = pasien.id_customer_member;
                cusmem.created_on = pasien.created_on_cusmem;
                cusmem.modified_on = pasien.modified_on_cusmem;
                cusmem.parent_biodata_id = pasien.parent_biodata_id;
                cusmem.customer_id = pasien.customer_id;
                cusmem.customer_relation_id = pasien.customer_relation_id;
                cusmem.created_by = pasien.created_by_cusmem;
                cusmem.created_on = pasien.created_on_cusmem;
                cusmem.modified_by = pasien.modified_by_cusmem;
                cusmem.modified_on = pasien.modified_on_cusmem;
                cusmem.created_by = pasien.created_by_cusmem;
                cusmem.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                cusmem.deleted_on = DateTime.Now;
                cusmem.is_delete = true;
                _context.Update(cusmem);
                await _context.SaveChangesAsync();

                return RedirectToAction("Pasien");
            }
            await DropDownGolDar();
            await DropDownRelasi();
            return View(pasien);
        }


        public async Task<List<m_blood_group>> DropDownGolDar()
        {
            List<m_blood_group> goldar = await (from c in _context.m_blood_group where c.is_delete == false select c).ToListAsync();
            goldar.Insert(0, new m_blood_group { id = 0, code = "-- Pilih --" });
            ViewBag.message = goldar;
            return goldar;
        }
        public async Task<List<m_customer_relation>> DropDownRelasi()
        {
            List<m_customer_relation> relasi = await (from c in _context.m_customer_relation where c.is_delete == false select c).ToListAsync();
            ViewBag.relation = relasi;
            return relasi;
        }

        public async Task<VMPasien> dataPasien(int Id)
        {
            VMPasien list = await (from bio in _context.m_biodata
                                   join cus in _context.m_customer
                                   on bio.id equals cus.biodata_id
                                   join blood in _context.m_blood_group
                                   on cus.blood_group_id equals blood.id
                                   join cusmem in _context.m_customer_member
                                   on cus.id equals cusmem.customer_id
                                   join rel in _context.m_customer_relation
                                   on cusmem.customer_relation_id equals rel.id
                                   where cus.is_delete == false
                                   select new VMPasien
                                   {
                                       id_customer_member = cusmem.id,
                                       biodata_id = bio.id,
                                       customer_id = cus.id,
                                       parent_biodata_id = cusmem.parent_biodata_id,
                                       dob = cus.dob,
                                       fullname = bio.fullname,

                                       gender = cus.gender,
                                       rhesus_type = cus.rhesus_type,
                                       weight = cus.weight,
                                       height = cus.height,
                                       blood_group_id = cus.blood_group_id,
                                       code = blood.code,
                                       customer_relation_id = rel.id,
                                       relation_name = rel.name,
                                       created_on_biodata = bio.created_on,
                                       created_on_cusmem = cusmem.created_on,
                                       created_on_customer = cus.created_on,
                                       modified_on_cusmem = cusmem.modified_on,
                                       modified_on_biodata = bio.modified_on,
                                       modified_on_customer = cus.modified_on,

                                       created_by_biodata = bio.created_by,
                                       created_by_cusmem = cusmem.created_by,
                                       created_by_customer = cus.created_by,
                                       modified_by_biodata = bio.modified_by,
                                       modified_by_cusmem = cusmem.modified_by,
                                       modified_by_customer = cus.modified_by

                                   }).FirstOrDefaultAsync(a => a.id_customer_member == Id);
            return list;
        }
        public async Task<List<VMPasien>> dataPasienlist(int Id)
        {
            List<VMPasien> list = await (from bio in _context.m_biodata
                                         join cus in _context.m_customer
                                         on bio.id equals cus.biodata_id
                                         join blood in _context.m_blood_group
                                         on cus.blood_group_id equals blood.id
                                         join cusmem in _context.m_customer_member
                                         on cus.id equals cusmem.customer_id
                                         join rel in _context.m_customer_relation
                                         on cusmem.customer_relation_id equals rel.id
                                         where cus.is_delete == false
                                         select new VMPasien
                                         {
                                             id_customer_member = cusmem.id,
                                             biodata_id = bio.id,
                                             customer_id = cus.id,
                                             parent_biodata_id = cusmem.parent_biodata_id,
                                             dob = cus.dob,
                                             fullname = bio.fullname,

                                             gender = cus.gender,
                                             rhesus_type = cus.rhesus_type,
                                             weight = cus.weight,
                                             height = cus.height,
                                             blood_group_id = cus.blood_group_id,
                                             code = blood.code,
                                             customer_relation_id = rel.id,
                                             relation_name = rel.name,
                                             created_on_biodata = bio.created_on,
                                             created_on_cusmem = cusmem.created_on,
                                             created_on_customer = cus.created_on,
                                             modified_on_cusmem = cusmem.modified_on

                                         }).ToListAsync();
            return list;
        }
        public async Task<IActionResult> DeleteSelected(string data)
        {
            List<VMPasien> pasienlist = new List<VMPasien>();
            string[] dataarr = data.Split(",");
            if (data.Length == 0)
            {
                return NotFound();
            }
            // var product = await _context.Products.FindAsync(Id);
            for (int i = 0; i < dataarr.Length; i++)

            {
                VMPasien pasien = await dataPasien(int.Parse(dataarr[i]));
                pasienlist.Add(pasien);
            }
            //List<VMPasien> pasien = await dataPasienlist(Id);

            if (pasienlist == null)
            {
                return NotFound();
            }

            return View(pasienlist);
        }

        [HttpPost, ActionName("DeleteSelected")]
        public async Task<IActionResult> DeleteSelectedConfirm(IEnumerable<int> deleteselect)
        {
            //int count = deleteselect.Count();
            //for (int i = 0; i < count ; i++)
            //{
            //    VMPasien pasien = await dataPasien(int.Parse(deleteselect{ i}));

            //    pasienlist.Add(pasien);
            //}
            DateTime date = DateTime.Now;

            foreach (var item in deleteselect)
            {
                m_customer_member cusmem = new m_customer_member();
                VMPasien pasien = await dataPasien(item);
                cusmem.id = pasien.id_customer_member;
                cusmem.parent_biodata_id = pasien.parent_biodata_id;
                cusmem.customer_id = pasien.customer_id;
                cusmem.customer_relation_id = pasien.customer_relation_id;
                cusmem.created_on = pasien.created_on_cusmem;
                cusmem.modified_on = pasien.modified_on_cusmem;
                cusmem.deleted_on = date;
                cusmem.is_delete = true;
                _context.Update(cusmem);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Pasien");
        }


        private static int CalculateAge(DateTime? dob)
        {
            int age = 0;
            DateTime doba = Convert.ToDateTime(dob);

            age = DateTime.Now.Year - doba.Year;
            if (DateTime.Now.DayOfYear < doba.DayOfYear)
            {
                age = age - 1;
            }

            return age;
        }

        public async Task<List<VMChat>> GetChat()
        {
            var result = await (from a in _context.t_customer_chat
                                join b in _context.t_customer_chat_history
                                  on a.id equals b.customer_chat_id
                                select new VMChat
                                {
                                    id = a.id,
                                    chathistoryid = b.id,
                                    customer_id = a.id
                                }).ToListAsync();
            return result;

        }


        // GET: m_payment_method/Edit/5
        public IActionResult Edit(VMProfil vmprofil)
        {

            if (vmprofil == null)
            {
                return NotFound();
            }
            return View(vmprofil);
        }

        public IActionResult EditEmail(VMProfil vmprofil)
        {
            if (vmprofil == null)
            {
                return NotFound();
            }
            return View(vmprofil);
        }

        public IActionResult EditPassword(VMProfil vmprofil)
        {
            if (vmprofil == null)
            {
                return NotFound();
            }
            return View(vmprofil);
        }

        // POST: m_payment_method/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveConfirmed(VMProfil vmprofil)
        {
            var vuser = await _context.m_user.FindAsync(vmprofil.id);
            var vbiodata = await _context.m_biodata.FindAsync(vmprofil.biodata_id);
            var vcustomer = await _context.m_customer.FindAsync(vmprofil.customer_id);
            //var tpass = await _context.m_user.FindAsync(vmprofil.password);


            vbiodata.fullname = vmprofil.fullname;
            vcustomer.dob = vmprofil.dob;
            vbiodata.mobile_phone = vmprofil.mobile_phone;
            vuser.email = vmprofil.email;
            vuser.password = vmprofil.password;
            //tpass.ol

            if (ModelState.IsValid)
            {
                try
                {
                    vbiodata.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    vbiodata.modified_on = dt;

                    _context.Update(vuser);
                    _context.Update(vbiodata);
                    _context.Update(vcustomer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {

                    return NotFound();
                }
                return Json(new { status = "success" });
            }
            return View(vmprofil);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(VMProfil vmprofil)
        {
            var vuser = await _context.m_user.FindAsync(vmprofil.id);
            var vbiodata = await _context.m_biodata.FindAsync(vmprofil.biodata_id);
            var vcustomer = await _context.m_customer.FindAsync(vmprofil.customer_id);
            //var tpass = await _context.m_user.FindAsync(vmprofil.password);

            vbiodata.fullname = vmprofil.fullname;
            vcustomer.dob = vmprofil.dob;
            vbiodata.mobile_phone = vmprofil.mobile_phone;
            vuser.email = vmprofil.email;
            vuser.password = vmprofil.password;
            //tpass.ol

            if (ModelState.IsValid)
            {
                try
                {
                    vbiodata.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    vbiodata.modified_on = dt;

                    _context.Update(vuser);
                    _context.Update(vbiodata);
                    _context.Update(vcustomer);
                    await _context.SaveChangesAsync();
                }
                //return RedirectToAction("Index");
                catch (DbUpdateConcurrencyException)
                {

                    return NotFound();
                }
                return RedirectToAction("Index");
            }
            return View(vmprofil);
        }
        public IActionResult editprofil(VMProfil vMProfil)
        {
            return View(vMProfil);
        }
        public async Task<IActionResult> edit1(VMProfil vmProfil)
        {
            var vuser = await _context.m_user.FindAsync(vmProfil.id);
            var vbiodata = await _context.m_biodata.FindAsync(vmProfil.biodata_id);
            var vcustomer = await _context.m_customer.FindAsync(vmProfil.customer_id);
            //var tpass = await _context.m_user.FindAsync(vmProfil.password);


            vmProfil.fullname = vbiodata.fullname;
            vmProfil.dob = vcustomer.dob;
            vmProfil.mobile_phone = vbiodata.mobile_phone;
            vmProfil.email = vuser.email;
            vmProfil.password = vuser.password;
            //vmProfil.old_password = vuser.password;
            if (vmProfil == null)
            {
                return NotFound();
            }
            return Json(new { status = "success", data = vmProfil });
        }
        public IActionResult SendEmail(VMProfil customer)
        {
            //var emailexist = ValidateEmailId(customer.email);
            //if (emailexist == null)
            //{

            Random rnd = new Random();
            string otp = rnd.Next(1000, 9999).ToString();
            customer.otp = otp;

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

            client.EnableSsl = true;

            MailAddress from = new MailAddress("tupakshakur12345@gmail.com", "[tupac shak]");

            MailAddress to = new MailAddress(customer.email);

            MailMessage message = new MailMessage(from, to);

            message.Body = "This is a test e-mail message sent using gmail as a relay server ";

            message.Subject = "Gmail test email with SSL and Credentials";

            NetworkCredential myCreds = new NetworkCredential("tupakshakur12345@gmail.com", "udingambut", "");

            client.Credentials = myCreds;

            try
            {
                client.Send(message);
                return Json(new { status = "success", data = customer });
            }

            catch (Exception)

            {
                return Json(new { status = "error" });
            }

            //return View();
            //}
            //else
            //{
            //    return Json(new { status = "error" });
            //}
        }
        public IActionResult OTP(VMProfil _email)
        {
            return View(_email);
        }
        public IActionResult Newpassword(VMProfil _pass)
        {
            return View(_pass);
        }
        private m_user ValidateEmailId(string emailId)
        {
            return _context.m_user.FirstOrDefault(a => a.email == emailId);
        }

        public async Task<IActionResult> WithdrawBal()
        {
            var id = HttpContext.Session.GetInt32("iduser");
            if (id == null)
            {
                return RedirectToAction("ErrorPage");
            }
            var wdn = await (from c in _context.m_wallet_default_nominal where c.is_delete == false select c).ToListAsync();
            ViewBag.wdn = wdn;          
            List<VMCusWallet> customnom = await (from wal in _context.t_customer_wallet
                                                 join cus in _context.m_customer
                                                 on wal.customer_id equals cus.id
                                                 join ccn in _context.t_customer_custom_nominal
                                                 on cus.id equals ccn.customer_id into cusnom
                                                 from c in cusnom.DefaultIfEmpty()
                                                 join m in _context.m_user
                                                 on cus.biodata_id equals m.biodata_id
                                                 where m.id == id && id != null
                                                 select new VMCusWallet
                                                 {
                                                     id = wal.id,
                                                     customer_id = cus.id,
                                                     pin = wal.pin,
                                                     balance = wal.balance,
                                                     barcode = wal.barcode,
                                                     points = wal.points,
                                                     cus_nominal_id = c.id,
                                                     cus_nominal = c.nominal,
                                                 }).ToListAsync();
            ViewBag.saldo = customnom.Select(a => a.balance).FirstOrDefault();
            ViewBag.idcus = customnom.Select(a => a.customer_id).FirstOrDefault();
            return View(customnom);
        }

        public async Task<IActionResult> WithdrawConfirmed(VMCusWallet cus)
        {
            if (cus.customer_id == null)
            {
                return RedirectToAction("ErrorPage");
            }
            var iduser = HttpContext.Session.GetInt32("iduser");
            var username = HttpContext.Session.GetString("username");
            DateTime dt = DateTime.Now;
            Random rnd = new Random();
            int OTP = rnd.Next(100000, 999999);
            t_customer_wallet_withdraw cww = new t_customer_wallet_withdraw();
            t_customer_custom_nominal ccn = new t_customer_custom_nominal();
            t_token tok = new t_token();
            string tokenn = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            var us = await (from a in _context.m_user where a.id == iduser && a.is_delete == false select a).FirstOrDefaultAsync();
            var cw = await (from c in _context.t_customer_wallet where c.customer_id == cus.customer_id && c.is_delete == false select c).FirstOrDefaultAsync();
            if (cus.cus_nominal_id != 0)
            {
                cww.customer_id = cus.customer_id;
                cww.amount = cus.amount;
                cww.bank_name = "Minimarket";
                cww.account_number = iduser.ToString();
                cww.account_name = username;
                cww.otp = OTP;
                cww.created_by = (long)cus.customer_id;
                cww.created_on = dt;
                _context.Add(cww);
                await _context.SaveChangesAsync();

                cw.balance -= cus.amount;
                cw.modified_by = (long)cus.customer_id;
                cw.modified_on = dt;
                _context.Update(cw);
                await _context.SaveChangesAsync();

                tok.email = us.email;
                tok.user_id = us.id;
                tok.token = tokenn;
                tok.expired_on = dt.AddHours(1);
                tok.is_expired = false;
                tok.used_for = "Tarik Saldo";
                tok.created_by = (long)cus.customer_id;
                tok.created_on = dt;
                _context.Add(tok);
                await _context.SaveChangesAsync();
                return Json(new { status = "success", data = OTP });
            }
            else if (cus.wallet_default_nominal_id != null)
            {
                cww.customer_id = cus.customer_id;
                cww.wallet_default_nominal_id = cus.wallet_default_nominal_id;
                cww.amount = cus.amount;
                cww.bank_name = "Minimarket";
                cww.account_number = iduser.ToString();
                cww.account_name = username;
                cww.otp = OTP;
                cww.created_by = (long)cus.customer_id;
                cww.created_on = dt;
                _context.Add(cww);
                await _context.SaveChangesAsync();

                cw.balance -= cus.amount;
                cw.modified_by = (long)cus.customer_id;
                cw.modified_on = dt;
                _context.Update(cw);
                await _context.SaveChangesAsync();

                tok.email = us.email;
                tok.user_id = us.id;
                tok.token = tokenn;
                tok.expired_on = dt.AddHours(1);
                tok.is_expired = false;
                tok.used_for = "Tarik Saldo";
                tok.created_by = (long)cus.customer_id;
                tok.created_on = dt;
                _context.Add(tok);
                await _context.SaveChangesAsync();
                return Json(new { status = "success", data = OTP });
            }
            else
            {
                cww.customer_id = cus.customer_id;
                cww.amount = cus.amount;
                cww.bank_name = "Minimarket";
                cww.account_number = iduser.ToString();
                cww.account_name = username;
                cww.otp = OTP;
                cww.created_by = (long)cus.customer_id;
                cww.created_on = dt;
                _context.Add(cww);
                await _context.SaveChangesAsync();

                cw.balance -= cus.amount;
                cw.modified_by = (long)cus.customer_id;
                cw.modified_on = dt;
                _context.Update(cw);
                await _context.SaveChangesAsync();

                ccn.customer_id = cus.customer_id;
                ccn.nominal = cus.cus_nominal;
                ccn.created_by = (long)cus.customer_id;
                ccn.created_on = dt;
                _context.Add(ccn);
                await _context.SaveChangesAsync();

                tok.email = us.email;
                tok.user_id = us.id;
                tok.token = tokenn;
                tok.expired_on = dt.AddHours(1);
                tok.is_expired = false;
                tok.used_for = "Tarik Saldo";
                tok.created_by = (long)cus.customer_id;
                tok.created_on = dt;
                _context.Add(tok);
                await _context.SaveChangesAsync();
                return Json(new { status = "success", data = OTP });
            }
        }

        public IActionResult WdSuccess(VMCusWallet vmco)
        {
            VMCusWallet vmcu = new VMCusWallet();
            vmcu.otp = (int)vmco.id;
            return View(vmcu);
        }

        public IActionResult PinView(VMCusWallet vmca)
        {
            if (vmca.customer_id == null)
            {
                return RedirectToAction("ErrorPage");
            }
            return View(vmca);
        }

        public IActionResult AutenSuccess()
        {
            return View();
        }

        public IActionResult AutenFailed()
        {
            return View();
        }

        public async Task<IActionResult> CheckPin(VMCusWallet vmc)
        {
            if (vmc.customer_id == null)
            {
                return RedirectToAction("ErrorPage");
            }
            else
            {
                VMCusWallet vmcus = await DataCust(vmc.customer_id);
                var cust = VerifyPin(vmc.customer_id, vmc.pin);
                if (vmc.input_attempt != 3)
                {    
                    if (cust != null)
                    {
                        vmcus.input_attempt = 0;
                        return Json(new { status = "success" });
                    }
                    else
                    {
                        //vmcus.input_attempt += 1;
                        return Json(new { status = "error", message = "* pin salah" });
                    }
                }
                else
                {
                    var id = HttpContext.Session.GetInt32("iduser");
                    var vuser = await (from c in _context.m_user where c.id == id && c.is_delete == false select c).FirstOrDefaultAsync();
                    DateTime dt = DateTime.Now;
                    vuser.is_locked = true;
                    vuser.modified_on = dt;
                    _context.Update(vuser);
                    await _context.SaveChangesAsync();
                    return Json(new { status = "success", messages = "locked" });
                }
            }
        }

        public async Task<VMCusWallet> DataCust(long? customer_id)
        {
            var id = HttpContext.Session.GetInt32("iduser");
            VMCusWallet vmc = await (from cw in _context.t_customer_wallet
                                     join cu in _context.m_customer
                                     on cw.customer_id equals cu.id
                                     join mu in _context.m_user
                                     on cu.biodata_id equals mu.biodata_id
                                     where mu.id == id && cw.customer_id == customer_id && mu.is_delete == false
                                     select new VMCusWallet
                                     {
                                         id = cw.id,
                                         customer_id = cu.id,
                                         biodata_id = mu.biodata_id,
                                         pin = cw.pin,
                                         balance = cw.balance,
                                         is_locked = mu.is_locked,
                                         input_attempt = 0,
                                         modified_on = mu.modified_on
                                     }).FirstOrDefaultAsync();
            return vmc;
        }

        private t_customer_wallet VerifyPin(long? customer_id, string pin)
        {
            return _context.t_customer_wallet.FirstOrDefault(a => a.customer_id == customer_id && a.pin == pin);
        }

        public IActionResult CustomNom(long? id)
        {
            VMCusWallet cust = new VMCusWallet();
            cust.customer_id = id;
            return View(cust);
        }


        public async Task<IActionResult> CusNom(VMCusWallet cust)
        {
            if (cust == null)
            {
                return RedirectToAction("ErrorPage");
            }
            var input = cust.cus_nominal;
            var wdn = await (from c in _context.m_wallet_default_nominal where c.is_delete == false select c.nominal).ToListAsync();
            var noms = wdn.ToArray();
            int counter = 0;
            for (int i = 0; i < noms.Length; i++)
            {
                if(input == noms[i])
                {
                    counter++;
                }
            }
            var custnominal = CustNominalExists(cust.customer_id);
            if (counter != 0)
            {
                return Json(new { status = "error", message = "* nominal lain sudah ada" });
            }
            else if (input == custnominal.nominal)
            {
                return Json(new { status = "error", message = "* nominal lain sudah ada" });
            }
            else
            {
                return Json(new { status = "success" });
            }
        }
        private t_customer_custom_nominal CustNominalExists(long? customer_id)
        {
            return _context.t_customer_custom_nominal.FirstOrDefault(a => a.customer_id == customer_id && a.is_delete == false);
        }

        public async Task<IActionResult> Alamat(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pSize)
        {
            var bioid = HttpContext.Session.GetInt32("bioid");

            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<VMPasien>.PageSizeList();
            ViewBag.srtalamat = sortingAlamat();

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var alamat = from almt in _context.m_biodata_address
                         join loc in _context.m_location
                         on almt.location_id equals loc.id
                         join loclev in _context.m_location_level
                         on loc.location_level_id equals loclev.id
                         join locp in _context.m_location
                         on loc.parent_id equals locp.id
                         join loclevp in _context.m_location_level
                         on locp.location_level_id equals loclevp.id
                         where almt.is_delete == false && almt.biodata_id == bioid
                         select new VMAlamat
                         {
                             id = almt.id,
                             label = almt.label,
                             recipient = almt.recipient,
                             recipient_phone_number = almt.recipient_phone_number,
                             address = almt.address,
                             wilayah = loclev.abbreviation + "." + loc.name + " " + loclevp.abbreviation + "." + locp.name
                         };

            ViewBag.alamat = alamat;

            if (!string.IsNullOrEmpty(searchString))
            {
                alamat = alamat.Where(c => c.recipient.Contains(searchString) || c.address.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    alamat = alamat.OrderByDescending(c => c.label);
                    break;
                case "label_asc":
                    alamat = alamat.OrderBy(c => c.label);
                    break;
                case "alamat_asc":
                    alamat = alamat.OrderBy(c => c.address);
                    break;
                default:
                    alamat = alamat.OrderBy(c => c.label);
                    break;
            }
            //int pageSize = 3;

            int pageSize = pSize ?? 5;
            return View(PageinatedList<VMAlamat>.CreateAsync(alamat.AsNoTracking().ToList(), pageNumber ?? 1, pageSize));
        }
        public static IEnumerable<SelectListItem> sortingAlamat()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Label", Value = "label_asc"},
                new SelectListItem{Text = "Alamat", Value = "alamat_asc"}
            };
            return items;
        }

        public async Task<IActionResult> TambahAlamat()
        {
            await DropWilayah();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> TambahAlamat(VMAlamat _alamat)
        {
            if (ModelState.IsValid)
            {
                m_biodata_address alamat = new m_biodata_address();
                alamat.label = _alamat.label;
                alamat.recipient = _alamat.recipient;
                alamat.recipient_phone_number = _alamat.recipient_phone_number;
                alamat.location_id = _alamat.location_id;
                alamat.postal_code = _alamat.postal_code;
                alamat.address = _alamat.address;
                alamat.biodata_id = HttpContext.Session.GetInt32("bioid");
                alamat.created_by = (long)HttpContext.Session.GetInt32("iduser");
                alamat.created_on = DateTime.Now;
                _context.Add(alamat);
                await _context.SaveChangesAsync();

                return RedirectToAction("Alamat");
            }
            await DropWilayah();
            return View(_alamat);


        }

        public async Task<IActionResult> EditAlamat(int Id)
        {
            await DropWilayah();
            if (Id == 0)
            {
                return NotFound();
            }
            // var product = await _context.Products.FindAsync(Id);
            VMAlamat product = await DataAlamat(Id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        [HttpPost]
        public async Task<IActionResult> EditAlamat(VMAlamat _alamat)
        {
            if (ModelState.IsValid)
            {
                m_biodata_address alamat = new m_biodata_address();
                alamat.id = _alamat.id;
                alamat.label = _alamat.label;
                alamat.recipient = _alamat.recipient;
                alamat.recipient_phone_number = _alamat.recipient_phone_number;
                alamat.location_id = _alamat.location_id;
                alamat.postal_code = _alamat.postal_code;
                alamat.address = _alamat.address;
                alamat.biodata_id = _alamat.biodata_id;
                alamat.created_by = _alamat.created_by;
                alamat.created_on = _alamat.created_on;
                alamat.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                alamat.modified_on = DateTime.Now;
                _context.Update(alamat);
                await _context.SaveChangesAsync();

                return RedirectToAction("Alamat");
            }
            await DropWilayah();
            return View(_alamat);


        }

        public async Task<IActionResult> DeleteAlamat(int Id)
        {
            await DropWilayah();
            if (Id == 0)
            {
                return NotFound();
            }
            // var product = await _context.Products.FindAsync(Id);
            VMAlamat product = await DataAlamat(Id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAlamat(VMAlamat _alamat)
        {
            if (ModelState.IsValid)
            {
                m_biodata_address alamat = new m_biodata_address();
                alamat.id = _alamat.id;
                alamat.label = _alamat.label;
                alamat.recipient = _alamat.recipient;
                alamat.recipient_phone_number = _alamat.recipient_phone_number;
                alamat.location_id = _alamat.location_id;
                alamat.postal_code = _alamat.postal_code;
                alamat.address = _alamat.address;
                alamat.biodata_id = _alamat.biodata_id;
                alamat.created_by = _alamat.created_by;
                alamat.created_on = _alamat.created_on;
                alamat.modified_by = _alamat.modified_by;
                alamat.modified_on = _alamat.modified_on;
                alamat.deleted_by = (long)HttpContext.Session.GetInt32("iduser"); ;
                alamat.deleted_on = DateTime.Now;
                alamat.is_delete = true;
                _context.Update(alamat);
                await _context.SaveChangesAsync();

                return RedirectToAction("Alamat");
            }
            await DropWilayah();
            return View(_alamat);


        }

        public async Task<List<VMLocation>> DropWilayah()
        {
            List<VMLocation> relasi = await (from c in _context.m_location
                                             join l in _context.m_location_level
                                             on c.location_level_id equals l.id
                                             join k in _context.m_location
                                             on c.id equals k.parent_id
                                             join ll in _context.m_location_level
                                             on k.location_level_id equals ll.id
                                             select new VMLocation
                                             {
                                                 id = k.id,
                                                 nameLocation = c.name,
                                                 parent_id = c.parent_id,
                                                 location_level_id = l.id,
                                                 nameLocationLevel = l.name,
                                                 abbreviation = l.abbreviation,
                                                 parentname = k.name,
                                                 abbreviationparent = ll.abbreviation,
                                                 wilayah = ll.abbreviation + "." + k.name + " " + l.abbreviation + "." + c.name
                                             }).ToListAsync();
            //relasi.Insert(0, new VMLocation { id = 0, wilayah = "--Pilih--" });
            ViewBag.lokasi = relasi;
            return relasi;
        }

        public async Task<VMAlamat> DataAlamat(int Id)
        {
            var alamat = await (from almt in _context.m_biodata_address
                                join loc in _context.m_location
                                on almt.location_id equals loc.id
                                join loclev in _context.m_location_level
                                on loc.location_level_id equals loclev.id
                                join locp in _context.m_location
                                on loc.parent_id equals locp.id
                                join loclevp in _context.m_location_level
                                on locp.location_level_id equals loclevp.id
                                where almt.is_delete == false
                                select new VMAlamat
                                {
                                    id = almt.id,
                                    biodata_id = almt.biodata_id,
                                    label = almt.label,
                                    recipient = almt.recipient,
                                    recipient_phone_number = almt.recipient_phone_number,
                                    address = almt.address,
                                    postal_code = almt.postal_code,
                                    location_id = almt.location_id,
                                    created_by = almt.created_by,
                                    created_on = almt.created_on,
                                    modified_by = almt.modified_by,
                                    modified_on = almt.modified_on,
                                    deleted_by = almt.deleted_by,
                                    deleted_on = almt.deleted_on,
                                    wilayah = loclev.abbreviation + "." + loc.name + " " + loclevp.abbreviation + "." + locp.name
                                }).FirstOrDefaultAsync(a => a.id == Id);

            return alamat;
        }

        public IActionResult SpesialisasiDokter()
        {
            return View();
        }
        public IActionResult ErrorPage()
        {
            return View();
        }

        public async Task<IActionResult> DeleteSelectedAlamat(string data)
        {
            List<VMAlamat> alamatlist = new List<VMAlamat>();
            string[] dataarr = data.Split(",");
            if (data.Length == 0)
            {
                return NotFound();
            }
            // var product = await _context.Products.FindAsync(Id);
            for (int i = 0; i < dataarr.Length; i++)

            {
                VMAlamat alamat = await DataAlamat(int.Parse(dataarr[i]));
                alamatlist.Add(alamat);
            }
            //List<VMPasien> pasien = await dataPasienlist(Id);

            if (alamatlist == null)
            {
                return NotFound();
            }

            return View(alamatlist);
        }

        [HttpPost, ActionName("DeleteSelectedAlamat")]
        public async Task<IActionResult> DeleteSelectedAlamatConfirm(IEnumerable<int> deleteselect)
        {
            //int count = deleteselect.Count();
            //for (int i = 0; i < count ; i++)
            //{
            //    VMPasien pasien = await dataPasien(int.Parse(deleteselect{ i}));

            //    pasienlist.Add(pasien);
            //}
            DateTime date = DateTime.Now;

            foreach (var item in deleteselect)
            {
                m_biodata_address alamat = new m_biodata_address();
                VMAlamat vmalamat = await DataAlamat(item);
                alamat.id = vmalamat.id;
                alamat.label = vmalamat.label;
                alamat.recipient = vmalamat.recipient;
                alamat.recipient_phone_number = vmalamat.recipient_phone_number;
                alamat.location_id = vmalamat.location_id;
                alamat.postal_code = vmalamat.postal_code;
                alamat.address = vmalamat.address;
                alamat.biodata_id = vmalamat.biodata_id;
                alamat.created_by = vmalamat.created_by;
                alamat.created_on = vmalamat.created_on;
                alamat.modified_by = vmalamat.modified_by;
                alamat.modified_on = vmalamat.modified_on;
                alamat.deleted_by = (long)HttpContext.Session.GetInt32("iduser"); ;
                alamat.deleted_on = DateTime.Now;
                alamat.is_delete = true;
                _context.Update(alamat);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Alamat");
        }

        public async Task<IActionResult>TambahSpesialisasiDokter()
         {
            var idUser = HttpContext.Session.GetInt32("iduser");
            var idDok = await (from u in _context.m_user
                               join r in _context.m_role
                               on u.role_id equals r.id
                               join b in _context.m_biodata
                               on u.biodata_id equals b.id
                               join d in _context.m_doctor
                               on u.biodata_id equals d.biodata_id
                               where u.id == idUser

                               select d.id).FirstOrDefaultAsync();


            //var idDokter = HttpContext.Session.GetInt32("iddok");
            ViewBag.idDoc = idDok;

            var special = await (from s in _context.t_current_doctor_specialization
                                 where s.is_delete == false && s.specialization_id != null && s.doctor_id == idDok
                                 select s).FirstOrDefaultAsync();
            ViewBag.ls = special;

            var gabung = from d in _context.m_doctor
                         join t in _context.t_current_doctor_specialization
                            on d.id equals t.doctor_id
                         join s in _context.m_specialization
                             on t.specialization_id equals s.id
                         where d.id == idDok

                         select new VMSpesialisasiDokter
                         {
                             id = t.id,
                             doctor_id = d.id,
                             specialization_id = s.id,
                             name = s.name,
                             biodata_id = d.biodata_id,
                             str = d.str

                         };
            
            return View(gabung);

        }


        public async Task<IActionResult>AddSpesialisasi()
        {            
            var special = await (from s in _context.m_specialization
                       where s.is_delete == false
                       select s).ToListAsync();
            ViewBag.ls = special;

            var idUser = HttpContext.Session.GetInt32("iduser");
            var idDok = await (from u in _context.m_user
                               join r in _context.m_role
                               on u.role_id equals r.id
                               join b in _context.m_biodata
                               on u.biodata_id equals b.id
                               join d in _context.m_doctor
                               on u.biodata_id equals d.biodata_id
                               where u.id == idUser

                               select d.id).FirstOrDefaultAsync();


            //var idDokter = HttpContext.Session.GetInt32("iddok");
            ViewBag.idDoc = idDok;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddSpesialisasi(VMSpesialisasiDokter spesial)
        {

            if (ModelState.IsValid)
            {
                t_current_doctor_specialization spes = new t_current_doctor_specialization();
                spes.specialization_id = spesial.specialization_id;
                spes.doctor_id = spesial.doctor_id;
                spes.created_by = (long)HttpContext.Session.GetInt32("iduser");
                spes.created_on = DateTime.Now;
                _context.Add(spes);
                await _context.SaveChangesAsync();

                return RedirectToAction("TambahSpesialisasiDokter");
            }
            return View(spesial);
        }

        public async Task<IActionResult> EditSpesialisasi(int id)
        {

            var special = await (from s in _context.m_specialization
                                 where s.is_delete == false
                                 select s).ToListAsync();
            ViewBag.ls = special;

            var spesial = await (from d in _context.m_doctor
                                 join t in _context.t_current_doctor_specialization
                                 on d.id equals t.doctor_id
                                 join s in _context.m_specialization
                                 on t.specialization_id equals s.id
                                 where d.is_delete == false

                                 select new VMSpesialisasiDokter
                                 {
                                     id = t.id,
                                     doctor_id = d.id,
                                     specialization_id = s.id,
                                     name = s.name,
                                     str = d.str,
                                     created_by = t.created_by,
                                     created_on = t.created_on,
                                     modified_by = t.modified_by,
                                     modified_on = t.modified_on

                                 }).FirstOrDefaultAsync(a => a.id == id);
            return View(spesial);
        }

        [HttpPost]
        public async Task<IActionResult> EditSpesialisasi(VMSpesialisasiDokter spesial)
        {
            if (ModelState.IsValid)
            {
                t_current_doctor_specialization tc = new t_current_doctor_specialization();
                tc.id = spesial.id;
                tc.specialization_id = spesial.specialization_id;
                tc.doctor_id = spesial.doctor_id;
                tc.created_by = spesial.created_by;
                tc.created_on = spesial.created_on;
                tc.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                tc.modified_on = DateTime.Now;
                _context.Update(tc);
                await _context.SaveChangesAsync();

                return RedirectToAction("TambahSpesialisasiDokter");
            }
            return View(spesial);

        }
    }
}

