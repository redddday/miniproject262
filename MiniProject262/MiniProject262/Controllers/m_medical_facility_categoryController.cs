﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using Microsoft.AspNetCore.Http;
using MiniProject262.ViewModels;


namespace MiniProject262.Controllers
{
    public class m_medical_facility_categoryController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_medical_facility_categoryController(MiniProjectContext context)
        {
            _context = context;
        }


        public async Task<IActionResult> Index(string searchString)
        {
            
            ViewData["CurrentFilter"] = searchString;

            var mfc = from c in _context.m_medical_facility_category
                      select c;
            if (!String.IsNullOrEmpty(searchString))
            {
                mfc = mfc.Where(s => s.name.Contains(searchString));
            }
            return View(await mfc.AsNoTracking().Where(a => a.is_delete == false).ToListAsync());
        }


        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_medical_facility_category = await _context.m_medical_facility_category
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_medical_facility_category == null)
            {
                return NotFound();
            }

            return View(m_medical_facility_category);
        }


        public IActionResult Create()
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_medical_facility_category m_medical_facility_category)
        {
            if (ModelState.IsValid)
            {
                m_medical_facility_category.created_by = (long)HttpContext.Session.GetInt32("iduser");
                DateTime dt = DateTime.Now;
                m_medical_facility_category.created_on = dt;

                //string up = m_education_level.code.ToUpper();
                //m_education_level.code = up;

                _context.Add(m_medical_facility_category);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(m_medical_facility_category);
        }


        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_medical_facility_category = await _context.m_medical_facility_category.FindAsync(id);
            if (m_medical_facility_category == null)
            {
                return NotFound();
            }
            return View(m_medical_facility_category);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_medical_facility_category m_medical_facility_category)
        {
            if (id != m_medical_facility_category.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_medical_facility_category.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    m_medical_facility_category.modified_on = dt;

                    _context.Update(m_medical_facility_category);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_medical_facility_categoryExists(m_medical_facility_category.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_medical_facility_category);
        }


        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_medical_facility_category = await _context.m_medical_facility_category.FindAsync(id);
            if (m_medical_facility_category == null)
            {
                return NotFound();
            }
            return View(m_medical_facility_category);

        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_medical_facility_category m_medical_facility_category)
        {

            if (id != m_medical_facility_category.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                m_medical_facility_category.is_delete = true;
                try
                {
                    m_medical_facility_category.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    m_medical_facility_category.deleted_on = dt;

                    _context.Update(m_medical_facility_category);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_medical_facility_categoryExists(m_medical_facility_category.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_medical_facility_category);
        }

        private bool m_medical_facility_categoryExists(long id)
        {
            return _context.m_medical_facility_category.Any(e => e.id == id);
        }
    }
}
