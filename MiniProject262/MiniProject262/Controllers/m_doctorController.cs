﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;

namespace MiniProject262.Controllers
{
    public class m_doctorController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_doctorController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: m_doctor
        public async Task<IActionResult> Index()
        {
            var students = from doc in _context.m_doctor
                               //on docof.doctor_id equals doc.id

                           join bio in _context.m_biodata
                           on doc.biodata_id equals bio.id

                           //join doctre in _context.t_doctor_treatment
                           //on doc.id equals doctre.doctor_id

                           //join tcds in _context.t_current_doctor_specialization
                           //on doc.id equals tcds.doctor_id

                           //join spec in _context.m_specialization
                           //on tcds.specialization_id equals spec.id

                           //join tdos in _context.t_doctor_office_schedule
                           //on doc.id equals tdos.doctor_id

                           //join mfs in _context.m_medical_facility_schedule
                           //on medfac.id equals mfs.medical_facility_id

                           select new VMDetailDokter
                           {
                               doctor_id = doc.id,
                               id = doc.id,
                               fullname = bio.fullname
                           };

            return View(students);
        }

        // GET: m_doctor/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            VMDetailDokter list = await DataBM(id);

            var abc = from loclev in _context.m_location_level
                      join loc in _context.m_location
                      on loclev.id equals loc.location_level_id

                      join medfac in _context.m_medical_facility
                      on loc.id equals medfac.location_id

                      join docof in _context.t_doctor_office
                      on medfac.id equals docof.mdeical_facility_id

                      join doc in _context.m_doctor
                       on docof.doctor_id equals doc.id

                      where doc.id == id && docof.is_delete == false && medfac.medical_facility_category_id != 2

                      select new VMDetailDokter

                      {
                          id = doc.id,
                          DoctorBackup_id = medfac.id,
                          doctor_id = doc.id,
                          parent_id = loc.parent_id,
                          location_id = loc.id,
                          location_level_id = loc.location_level_id,
                          namaLokasi = loc.name,
                          mdeical_facility_id = medfac.id,
                          rumahSakit = medfac.name,
                          full_address = medfac.full_address,
                          abbreviation = loclev.abbreviation,
                          specialization = docof.specialization,
                          //mdeical_facility_id = docof.mdeical_facility_id,
                          DoctorOffice_id= docof.id,
                          //day = medfacsched.day,
                          //time_schedule_start = medfacsched.time_schedule_start,
                          //time_schedule_end = medfacsched.time_schedule_end,
                          deleted_on = docof.deleted_on,
                          tahun = docof.created_on.Year,
                          //price_start_from = doctreprice.price_start_from

                      };

            ViewBag.ABC = abc;

            var abcd = from loclev in _context.m_location_level
                      join loc in _context.m_location
                      on loclev.id equals loc.location_level_id

                      join medfac in _context.m_medical_facility
                      on loc.id equals medfac.location_id

                      join docof in _context.t_doctor_office
                      on medfac.id equals docof.mdeical_facility_id

                      join doc in _context.m_doctor
                       on docof.doctor_id equals doc.id

                      where doc.id == id && medfac.medical_facility_category_id != 2

                       select new VMDetailDokter

                      {
                          id = doc.id,
                          DoctorBackup_id = medfac.id,
                          doctor_id = doc.id,
                          parent_id = loc.parent_id,
                          location_id = loc.id,
                          location_level_id = loc.location_level_id,
                          namaLokasi = loc.name,
                          mdeical_facility_id = medfac.id,
                          rumahSakit = medfac.name,
                          full_address = medfac.full_address,
                          abbreviation = loclev.abbreviation,
                          specialization = docof.specialization,
                          //mdeical_facility_id = docof.mdeical_facility_id,
                          DoctorOffice_id = docof.id,
                          //day = medfacsched.day,
                          //time_schedule_start = medfacsched.time_schedule_start,
                          //time_schedule_end = medfacsched.time_schedule_end,
                          deleted_on = docof.deleted_on,
                          tahun = docof.created_on.Year,
                          //price_start_from = doctreprice.price_start_from

                      };

            ViewBag.ABCD = abcd;


            var bcd = from doc in _context.m_doctor
                          //on docof.doctor_id equals doc.id


                      join bio in _context.m_biodata
                      on doc.biodata_id equals bio.id

                      join doced in _context.m_doctor_education
                      on doc.id equals doced.doctor_id

                      join edlev in _context.m_education_level
                      on doced.education_level_id equals edlev.id

                      where doc.id == id

                      select new VMDetailDokter

                      {
                          id = doc.id,
                          doctor_id = doc.id,
                          fullname = bio.fullname,

                          institution_name = doced.institution_name,
                          major = doced.major,
                          start_year = doced.start_year,


                          image = bio.image,
                          image_path = bio.image_path
                      };
            ViewBag.BCD = bcd;

            var cde =                      
                (

                        from medfac in _context.m_medical_facility

                        join medfacsched in _context.m_medical_facility_schedule
                        on medfac.id equals medfacsched.medical_facility_id

                        join docofsched in _context.t_doctor_office_schedule
                        on medfacsched.id equals docofsched.medical_facility_schedule_id


                        where docofsched.doctor_id == id && medfac.medical_facility_category_id != 2

                        select new VMDetailDokter

                       {
                           //id = doc.id,
                           doctor_id = docofsched.doctor_id,
                           id = medfac.id,
                           day = medfacsched.day,
                           time_schedule_start = medfacsched.time_schedule_start,
                           time_schedule_end = medfacsched.time_schedule_end,
                           DoctorBackup_id= docofsched.medical_facility_schedule_id

                       }).ToList();
            ViewBag.CDE = cde;

            var efg = from doc in _context.m_doctor

                      join doctre in _context.t_doctor_treatment
                      on doc.id equals doctre.doctor_id

                      where doc.id == id

                      select new VMDetailDokter

                      {
                          id = doc.id,
                          doctor_id = doc.id,
                          tindakanMedis = doctre.name,
                      };
            ViewBag.EFG = efg;

            var fgh = (
                from doc in _context.m_doctor

                join doctre in _context.t_doctor_treatment
                on doc.id equals doctre.doctor_id

                join docoftre in _context.t_doctor_office_treatment
                on doctre.id equals docoftre.doctor_treatment_id

                join doctreprice in _context.t_doctor_office_treatment_price
                on docoftre.id equals doctreprice.doctor_office_treatment_id

                join docof in _context.t_doctor_office
               on doc.id equals docof.doctor_id



                where doctre.is_delete == false && docoftre.is_delete == false && doctreprice.is_delete == false
                select new VMDetailDokter

                {
                    doctor_id = doctre.doctor_id,
                    mdeical_facility_id = docof.mdeical_facility_id,
                    price_start_from = doctreprice.price_start_from
                }).ToList();
            ViewBag.FGH = fgh;

            var ronaldo =

                       (from medfac in _context.m_medical_facility


                        join docof in _context.t_doctor_office
                        on medfac.id equals docof.mdeical_facility_id

                        join doc in _context.m_doctor
                         on docof.doctor_id equals doc.id

                        join bio in _context.m_biodata
                          on doc.biodata_id equals bio.id

                        join tdos in _context.t_doctor_office_schedule
                         on doc.id equals tdos.doctor_id

                        join medfacsche in _context.m_medical_facility_schedule
                         on medfac.id equals medfacsche.medical_facility_id

                        where tdos.doctor_id == doc.id && doc.is_delete == false && medfac.medical_facility_category_id != 2
                        orderby tdos.created_by descending

                        select new VMDetailDokter

                        {
                            id = medfac.id,
                            doctor_id = doc.id,
                            tahun = CalculateAge(tdos.created_on),
                            created_on = tdos.created_on

                        }).ToList();



            ViewBag.RONALDO = ronaldo.OrderByDescending(a => a.tahun);

            var berg = (              
                        from doc in _context.m_doctor
                         //on docof.doctor_id equals doc.id

                        join doctre in _context.t_doctor_treatment
                        on doc.id equals doctre.doctor_id

                        join docoftre in _context.t_doctor_office_treatment
                        on doctre.id equals docoftre.doctor_treatment_id

                        join doctreprice in _context.t_doctor_office_treatment_price
                        on docoftre.id equals doctreprice.doctor_office_treatment_id

                        where doc.id == id && doctre.name.Contains("Konsultasi") && doctre.is_delete==false && docoftre.is_delete==false && doctreprice.is_delete==false

                       select new VMDetailDokter

                       {
                           id = doc.id,
                           doctor_id = doc.id,
                          //mdeical_facility_id=medfac.id,
                           DoctorOffice_id = docoftre.doctor_office_id,
                           price_start_from = doctreprice.price_start_from

                       }).ToList();
            ViewBag.BERG = berg.OrderBy(a => a.price_start_from);

            var bergs = (from loclev in _context.m_location_level
                        join loc in _context.m_location
                        on loclev.id equals loc.location_level_id

                        join medfac in _context.m_medical_facility
                        on loc.id equals medfac.location_id

                        join medfacsched in _context.m_medical_facility_schedule
                        on medfac.id equals medfacsched.medical_facility_id

                        join docofsched in _context.t_doctor_office_schedule
                        on medfacsched.id equals docofsched.medical_facility_schedule_id

                        join docof in _context.t_doctor_office
                        on medfac.id equals docof.mdeical_facility_id

                        join doc in _context.m_doctor
                           on docof.doctor_id equals doc.id

                        join doctre in _context.t_doctor_treatment
                       on doc.id equals doctre.doctor_id

                       join docoftre in _context.t_doctor_office_treatment
                       on doctre.id equals docoftre.doctor_treatment_id

                       join doctreprice in _context.t_doctor_office_treatment_price
                       on docoftre.id equals doctreprice.doctor_office_treatment_id

                       where doc.id == id && doctre.name.Contains("Konsultasi") && doctre.is_delete == false && docoftre.is_delete == false && doctreprice.is_delete == false && medfac.medical_facility_category_id != 2

                         select new VMDetailDokter

                       {
                           id = doc.id,
                           doctor_id = doc.id,
                            mdeical_facility_id=medfac.id,
                            DoctorOffice_id = docoftre.doctor_office_id,
                           price_start_from = doctreprice.price_start_from

                       }).ToList();
            ViewBag.BERGS = bergs;

            if (id == null)
            {
                return NotFound();
            }

            if (list == null)
            {
                return NotFound();
            }

            return View(list);
        }

        // GET: m_doctor/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: m_doctor/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,biodata_id,str,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_doctor m_doctor)
        {
            if (ModelState.IsValid)
            {
                _context.Add(m_doctor);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(m_doctor);
        }

        // GET: m_doctor/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_doctor = await _context.m_doctor.FindAsync(id);
            if (m_doctor == null)
            {
                return NotFound();
            }
            return View(m_doctor);
        }

        // POST: m_doctor/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,biodata_id,str,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_doctor m_doctor)
        {
            if (id != m_doctor.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(m_doctor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_doctorExists(m_doctor.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_doctor);
        }

        // GET: m_doctor/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_doctor = await _context.m_doctor
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_doctor == null)
            {
                return NotFound();
            }

            return View(m_doctor);
        }

        // POST: m_doctor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var m_doctor = await _context.m_doctor.FindAsync(id);
            _context.m_doctor.Remove(m_doctor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool m_doctorExists(long id)
        {
            return _context.m_doctor.Any(e => e.id == id);
        }
        public async Task<VMDetailDokter> DataBM(long? id)
        {
            VMDetailDokter list = await (
                                        //from loclev in _context.m_location_level
                                        //join loc in _context.m_location
                                        //on loclev.id equals loc.location_level_id

                                        from medfac in _context.m_medical_facility
                                        //on loc.id equals medfac.location_id

                                        join medfacsched in _context.m_medical_facility_schedule
                                        on medfac.id equals medfacsched.medical_facility_id

                                        join docof in _context.t_doctor_office
                                        on medfac.id equals docof.mdeical_facility_id

                                        join doc in _context.m_doctor
                                        on docof.doctor_id equals doc.id

                                        join tdos in _context.t_doctor_office_schedule
                                        on doc.id equals tdos.doctor_id

                                        join bio in _context.m_biodata
                                        on doc.biodata_id equals bio.id

                                        join doctre in _context.t_doctor_treatment
                                        on doc.id equals doctre.doctor_id

                                        join docoftre in _context.t_doctor_office_treatment
                                        on doctre.id equals docoftre.doctor_treatment_id

                                        join doctreprice in _context.t_doctor_office_treatment_price
                                        on docoftre.id equals doctreprice.doctor_office_treatment_id

                                        join tcds in _context.t_current_doctor_specialization
                                        on doc.id equals tcds.doctor_id

                                        join spec in _context.m_specialization
                                        on tcds.specialization_id equals spec.id

                                        join doced in _context.m_doctor_education
                                        on doc.id equals doced.doctor_id

                                        join edlev in _context.m_education_level
                                        on doced.education_level_id equals edlev.id


                                        where doc.id == id 

                                        select new VMDetailDokter

                                        {
                                            id = doc.id,
                                            doctor_id = doc.id,
                                            //parent_id = loc.parent_id,
                                            //location_id = loc.id,
                                            //location_level_id = loc.location_level_id,
                                            DoctorBackup_id= tdos.medical_facility_schedule_id,

                                            fullname = bio.fullname,
                                            //namaLokasi = loc.name,
                                            specialization = spec.name,
                                            tindakanMedis = doctre.name,
                                            //rumahSakit = medfac.name,
                                            //full_address = medfac.full_address,
                                            //abbreviation = loclev.abbreviation,
                                            institution_name = doced.institution_name,
                                            major = doced.major,
                                            start_year = doced.start_year,
                                            day = medfacsched.day,
                                            time_schedule_start = medfacsched.time_schedule_start,
                                            time_schedule_end = medfacsched.time_schedule_end,
                                            pengalaman = CalculateAge(tdos.created_on),
                                            price_start_from = doctreprice.price_start_from,
                                            price_until_from = doctreprice.price_until_from,
                                            price = doctreprice.price,
                                            str = doc.str,


                                            image = bio.image,
                                            image_path = bio.image_path
                                        }
                                              ).FirstOrDefaultAsync();

            return list;
        }
        public static int CalculateAge(DateTime? dob)
        {
            int age = 0;
            DateTime doba = Convert.ToDateTime(dob);

            age = DateTime.Now.Year - doba.Year;
            if (DateTime.Now.DayOfYear < doba.DayOfYear)
            {
                age = age - 1;
            }

            return age;
        }
    }
}
