﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;

namespace MiniProject262.Controllers
{
    public class m_roleController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_roleController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: m_role
        public async Task<IActionResult> Index(string SortOrder, string CurrentFilter, string SearchString, int? pageNumber, int? pSize)
        {
            //m_admin dummy = new m_admin
            //{
            //    id = 200
            //};

            //ViewBag.dummy = dummy.id;
            ViewData["CurrentSort"] = SortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(SortOrder) ? "name_desc" : "";
            //ViewData["CodeSortParm"] = String.IsNullOrEmpty(SortOrder) ? "code_desc" : "";
            ViewData["CurrentFilter"] = SearchString;
            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<m_role>.PageSizeList();

            if (SearchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                SearchString = CurrentFilter;
            }

            //ViewData["currentfilter"] = SearchString;

            var role = from r in _context.m_role
                       select r;
            if (!string.IsNullOrEmpty(SearchString))
            {
                role = role.Where(r => r.name.Contains(SearchString));
            }

            switch (SortOrder)
            {
                case "name_desc":
                    role = role.OrderByDescending(r => r.name);
                    break;
            }
            int pageSize = pSize ?? 5;
            return View(PageinatedList<m_role>.CreateAsync(role.AsNoTracking().Where(r => r.is_delete == false).ToList(), pageNumber ?? 1, pageSize));

            //return View(await _context.m_role.ToListAsync());
        }

        public int countCart()
        {
            int count = _context.m_role.Where(r => r.is_delete == false).Count();
            ViewBag.count = count;
            return count;
        }

        // GET: m_role/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_role = await _context.m_role
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_role == null)
            {
                return NotFound();
            }

            return View(m_role);
        }

        // GET: m_role/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: m_role/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,code,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_role m_role)
        {
            if (ModelState.IsValid)
            {
                m_role.created_by = (long)HttpContext.Session.GetInt32("iduser");
                m_role.created_on = DateTime.Now;
                _context.Add(m_role);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(m_role);
        }

        // GET: m_role/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_role = await _context.m_role.FindAsync(id);
            if (m_role == null)
            {
                return NotFound();
            }
            return View(m_role);
        }

        // POST: m_role/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,code,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_role m_role)
        {
            if (id != m_role.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_role.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    m_role.modified_on = DateTime.Now;
                    _context.Update(m_role);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_roleExists(m_role.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_role);
        }

        // GET: m_role/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_role = await _context.m_role
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_role == null)
            {
                return NotFound();
            }

            return View(m_role);
        }

        // POST: m_role/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, [Bind("id,name,code,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_role m_role)
        {
            if (id != m_role.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_role.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    m_role.is_delete = true;
                    m_role.deleted_on = DateTime.Now;
                    _context.Update(m_role);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_roleExists(m_role.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_role);
        }

        public string ValidateName(string cekNama, string cekKode)
        {
            m_role nama = new m_role();
            nama = _context.m_role.Where(a => (a.name == cekNama && a.is_delete == false) || (a.code == cekKode && a.is_delete == false)).FirstOrDefault();
            if (nama == null)
            {
                return "0";
            }
            else if (nama != null && nama.name == cekNama)
            {
                return "1";
            }
            else if (nama != null && nama.code == cekKode)
            {
                return "2";
            }
            else
            {
                return "0";
            }
        }

        private bool m_roleExists(long id)
        {
            return _context.m_role.Any(e => e.id == id);
        }
    }
}
