﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using Microsoft.AspNetCore.Http;
using MiniProject262.ViewModels;


namespace MiniProject262.Controllers
{
    public class m_education_levelController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_education_levelController(MiniProjectContext context)
        {
            _context = context;
        }

        
        public async Task<IActionResult> Index(string searchString)
        {
            //m_admin dummy = new m_admin
            //{
            //    id = 200
            //};

            //ViewBag.dummy = dummy.id;
            ViewData["CurrentFilter"] = searchString;

            var mel = from c in _context.m_education_level
                           select c;
            if (!String.IsNullOrEmpty(searchString))
            {
                mel = mel.Where(s => s.name.Contains(searchString));
            }
            return View(await mel.AsNoTracking().Where(a => a.is_delete == false).ToListAsync());
        }

        
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_education_level = await _context.m_education_level
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_education_level == null)
            {
                return NotFound();
            }

            return View(m_education_level);
        }

        
        public IActionResult Create()
        {

            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_education_level m_education_level)
        {
            if (ModelState.IsValid)
            {
                m_education_level.created_by = (long)HttpContext.Session.GetInt32("iduser");
                DateTime dt = DateTime.Now;
                m_education_level.created_on = dt;

                //string up = m_education_level.code.ToUpper();
                //m_education_level.code = up;

                _context.Add(m_education_level);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(m_education_level);
        }

        
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_education_level = await _context.m_education_level.FindAsync(id);
            if (m_education_level == null)
            {
                return NotFound();
            }
            return View(m_education_level);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_education_level m_education_level)
        {
            if (id != m_education_level.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_education_level.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    m_education_level.modified_on = dt;

                    _context.Update(m_education_level);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_education_levelExists(m_education_level.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_education_level);
        }

        
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_education_Level = await _context.m_education_level.FindAsync(id);
            if (m_education_Level == null)
            {
                return NotFound();
            }
            return View(m_education_Level);

        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, [Bind("id,name,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_education_level m_education_level)
        {

            if (id != m_education_level.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                m_education_level.is_delete = true;
                try
                {
                    m_education_level.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    m_education_level.deleted_on = dt;

                    _context.Update(m_education_level);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_education_levelExists(m_education_level.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_education_level);
        }

        private bool m_education_levelExists(long id)
        {
            return _context.m_education_level.Any(e => e.id == id);
        }
    }
}
