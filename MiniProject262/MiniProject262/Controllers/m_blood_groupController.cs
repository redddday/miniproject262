﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using Microsoft.AspNetCore.Http;
using MiniProject262.ViewModels;

namespace MiniProject262.Controllers
{
    public class m_blood_groupController : Controller
    {
        
        private readonly MiniProjectContext _context;

        public m_blood_groupController(MiniProjectContext context)
        {

            _context = context;

        }

        // GET: m_blood_group
        public async Task<IActionResult> Index(string searchString)
        {
            //m_admin dummy = new m_admin
            //{
            //    id = 200
            //};
            ViewData["CurrentFilter"] = searchString;

            var blood = from s in _context.m_blood_group
                        join user in _context.m_user
                        on s.created_by equals user.id
                        join bio in _context.m_biodata
                        on user.biodata_id equals bio.id
                        select new VMBlood 
                        { 
                           id = s.id,
                           code = s.code,
                           description = s.description,
                           id_user = user.id,
                           biodata_id = bio.id,
                           fullname = bio.fullname,
                           is_delete = s.is_delete
                           
                        };
            if (!String.IsNullOrEmpty(searchString))
            {
                blood = blood.Where(s => s.code.Contains(searchString) );
               
            }

            //ViewBag.dummy = dummy.id;
            return View(await blood.AsNoTracking().Where(a => a.is_delete == false).ToListAsync());
        }

        // GET: m_blood_group/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_blood_group = await _context.m_blood_group
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_blood_group == null)
            {
                return NotFound();
            }

            return View(m_blood_group);
        }

        // GET: m_blood_group/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: m_blood_group/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,code,description,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_blood_group m_blood_group)
        {
            if (ModelState.IsValid)
            {
                var id = HttpContext.Session.GetInt32("iduser");
                var ceking = _context.m_blood_group.FirstOrDefault(a => a.code == m_blood_group.code && a.created_by == id);
                if (ceking != null)
                {
                    return Json(new { status = "error", message = "*data sudah ada" });

                }
                m_blood_group.created_by = (long)HttpContext.Session.GetInt32("iduser");
                DateTime dt = DateTime.Now;
                m_blood_group.created_on = dt;

                string up = m_blood_group.code.ToUpper();
                m_blood_group.code = up;
               
                _context.Add(m_blood_group);
                await _context.SaveChangesAsync();
                return Json(new { status = "sucess"});
            }
            return View(m_blood_group);
        }

        // GET: m_blood_group/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_blood_group = await _context.m_blood_group.FindAsync(id);
            if (m_blood_group == null)
            {
                return NotFound();
            }
            return View(m_blood_group);
        }

        // POST: m_blood_group/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,code,description,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_blood_group m_blood_group)
        {
            if (id != m_blood_group.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_blood_group.modified_by = (long)HttpContext.Session.GetInt32("iduser");
                    m_blood_group.modified_on = DateTime.Now;
                    string up = m_blood_group.code.ToUpper();
                    m_blood_group.code = up;
                    _context.Update(m_blood_group);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_blood_groupExists(m_blood_group.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_blood_group);
        }

        // GET: m_blood_group/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_blood_group = await _context.m_blood_group.FindAsync(id);
            if (m_blood_group == null)
            {
                return NotFound();
            }
            return View(m_blood_group);
        }

        // POST: m_blood_group/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, 
            [Bind("id,code,description,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_blood_group m_blood_group)
        {

            if (id != m_blood_group.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_blood_group.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    m_blood_group.is_delete = true;
                    m_blood_group.deleted_on = DateTime.Now;
                    _context.Update(m_blood_group);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_blood_groupExists(m_blood_group.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_blood_group);
        }

        private bool m_blood_groupExists(long id)
        {
            return _context.m_blood_group.Any(e => e.id == id);
        }
    }
}
