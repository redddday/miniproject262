﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;

namespace MiniProject262.Controllers
{
    public class VMCariDokterController : Controller
    {
        private readonly MiniProjectContext _context;

        public VMCariDokterController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: VMCariDokter
        public IActionResult Index(string searchString)
        {
            var students = from loclev in _context.m_location_level
                       join loc in _context.m_location
                       on loclev.id equals loc.location_level_id

                       join medfac in _context.m_medical_facility
                       on loc.id equals medfac.location_id

                       join docof in _context.t_doctor_office
                       on medfac.id equals docof.mdeical_facility_id

                       join doc in _context.m_doctor
                       on docof.doctor_id equals doc.id

                       join bio in _context.m_biodata
                       on doc.biodata_id equals bio.id

                       join doctre in _context.t_doctor_treatment
                       on doc.id equals doctre.doctor_id

                       join tcds in _context.t_current_doctor_specialization
                       on doc.id equals tcds.doctor_id

                       join spec in _context.m_specialization
                       on tcds.specialization_id equals spec.id

                       select new VMCariDokter
                       {
                           doctor_id = doc.id,
                           biodata_id = bio.id,
                           mdeical_facility_id = medfac.id,
                           location_id = loc.id,
                           location_level_id = loclev.id,
                           specialization_id = spec.id,
                           treatId=doctre.id,

                           fullname = bio.fullname,
                           namaLokasi = loc.name,
                           specialization = spec.name,
                           tindakanMedis = doctre.name,

                           image = bio.image,
                           image_path = bio.image_path
                           
                       }
;
            //m_admin dummy = new m_admin
            //{
            //    id = 200
            //};

            //ViewBag.dummy = dummy.id;
            //return View(await students.AsNoTracking().Where(a => a.is_delete == false).ToListAsync());
            return View(students);
            //return View();
        }

        // GET: VMCariDokter/Details/5
        public async Task< IActionResult> CariDokter(string lokasi,string namadok,string spesial,string tindakanmedis, string searchString, int? pageNumber, int? pSize, string currentFilter)
        {        
            var students = from loclev in _context.m_location_level
                           join loc in _context.m_location
                           on loclev.id equals loc.location_level_id

                           join medfac in _context.m_medical_facility
                           on loc.id equals medfac.location_id

                           join docof in _context.t_doctor_office
                           on medfac.id equals docof.mdeical_facility_id

                           join doc in _context.m_doctor
                           on docof.doctor_id equals doc.id

                           join bio in _context.m_biodata
                           on doc.biodata_id equals bio.id

                           join doctre in _context.t_doctor_treatment
                           on doc.id equals doctre.doctor_id

                           join tcds in _context.t_current_doctor_specialization
                           on doc.id equals tcds.doctor_id

                           join spec in _context.m_specialization
                           on tcds.specialization_id equals spec.id

                           select new VMCariDokter
                           {
                               //doctor_id = doc.id,
                               //biodata_id = bio.id,
                               //mdeical_facility_id = medfac.id,
                               //location_id = loc.id,
                               //location_level_id = loclev.id,
                               //specialization_id = spec.id,
                               //treatId = doctre.id,
                               //DoctorOffice_id=docof.id,
                               //DoctorBackup_id=docof.doctor_id,
                               id=doc.id,

                               fullname = bio.fullname,
                               namaLokasi = loc.name,
                               specialization = spec.name,
                               tindakanMedis = doctre.name,
                               rumahSakit=medfac.name,
                               full_address=medfac.full_address,

                               image = bio.image,
                               image_path = bio.image_path
                           };


            List<m_specialization> cl = new List<m_specialization>();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;

            List<m_location> cd = new List<m_location>();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;

            List<t_doctor_treatment> ck = new List<t_doctor_treatment>();
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;
            //m_admin dummy = new m_admin
            //{
            //    id = 200
            //};

            //ViewBag.dummy = dummy.id;

            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<m_payment_method>.PageSizeList();
            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }



            ViewData["CurrentFilter"] = namadok;
            ViewData["CurrentFilter1"] = lokasi;
            ViewData["CurrentFilter2"] = spesial;
            ViewData["CurrentFilter3"] = tindakanmedis;
            ViewBag.Filter1 = namadok;
            ViewBag.Filter2 = lokasi;
            ViewBag.Filter3 = spesial;
            ViewBag.Filter4 = tindakanmedis;

            if (!String.IsNullOrEmpty(lokasi) && !String.IsNullOrEmpty(spesial))
            {
                students = students.Where(s => s.namaLokasi.Contains(lokasi) && s.specialization.Contains(spesial));

            }
            else if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial))
            {
                students = students.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial));

            }
            else if (!String.IsNullOrEmpty(namadok) && !String.IsNullOrEmpty(spesial))
            {
                students = students.Where(s => s.fullname.Contains(namadok) && s.specialization.Contains(spesial));

            }
            else if (!String.IsNullOrEmpty(namadok) && !String.IsNullOrEmpty(spesial)&& !String.IsNullOrEmpty(lokasi))
            {
                students = students.Where(s => s.fullname.Contains(namadok) && s.specialization.Contains(spesial)&& s.namaLokasi.Contains(lokasi));

            }
            else if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(lokasi))
            {
                students = students.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial) && s.namaLokasi.Contains(lokasi));

            }
            else if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(namadok))
            {
                students = students.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial) && s.fullname.Contains(namadok));

            }
            else if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(namadok) && !String.IsNullOrEmpty(lokasi))
            {
                students = students.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial) && s.fullname.Contains(namadok) && s.namaLokasi.Contains(lokasi));

            }
            else if (!String.IsNullOrEmpty(spesial))
            {
                students = students.Where(s => s.specialization.Contains(spesial));

            }

            int pageSize = pSize ?? 12;
            //return View(await students.AsNoTracking().ToListAsync()) ;
            return View(PageinatedList<VMCariDokter>.CreateAsync(students.AsNoTracking().ToList(), pageNumber ?? 1, pageSize));
            //return View(students);
        }

        // GET: VMCariDokter/Create
        public async Task< IActionResult> Create(string searchString)
        {
            List<m_specialization> cl = await DataSpec();
            ViewBag.ListSpec = cl;

            List<m_location> cd = await DataLoc();
            ViewBag.ListLoc = cd;

            List<t_doctor_treatment> ck = await DataTreat();
            ViewBag.LisDocTreat = ck;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VMCariDokter vMCariDokter)
        {

           VMCariDokter newDoc = await DataKM(vMCariDokter.id);

            List<m_specialization> cl = new List<m_specialization>();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;

            List<m_location> cd = new List<m_location>();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;

            List<t_doctor_treatment> ck = new List<t_doctor_treatment>();
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;

            return View(newDoc);
        }

        // GET: VMCariDokter/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vMCariDokter = await _context.VMCariDokter.FindAsync(id);

            List<m_specialization> cl = new List<m_specialization>();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;

            List<m_location> cd = new List<m_location>();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;

            List<t_doctor_treatment> ck = new List<t_doctor_treatment>();
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;

            if (vMCariDokter == null)
            {
                return NotFound();
            }
            return View(vMCariDokter);
        }

        // POST: VMCariDokter/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,parent_id,location_level_id,medical_facility_category_id,location_id,full_address,email,phone_code,phone,fax,abbreviation,doctor_id,mdeical_facility_id,specialization,biodata_id,str,fullname,mobile_phone,image,image_path,specialization_id")] VMCariDokter vMCariDokter)
        {
            if (id != vMCariDokter.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vMCariDokter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VMCariDokterExists(vMCariDokter.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vMCariDokter);
        }

        // GET: VMCariDokter/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vMCariDokter = await _context.VMCariDokter
                .FirstOrDefaultAsync(m => m.id == id);
            if (vMCariDokter == null)
            {
                return NotFound();
            }

            return View(vMCariDokter);
        }

        // POST: VMCariDokter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var vMCariDokter = await _context.VMCariDokter.FindAsync(id);
            _context.VMCariDokter.Remove(vMCariDokter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VMCariDokterExists(long id)
        {
            return _context.VMCariDokter.Any(e => e.id == id);
        }

        public async Task<List<VMCariDokter>> DataJM(VMCariDokter _id)
        {
            List<VMCariDokter> list = await (from loclev in _context.m_location_level
                                             join loc in _context.m_location
                                             on loclev.id equals loc.location_level_id

                                             join medfac in _context.m_medical_facility
                                             on loc.id equals medfac.location_id

                                             join docof in _context.t_doctor_office
                                             on medfac.id equals docof.mdeical_facility_id

                                             join doc in _context.m_doctor
                                             on docof.doctor_id equals doc.id

                                             join bio in _context.m_biodata
                                             on doc.biodata_id equals bio.id

                                             join doctre in _context.t_doctor_treatment
                                             on doc.id equals doctre.doctor_id

                                             join tcds in _context.t_current_doctor_specialization
                                             on doc.id equals tcds.doctor_id

                                             join spec in _context.m_specialization
                                             on tcds.specialization_id equals spec.id

                                             select new VMCariDokter
                                             {
                                                 doctor_id = doc.id,
                                                 biodata_id = bio.id,
                                                 mdeical_facility_id = medfac.id,
                                                 location_id = loc.id,
                                                 location_level_id = loclev.id,
                                                 specialization_id = spec.id,

                                                 fullname = bio.fullname,
                                                 namaLokasi = loc.name,
                                                 specialization = spec.name,
                                                 tindakanMedis = doctre.name,

                                                 image = bio.image,
                                                 image_path = bio.image_path
                                             }
                                             ).ToListAsync();

            return list;
        }

        public async Task<VMCariDokter> DataKM(long? id)
        {
            VMCariDokter list = await (from loclev in _context.m_location_level
                                             join loc in _context.m_location
                                             on loclev.id equals loc.location_level_id

                                             join medfac in _context.m_medical_facility
                                             on loc.id equals medfac.location_id

                                             join docof in _context.t_doctor_office
                                             on medfac.id equals docof.mdeical_facility_id

                                             join doc in _context.m_doctor
                                             on docof.doctor_id equals doc.id

                                             join bio in _context.m_biodata
                                             on doc.biodata_id equals bio.id

                                             join doctre in _context.t_doctor_treatment
                                             on doc.id equals doctre.doctor_id

                                             join tcds in _context.t_current_doctor_specialization
                                             on doc.id equals tcds.doctor_id

                                             join spec in _context.m_specialization
                                             on tcds.specialization_id equals spec.id

                                             select new VMCariDokter
                                             {
                                                 //doctor_id = doc.id,
                                                 //biodata_id = bio.id,
                                                 //mdeical_facility_id = medfac.id,
                                                 //location_id = loc.id,
                                                 //location_level_id = loclev.id,
                                                 //specialization_id = spec.id,
                                                 id=doc.id,

                                                 fullname = bio.fullname,
                                                 namaLokasi = loc.name,
                                                 specialization = spec.name,
                                                 tindakanMedis = doctre.name,

                                                 image = bio.image,
                                                 image_path = bio.image_path
                                             }
                                             ).FirstOrDefaultAsync();

            return list;
        }


        public IActionResult SearchParam(VMCariDokter _caridokter)
        {
            List<m_specialization> cl = new List<m_specialization>();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;

            List<m_location> cd = new List<m_location>();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;

            List<t_doctor_treatment> ck = new List<t_doctor_treatment>();
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;
            return View();
        }
        public async Task<VMSearch> DataMM(int id)
        {

            VMSearch list = await (from loclev in _context.m_location_level
                                   join loc in _context.m_location
                                   on loclev.id equals loc.location_level_id

                                   join medfac in _context.m_medical_facility
                                   on loc.id equals medfac.location_id

                                   join docof in _context.t_doctor_office
                                   on medfac.id equals docof.mdeical_facility_id

                                   join doc in _context.m_doctor
                                   on docof.doctor_id equals doc.id

                                   join bio in _context.m_biodata
                                   on doc.biodata_id equals bio.id

                                   join doctre in _context.t_doctor_treatment
                                   on doc.id equals doctre.doctor_id

                                   join tcds in _context.t_current_doctor_specialization
                                   on doc.id equals tcds.doctor_id

                                   join spec in _context.m_specialization
                                   on tcds.specialization_id equals spec.id

                                   where doc.id == id
                                   select new VMSearch
                                   {
                                       doctor_id = doc.id,
                                       namaLokasi = loc.name,
                                       tindakanMedis = doctre.name,
                                       specialization = spec.name
                                   }
                                             ).FirstOrDefaultAsync(h => h.doctor_id == id);

            return list;
        }
        public async Task<List<m_specialization>> DataSpec()
        {
            List<m_specialization> cl = await (from c in _context.m_specialization select c).ToListAsync();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;
            return cl;
        }
        public async Task<List<m_location>> DataLoc()
        {
            List<m_location> cd = await (from c in _context.m_location select c).ToListAsync();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;
            return cd;
        }
        public async Task<List<t_doctor_treatment>> DataTreat()
        {
            List<t_doctor_treatment> ck = await (from c in _context.t_doctor_treatment select c).ToListAsync();
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;
            return ck;
        }
        public async Task<JsonResult> GetDataCariDOkterAjax(VMCariDokter _id)
        {
            List<VMCariDokter> list = await DataJM(_id);
            return Json(list);
        }
    }
}
