﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;

namespace MiniProject262.Views
{
    public class m_location_levelController : Controller
    {
        private readonly MiniProjectContext _context;

        public m_location_levelController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: m_location_level
        public async Task<IActionResult> Index(string SortOrder, string CurrentFilter, string SearchString, int? pageNumber, int? pSize)
        {
            m_admin dummy = new m_admin
            {
                id = 200
            };

            ViewBag.dummy = dummy.id;

            
            ViewData["CurrentSort"] = SortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(SortOrder) ? "name_desc" : "";
            ViewData["CodeSortParm"] = String.IsNullOrEmpty(SortOrder) ? "code_desc" : "";
            ViewData["CurrentFilter"] = SearchString;
            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<m_location_level>.PageSizeList();

            if (SearchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                SearchString = CurrentFilter;
            }

            ViewData["currentfilter"] = SearchString;

            var role = from r in _context.m_location_level
                       select r;
            if (!string.IsNullOrEmpty(SearchString))
            {
                role = role.Where(r => r.name.Contains(SearchString));
            }

            switch (SortOrder)
            {
                case "name_desc":
                    role = role.OrderByDescending(r => r.name);
                    break;
            }
            int pageSize = pSize ?? 5;
            return View(PageinatedList<m_location_level>.CreateAsync(role.AsNoTracking().Where(r => r.is_delete == false).ToList(), pageNumber ?? 1, pageSize));

            //return View(await _context.m_role.ToListAsync());
        }

        // GET: m_location_level/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_location_level = await _context.m_location_level
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_location_level == null)
            {
                return NotFound();
            }

            return View(m_location_level);
        }

        // GET: m_location_level/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: m_location_level/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,abbreviation,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_location_level m_location_level)
        {
            if (ModelState.IsValid)
            {
                _context.Add(m_location_level);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(m_location_level);
        }

        // GET: m_location_level/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_location_level = await _context.m_location_level.FindAsync(id);
            if (m_location_level == null)
            {
                return NotFound();
            }
            return View(m_location_level);
        }

        // POST: m_location_level/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,abbreviation,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_location_level m_location_level)
        {
            if (id != m_location_level.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(m_location_level);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_location_levelExists(m_location_level.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_location_level);
        }

        // GET: m_location_level/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_location_level = await _context.m_location_level
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_location_level == null)
            {
                return NotFound();
            }

            return View(m_location_level);
        }

        // POST: m_location_level/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id, [Bind("id,name,code,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_location_level m_location_level)
        {
            if (id != m_location_level.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    m_location_level.deleted_by = (long)HttpContext.Session.GetInt32("iduser");
                    m_location_level.is_delete = true;
                    m_location_level.deleted_on = DateTime.Now;
                    _context.Update(m_location_level);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_location_levelExists(m_location_level.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_location_level);
        }

        private bool m_location_levelExists(long id)
        {
            return _context.m_location_level.Any(e => e.id == id);
        }
    }
}
