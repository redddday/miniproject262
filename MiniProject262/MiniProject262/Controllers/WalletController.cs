﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MiniProject262.Models;
using MiniProject262.ViewModels;

namespace MiniProject262.Controllers
{
    public class WalletController : Controller
    {
        private readonly MiniProjectContext _context;

        public WalletController(MiniProjectContext context)
        {
            _context = context;
        }

        // GET: Wallet
        public IActionResult Index(string searchString)
        {
            ViewData["CurrentFilter"] = searchString;
            var nominals = from n in _context.m_wallet_default_nominal
                           join m in _context.m_user
                           on n.created_by equals m.id
                           join b in _context.m_biodata
                           on m.biodata_id equals b.id
                           where n.is_delete == false
                           select new VMUser
                           {
                               id = n.id,
                               nominal = n.nominal,
                               created_by = n.created_by,
                               created_on = n.created_on,
                               modified_by = n.modified_by,
                               modified_on = n.modified_on,
                               deleted_by = n.deleted_by,
                               deleted_on = n.deleted_on,
                               is_delete = n.is_delete,
                               fullname = b.fullname
                           };

            //var fname = from n in _context.m_wallet_default_nominal
            //            join m in _context.m_user
            //            on n.created_by equals m.id
            //            join b in _context.m_biodata
            //            on m.biodata_id equals b.id
            //            where n.is_delete == false && m.is_delete == false
            //            select b.fullname
            if (!String.IsNullOrEmpty(searchString))
            {
                char[] charsToTrim = { 'r', 'p', ' ' };
                searchString = searchString.Replace(",", "");
                searchString = searchString.Replace(".", "");
                nominals = nominals.Where(n => n.nominal.ToString().Equals(searchString.ToLower().Trim(charsToTrim)));
                if (nominals == null)
                {
                    ViewBag.temp = 0;
                }
                else
                {
                    ViewBag.temp = 1;
                }
            }
            return View(nominals.AsNoTracking());
            //return View(await _context.m_wallet_default_nominal.ToListAsync());
        }


        // GET: Wallet/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Wallet/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateConfirmed([Bind("id,nominal,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_wallet_default_nominal m_wallet)
        {
            var input = m_wallet.nominal;
            var wdn = await (from c in _context.m_wallet_default_nominal where c.is_delete == false select c.nominal).ToListAsync();
            var noms = wdn.ToArray();
            int counter = 0;
            for (int i = 0; i < noms.Length; i++)
            {
                if (input == noms[i])
                {
                    counter++;
                }
            }
            if (counter != 0)
            {
                return Json(new { status = "error", message = "* nominal sudah ada" });
            }
            else
            {
                long id = (long)HttpContext.Session.GetInt32("iduser");
                DateTime dt = DateTime.Now;
                m_wallet.created_by = id;
                m_wallet.created_on = dt;
                _context.Add(m_wallet);
                await _context.SaveChangesAsync();
                return Json(new { status = "success" });
            }
        }

        // GET: Wallet/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_wallet_default_nominal = await _context.m_wallet_default_nominal.FindAsync(id);
            if (m_wallet_default_nominal == null)
            {
                return NotFound();
            }
            return View(m_wallet_default_nominal);
        }

        // POST: Wallet/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,nominal,created_by,created_on,modified_by,modified_on,deleted_by,deleted_on,is_delete")] m_wallet_default_nominal m_wallet_d)
        {
            if (id != m_wallet_d.id)
            {
                return NotFound();
            }
            var m_wallet = await _context.m_wallet_default_nominal.FindAsync(id);
            if (ModelState.IsValid)
            {
                try
                {
                    long iduser = (long)HttpContext.Session.GetInt32("iduser");
                    DateTime dt = DateTime.Now;
                    m_wallet.nominal = m_wallet_d.nominal;
                    m_wallet.modified_by = iduser;
                    m_wallet.modified_on = dt;
                    _context.Update(m_wallet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!m_wallet_default_nominalExists(m_wallet_d.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(m_wallet);
        }

        // GET: Wallet/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var m_wallet_default_nominal = await _context.m_wallet_default_nominal
                .FirstOrDefaultAsync(m => m.id == id);
            if (m_wallet_default_nominal == null)
            {
                return NotFound();
            }

            return View(m_wallet_default_nominal);
        }

        // POST: Wallet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var m_wallet = await _context.m_wallet_default_nominal.FindAsync(id);
            long iduser = (long)HttpContext.Session.GetInt32("iduser");
            DateTime dt = DateTime.Now;
            m_wallet.deleted_by = iduser;
            m_wallet.deleted_on = dt;
            m_wallet.is_delete = true;
            _context.Update(m_wallet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool m_wallet_default_nominalExists(long id)
        {
            return _context.m_wallet_default_nominal.Any(e => e.id == id);
        }
    }
}
