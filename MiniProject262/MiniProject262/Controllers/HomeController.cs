﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MiniProject262.Models;
using MiniProject262.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace MiniProject262.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MiniProjectContext _context;

        public HomeController(ILogger<HomeController> logger, MiniProjectContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SendOTP(VMUser user)
        {
            Random rnd = new Random();
            int isiOtp = rnd.Next(100000, 999999);
            user.otp = isiOtp;

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

            client.EnableSsl = true;

            MailAddress from = new MailAddress("tupakshakur12345@gmail.com", "[Med.Id]");

            MailAddress to = new MailAddress(user.email);

            MailMessage message = new MailMessage(from, to);

            message.Subject = "Verifikasi Email Med.Id";/*"Gmail test email with SSL and Credentials";*/

            message.Body = "Kode OTP kamu dari Med.Id adalah - " + isiOtp;

            NetworkCredential myCreds = new NetworkCredential("tupakshakur12345@gmail.com", "udingambut", "");

            client.Credentials = myCreds;

            try

            {

                client.Send(message);
                return Json(new { status = "success", data = user });

            }

            catch (Exception ex)

            {
                return Json(new { status = "error" });
                //Console.WriteLine("Exception is:" + ex.ToString());

            }

            //Console.WriteLine("Goodbye.");
            return View();

        }
        public IActionResult Login()
        {
            if (HttpContext.Session.GetInt32("iduser") != null)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View();
            }
        }

        public async Task<IActionResult> Logout()
        {
            if (HttpContext.Session.GetInt32("iduser") != null)
            {
                long id = (long)HttpContext.Session.GetInt32("iduser");
                var user = await _context.m_user.FindAsync(id);
                DateTime dt = DateTime.Now;
                user.last_login = dt;
                _context.Update(user);
                await _context.SaveChangesAsync();
                HttpContext.Session.Clear();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(VMUser user)
        {
            VMUser vmu = await DataUser(user.email);
            var emailcheck = EmailExists(user.email);
            var passcheck = VerifyPass(user.email, user.password);
            if (emailcheck != null)
            {
                if (passcheck != null)
                {
                    if (passcheck.is_locked == false)
                    {
                        var tahun = vmu.created_on.Year;
                        HttpContext.Session.SetString("username", vmu.fullname);
                        HttpContext.Session.SetInt32("iduser", (int)vmu.id);
                        HttpContext.Session.SetInt32("idrole", (int)vmu.role_id);
                        //HttpContext.Session.SetInt32("iddok", (int)vmu.dokter_id);
                        HttpContext.Session.SetInt32("tahun", tahun);
                        HttpContext.Session.SetInt32("bioid", (int)vmu.biodata_id);
                        passcheck.login_attempt = 0;
                        _context.Update(passcheck);
                        await _context.SaveChangesAsync();
                        return Json(new { status = "success", data = vmu.fullname });
                    }
                    return Json(new { status = "error", message = "* akun anda terkunci" });
                }
                else
                {
                    if (emailcheck.login_attempt == 3)
                    {
                        DateTime dt = DateTime.Now;
                        emailcheck.is_locked = true;
                        emailcheck.modified_on = dt;
                        _context.Update(emailcheck);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        emailcheck.login_attempt += 1;
                        _context.Update(emailcheck);
                        await _context.SaveChangesAsync();
                    }
                    return Json(new { status = "error", message = "* email atau password salah" });
                }
            }
            else if (user.email != null && user.password != null)
            {
                return Json(new { status = "error", message = "* email atau password salah" });
            }
            else
            {
                return Json(new { status = "error", message = "* email atau password salah" });
            }
        }

        public IActionResult VerifEmail(int id)
        {
            if (id == 66)
            {
                return View();
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        public IActionResult OTP(VMUser user)
        {
            if (user.email == null)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                VMUser vmu = user;
                return View(vmu);
            }
        }

        public IActionResult SetPass(VMUser user)
        {
            if (user.email == null)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                if (user == null)
                {
                    return NotFound();
                }
                VMUser vmu = user;
                return View(vmu);
            }
        }

        public async Task<IActionResult> SetPassword(VMUser user)
        {
            if (user.email == null)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                var muser = await _context.m_user.FindAsync(user.id);
                if (muser == null)
                {
                    return Json(new { status = "error" });
                }
                VMUser vmu = new VMUser();
                vmu.id = muser.id;
                vmu.email = muser.email;
                vmu.password = muser.password;
                vmu.modified_on = muser.modified_on;
                return Json(new { status = "success", data = vmu });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetPassConfirmed(VMUser user)
        {
            if (user.email != null)
            {
                if (ModelState.IsValid)
                {
                    DateTime dt = DateTime.Now;
                    m_user muser = await _context.m_user.FindAsync(user.id);
                    t_reset_password tr = new t_reset_password();
                    tr.old_password = muser.password;
                    tr.new_password = user.new_password;
                    tr.reset_for = "Lupa password";
                    tr.created_by = muser.id;
                    tr.created_on = dt;
                    _context.Add(tr);
                    muser.password = user.new_password;
                    muser.modified_on = dt;
                    _context.Update(muser);
                    await _context.SaveChangesAsync();
                    return Json(new { status = "success" });
                }
                return Json(new { status = "error" });
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        public async Task<IActionResult> sendMail(VMUser user)
        {
            if (user.email != null)
            {
                var emailcheck = EmailExists(user.email);
                if (emailcheck != null)
                {
                    if (emailcheck.is_delete == false)
                    {
                        if (emailcheck.is_locked == false)
                        {
                            VMUser vmu = new VMUser();

                            var tokencheck = TokenExists(emailcheck.email);
                            if (tokencheck != null)
                            {
                                tokencheck.is_expired = true;
                                _context.Update(tokencheck);
                                await _context.SaveChangesAsync();
                            }

                            Random rnd = new Random();
                            int otp = rnd.Next(100000, 999999);
                            vmu.id = emailcheck.id;
                            vmu.biodata_id = emailcheck.biodata_id;
                            vmu.role_id = emailcheck.role_id;
                            vmu.email = emailcheck.email;
                            vmu.password = emailcheck.password;
                            vmu.login_attempt = emailcheck.login_attempt;
                            vmu.is_locked = emailcheck.is_locked;
                            vmu.last_login = emailcheck.last_login;
                            vmu.created_by = emailcheck.created_by;
                            vmu.created_on = emailcheck.created_on;
                            vmu.modified_by = emailcheck.modified_by;
                            vmu.modified_on = emailcheck.modified_on;
                            vmu.deleted_by = emailcheck.deleted_by;
                            vmu.deleted_on = emailcheck.deleted_on;
                            vmu.is_delete = emailcheck.is_delete;
                            vmu.otp = otp;

                            DateTime dt = DateTime.Now;
                            t_token tok = new t_token();
                            tok.email = emailcheck.email;
                            tok.user_id = emailcheck.id;
                            tok.token = otp.ToString();
                            tok.expired_on = dt.AddSeconds(90);
                            tok.is_expired = false;
                            tok.used_for = "Lupa password";
                            tok.created_by = emailcheck.id;
                            tok.created_on = dt;
                            _context.Add(tok);
                            await _context.SaveChangesAsync();

                            m_biodata bio = await _context.m_biodata.FindAsync(emailcheck.biodata_id);

                            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

                            client.EnableSsl = true;

                            MailAddress from = new MailAddress("tupakshakur12345@gmail.com", "Med.Id");

                            MailAddress to = new MailAddress(emailcheck.email, bio.fullname);

                            MailMessage message = new MailMessage(from, to);

                            message.IsBodyHtml = true;

                            message.Body = "<html><body>Kode OTP anda adalah <b>" + otp + "</b><br>" +
                                "Jangan beritahukan kode OTP anda kepada siapapun termasuk kepada pihak Med.Id</body></html>";

                            message.Subject = "[Verifikasi email untuk akun Med.Id]";

                            NetworkCredential myCreds = new NetworkCredential("tupakshakur12345@gmail.com", "udingambut", "");

                            client.Credentials = myCreds;

                            try
                            {
                                client.Send(message);
                                return Json(new { status = "success", data = vmu });
                            }
                            catch (Exception)
                            {
                                return Json(new { status = "error" });
                            }
                        }
                        else
                        {
                            return Json(new { status = "error", message = "* akun anda terkunci" });
                        }
                    }
                    else
                    {
                        return Json(new { status = "error", message = "* email tidak terdaftar" });
                    }
                }
                else
                {
                    return Json(new { status = "error", message = "* email tidak terdaftar" });
                }
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // priority table = m_user for login *denis
        // base properties yg dipake dari m_user
        public async Task<VMUser> DataUser(string email)
        {
            VMUser mus = await (from mu in _context.m_user
                                join bi in _context.m_biodata
                                    on mu.biodata_id equals bi.id
                                join ro in _context.m_role
                                    on mu.role_id equals ro.id
                                //join dok in _context.m_doctor
                                //    on bi.id equals dok.biodata_id
                                where mu.email == email && mu.is_delete == false && bi.is_delete == false
                                select new VMUser
                                {
                                    id = mu.id,
                                    biodata_id = bi.id,
                                    //dokter_id = dok.id,
                                    role_id = ro.id,
                                    email = mu.email,
                                    password = mu.password,
                                    login_attempt = mu.login_attempt,
                                    is_locked = mu.is_locked,
                                    last_login = mu.last_login,
                                    name = ro.name,
                                    code = ro.code,
                                    fullname = bi.fullname,
                                    mobile_phone = bi.mobile_phone,
                                    image = bi.image,
                                    image_path = bi.image_path,
                                    created_by = mu.created_by,
                                    created_on = mu.created_on,
                                    modified_by = mu.modified_by,
                                    modified_on = mu.modified_on,
                                    deleted_by = mu.deleted_by,
                                    deleted_on = mu.deleted_on,
                                    is_delete = mu.is_delete
                                }).FirstOrDefaultAsync();
            return mus;
        }

        private m_user EmailExists(string email)
        {
            return _context.m_user.FirstOrDefault(a => a.email == email);
        }

        private t_token TokenExists(string email)
        {
            //return _context.t_token.Where(a => a.email == email && a.is_expired == false).ToList();
            return _context.t_token.FirstOrDefault(a => a.email == email && a.is_expired == false);
        }

        private m_user VerifyPass(string email, string password)
        {
            return _context.m_user.FirstOrDefault(a => a.email == email && a.password == password);
        }

        public IActionResult Privacy()
        {
            return View();
        }


        public IActionResult EmailCheck(m_user user)
        {
            return View();
        }

        public IActionResult Register(VMUser user)
        {
            List<m_role> listCat = new List<m_role>();
            listCat = (from r in _context.m_role select r).ToList();
            listCat.Insert(0, new m_role { id = 0, name = "--Pilih--" });
            ViewBag.lc = listCat;
            //VMUser emailBag = new VMUser();
            //user.email = emailBag.email();


            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterConfirm(VMUser user)
        {
            if (ModelState.IsValid)
            {
                DateTime tgl = DateTime.Now;
                m_biodata bioNew = new m_biodata();
                bioNew.fullname = user.fullname;
                bioNew.mobile_phone = user.mobile_phone;
                bioNew.created_on = tgl;
                _context.Add(bioNew);
                await _context.SaveChangesAsync();

                var bioId = await _context.m_biodata.Where(a => a.fullname == user.fullname).FirstOrDefaultAsync();


                m_role roleNew = new m_role();
                roleNew.id = user.role_id;
                m_user userNew = new m_user();
                userNew.email = user.email;
                userNew.password = user.password;
                userNew.biodata_id = bioId.id;
                userNew.role_id = user.role_id;
                userNew.created_by = -1;
                userNew.created_on = tgl;
                _context.Add(userNew);


                await _context.SaveChangesAsync();
                return RedirectToAction("Index");

            }
            return View(user);
        }


        //[HttpPost]
        //      [ValidateAntiForgeryToken]
        //      public async Task<IActionResult> RegisterUser(VMUser user)
        //      {
        //          var EmailAvail = _context.m_user.FirstOrDefault(u => u.email == user.email);
        //          var EmailOk = RegExist(user.email);

        //          if (ModelState.IsValid)
        //          {
        //              m_user cus = new m_user();
        //              cus.biodata_id = customer.NameCustomer;
        //              cus.Email = customer.Email;
        //              cus.Password = customer.Password;
        //              cus.Address = customer.Address;

        //              var hashsalt = EncryptPassword(customer.Password);
        //              cus.Password = hashsalt.Hash;
        //              cus.StoredSalt = hashsalt.Salt;

        //              _context.Add(cus);
        //              await _context.SaveChangesAsync();
        //              return RedirectToAction("Login");
        //          }
        //          return View(customer);
        //      }

        ////public IActionResult GetOtp(VMCustomer cust)
        ////{
        ////    Random rnd = new Random();
        ////    int otp = rnd.Next(1000, 9999);
        ////    ViewData["msgotp"] = otp;
        ////    cust.Otp = otp.ToString();
        ////    string msg = "Your OTP from Agithentic.ID is " + otp;
        ////    bool f = SendEmail("agithamohammad@gmail.com", cust.Email, "Subjected to OTP", msg, cust.NameCustomer);
        ////    if (f)
        ////    {
        ////        return Json(new { status = "success", data = cust });
        ////    }
        ////    else
        ////    {
        ////        return Json(new { status = "error" });
        ////    }
        ////}

        ////public bool SendEmail(string from, string to, string subject, string body, string username)
        ////{
        ////    return View(from);
        ////}

        public string ValidateEmail(string cekEmail)
        {
            m_user user = new m_user();
            user = _context.m_user.Where(a => a.email == cekEmail && a.is_delete == false).FirstOrDefault();
            if (user != null)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }

        public IActionResult VerifyOTP(VMUser user)
        {
            return View(user);
        }

        //public IActionResult GetPassword(VMUser user)
        //{
        //	return View(user);
        //}
        public IActionResult CreatePassword(VMUser user)
        {
            return View(user);
        }

        [HttpPost]
        public IActionResult CreatePassConfirm(VMUser user)
        {
            return Json(new { status = "success", data = user });

        }

        //private Customer RegExist(string email)
        //{
        //    return _context.Customer.FirstOrDefault(a => a.Email == email);
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Create(string searchString)
        {

            List<m_specialization> cl = await DataSpec();
            ViewBag.ListSpec = cl;

            List<m_location> cd = await DataLoc();
            ViewBag.ListLoc = cd;

            //List<t_doctor_treatment> ck = await DataTreat();
            //ViewBag.LisDocTreat = ck;

            var ck = (from y in _context.t_doctor_treatment
                      where y.is_delete == false
                      group y by y.name into g

                      select new { name = g.Key }).ToList();
            //orderby newGroup.Key
            ck.Insert(0, new { name = " " });

            ViewBag.LisDocTreat = ck;

            return View();


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VMCariDokter vMCariDokter)
        {

            VMCariDokter newDoc = await DataKM(vMCariDokter.id);

            List<m_specialization> cl = new List<m_specialization>().Where(a => a.is_delete = false).ToList();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;

            List<m_location> cd = new List<m_location>().Where(a => a.parent_id == 0 || a.parent_id == null && a.is_delete == false).ToList();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;

            List<t_doctor_treatment> ck = new List<t_doctor_treatment>().Where(a => a.is_delete = false).ToList(); ;
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;



            //ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });

            return View(newDoc);
        }
        public async Task<List<m_specialization>> DataSpec()
        {
            List<m_specialization> cl = await (from c in _context.m_specialization select c).Where(a => a.is_delete == false).ToListAsync();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;
            return cl;
        }
        public async Task<List<m_location>> DataLoc()
        {
            List<m_location> cd = await (from c in _context.m_location select c).Where(a => a.parent_id == 0 || a.parent_id == null && a.is_delete == false).ToListAsync();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;
            return cd;
        }
        public async Task<List<t_doctor_treatment>> DataTreat()
        {
            List<t_doctor_treatment> ck = await (from c in _context.t_doctor_treatment select c).Where(a => a.is_delete == false).ToListAsync();
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;

            return ck;

            //return ck;
        }
        public async Task<VMCariDokter> DataKM(long? id)
        {
            VMCariDokter list = await (from loclev in _context.m_location_level
                                       join loc in _context.m_location
                                       on loclev.id equals loc.location_level_id

                                       join medfac in _context.m_medical_facility
                                       on loc.id equals medfac.location_id

                                       join docof in _context.t_doctor_office
                                       on medfac.id equals docof.mdeical_facility_id

                                       join doc in _context.m_doctor
                                       on docof.doctor_id equals doc.id

                                       join bio in _context.m_biodata
                                       on doc.biodata_id equals bio.id

                                       join doctre in _context.t_doctor_treatment
                                       on doc.id equals doctre.doctor_id

                                       join tcds in _context.t_current_doctor_specialization
                                       on doc.id equals tcds.doctor_id

                                       join spec in _context.m_specialization
                                       on tcds.specialization_id equals spec.id

                                       select new VMCariDokter

                                       {
                                           //doctor_id = doc.id,
                                           //biodata_id = bio.id,
                                           //mdeical_facility_id = medfac.id,
                                           //location_id = loc.id,
                                           //location_level_id = loclev.id,
                                           //specialization_id = spec.id,
                                           id = doc.id,

                                           fullname = bio.fullname,
                                           namaLokasi = loc.name,
                                           specialization = spec.name,
                                           tindakanMedis = doctre.name,

                                           image = bio.image,
                                           image_path = bio.image_path
                                       }
                                              ).FirstOrDefaultAsync();

            return list;
        }
        public async Task<IActionResult> CariDokter(string lokasi, string namadok, string spesial, string tindakanmedis, string searchString, int? pageNumber, int? pSize, string currentFilter, string currentFilter1, string currentFilter2, string currentFilter3)
        {
            var students = from doc in _context.m_doctor
                               //on docof.doctor_id equals doc.id

                           join bio in _context.m_biodata
                           on doc.biodata_id equals bio.id

                           //join doctre in _context.t_doctor_treatment
                           //on doc.id equals doctre.doctor_id

                           join tcds in _context.t_current_doctor_specialization
                           on doc.id equals tcds.doctor_id

                           join spec in _context.m_specialization
                           on tcds.specialization_id equals spec.id

                           //join tdos in _context.t_doctor_office_schedule
                           //on doc.id equals tdos.doctor_id

                           //join mfs in _context.m_medical_facility_schedule
                           //on medfac.id equals mfs.medical_facility_id

                           where doc.is_delete==false

                           select new VMCariDokter
                           {
                               doctor_id = doc.id,
                               id = doc.id,
                               fullname = bio.fullname,
                               specialization = spec.name,
                               //tindakanMedis = doctre.name,
                               image = bio.image,
                               image_path = bio.image_path,
                              // pengalaman = CalculateAge(tdos.created_on)

                           };

            var apl = from loclev in _context.m_location_level
                      join loc in _context.m_location
                      on loclev.id equals loc.location_level_id
                      join medfac in _context.m_medical_facility
                      on loc.id equals medfac.location_id
                      join docof in _context.t_doctor_office
                      on medfac.id equals docof.mdeical_facility_id
                      join doc in _context.m_doctor
                       on docof.doctor_id equals doc.id

                      where doc.is_delete==false && docof.is_delete==false && medfac.medical_facility_category_id != 2

                      select new VMCariDokter
                      {
                          id = doc.id,
                          DoctorOffice_id = docof.id,
                          DoctorBackup_id = docof.doctor_id,
                          mdeical_facility_id=medfac.id,
                          

                          namaLokasi = loc.name,
                          rumahSakit = medfac.name,
                          full_address = medfac.full_address
                      };
            ViewBag.APL = apl;

            var apk = (from loclev in _context.m_location_level
                      join loc in _context.m_location
                      on loclev.id equals loc.location_level_id
                      join medfac in _context.m_medical_facility
                      on loc.id equals medfac.location_id
                      join docof in _context.t_doctor_office
                      on medfac.id equals docof.mdeical_facility_id
                      join doc in _context.m_doctor
                       on docof.doctor_id equals doc.id

                       where doc.is_delete == false && docof.is_delete == false

                       select new VMCariDokter
                      {
                          id = doc.id,
                          DoctorOffice_id = docof.id,
                          DoctorBackup_id = docof.doctor_id,
                          mdeical_facility_id = medfac.id,


                          namaLokasi = loc.name,
                          rumahSakit = medfac.name,
                          full_address = medfac.full_address
                      }).ToList();
            ViewBag.APK = apk;

            var usee = from doc in _context.m_doctor
                       join doctre in _context.t_doctor_treatment
                             on doc.id equals doctre.doctor_id

                       where doc.is_delete == false
                       select new VMCariDokter
                       {
                           doctor_id = doc.id,
                           id = doc.id,

                           tindakanMedis = doctre.name,
                           //pengalaman = CalculateAge(tdos.created_on)

                       };
            ViewBag.USEE = usee;

            var students1 = from loclev in _context.m_location_level
                            join loc in _context.m_location
                            on loclev.id equals loc.location_level_id

                            join medfac in _context.m_medical_facility
                            on loc.id equals medfac.location_id

                            join docof in _context.t_doctor_office
                            on medfac.id equals docof.mdeical_facility_id

                            join doc in _context.m_doctor
                                on docof.doctor_id equals doc.id

                            join bio in _context.m_biodata
                            on doc.biodata_id equals bio.id

                            //join doctre in _context.t_doctor_treatment
                            //on doc.id equals doctre.doctor_id

                            join tcds in _context.t_current_doctor_specialization
                            on doc.id equals tcds.doctor_id

                            join spec in _context.m_specialization
                            on tcds.specialization_id equals spec.id

                            //join tdos in _context.t_doctor_office_schedule
                            //on doc.id equals tdos.doctor_id

                            where loc.name == lokasi || spec.name == spesial || /*doctre.name == tindakanmedis||*/ bio.fullname == namadok && doc.is_delete == false && docof.is_delete==false && medfac.medical_facility_category_id != 2

                            select new VMCariDokter
                            {
                                doctor_id = doc.id,
                                id = doc.id,

                                fullname = bio.fullname,
                                namaLokasi = loc.name,
                                specialization = spec.name,
                                //tindakanMedis = doctre.name,
                                rumahSakit = medfac.name,
                                full_address = medfac.full_address,

                                image = bio.image,
                                image_path = bio.image_path,
                                is_delete=docof.is_delete
                                //pengalaman = CalculateAge(tdos.created_on)

                            };

            var students6 = from loclev in _context.m_location_level
                            join loc in _context.m_location
                            on loclev.id equals loc.location_level_id

                            join medfac in _context.m_medical_facility
                            on loc.id equals medfac.location_id

                            join docof in _context.t_doctor_office
                            on medfac.id equals docof.mdeical_facility_id

                            join doc in _context.m_doctor
                                on docof.doctor_id equals doc.id

                            join bio in _context.m_biodata
                            on doc.biodata_id equals bio.id

                            join doctre in _context.t_doctor_treatment
                            on doc.id equals doctre.doctor_id

                            join tcds in _context.t_current_doctor_specialization
                            on doc.id equals tcds.doctor_id

                            join spec in _context.m_specialization
                            on tcds.specialization_id equals spec.id

                            where loc.name == lokasi || spec.name == spesial || doctre.name == tindakanmedis|| bio.fullname == namadok && doc.is_delete == false && docof.is_delete == false && medfac.medical_facility_category_id != 2

                            select new VMCariDokter
                            {
                                doctor_id = doc.id,
                                id = doc.id,

                                fullname = bio.fullname,
                                namaLokasi = loc.name,
                                specialization = spec.name,
                                tindakanMedis = doctre.name,
                                rumahSakit = medfac.name,
                                full_address = medfac.full_address,

                                image = bio.image,
                                image_path = bio.image_path,
                                is_delete = docof.is_delete
                                //pengalaman = CalculateAge(tdos.created_on)

                            };
            ViewBag.student = students1;

            var students2 = from loclev in _context.m_location_level
                            join loc in _context.m_location
                            on loclev.id equals loc.location_level_id

                            join medfac in _context.m_medical_facility
                            on loc.id equals medfac.location_id

                            join docof in _context.t_doctor_office
                            on medfac.id equals docof.mdeical_facility_id

                            join doc in _context.m_doctor
                             on docof.doctor_id equals doc.id

                            join bio in _context.m_biodata
                            on doc.biodata_id equals bio.id

                            join doctre in _context.t_doctor_treatment
                            on doc.id equals doctre.doctor_id

                            join tcds in _context.t_current_doctor_specialization
                            on doc.id equals tcds.doctor_id

                            join spec in _context.m_specialization
                            on tcds.specialization_id equals spec.id

                            //join tdos in _context.t_doctor_office_schedule
                            //on doc.id equals tdos.doctor_id

                            where loc.name == lokasi && spec.name == spesial && doctre.name == tindakanmedis && doc.is_delete == false && docof.is_delete == false && medfac.medical_facility_category_id!=2

                            select new VMCariDokter
                            {

                                doctor_id = doc.id,
                                id = doc.id,

                                fullname = bio.fullname,
                                namaLokasi = loc.name,
                                specialization = spec.name,
                                tindakanMedis = doctre.name,
                                rumahSakit = medfac.name,
                                full_address = medfac.full_address,

                                image = bio.image,
                                image_path = bio.image_path,
                                is_delete=docof.is_delete
                                //pengalaman = CalculateAge(tdos.created_on)

                            };

            var students3 =
                           from doc in _context.m_doctor

                           join bio in _context.m_biodata
                           on doc.biodata_id equals bio.id

                           join doctre in _context.t_doctor_treatment
                           on doc.id equals doctre.doctor_id

                           join tcds in _context.t_current_doctor_specialization
                           on doc.id equals tcds.doctor_id

                           join spec in _context.m_specialization
                           on tcds.specialization_id equals spec.id

                           //join tdos in _context.t_doctor_office_schedule
                           //on doc.id equals tdos.doctor_id

                           where spec.name == spesial || doctre.name == tindakanmedis || bio.fullname == namadok && doc.is_delete == false

                           select new VMCariDokter
                           {
                               doctor_id = doc.id,
                               id = doc.id,

                               fullname = bio.fullname,
                               specialization = spec.name,
                               tindakanMedis = doctre.name,

                               image = bio.image,
                               image_path = bio.image_path,
                               //pengalaman = CalculateAge(tdos.created_on)

                           };

            var mario =

                        (from medfac in _context.m_medical_facility
                            

                         join docof in _context.t_doctor_office
                         on medfac.id equals docof.mdeical_facility_id

                        join doc in _context.m_doctor
                        on docof.doctor_id equals doc.id


                        join bio in _context.m_biodata
                         on doc.biodata_id equals bio.id

                        join tdos in _context.t_doctor_office_schedule
                        on doc.id equals tdos.doctor_id

                        join medfacsche in _context.m_medical_facility_schedule
                        on medfac.id equals medfacsche.medical_facility_id

                        where tdos.doctor_id==doc.id && doc.is_delete==false
                        orderby tdos.created_by descending

                        select new VMDetailDokter

                        {
                            id=medfac.id,
                            doctor_id=doc.id,
                            tahun = CalculateAge(tdos.created_on),
                            created_on=tdos.created_on
                                                      
                        }).ToList();



            ViewBag.MARIO = mario.OrderByDescending(a=>a.tahun);

            var lgc = from medfac in _context.m_medical_facility
                      join medfaccat in _context.m_medical_facility_category

                      on medfac.medical_facility_category_id equals medfaccat.id

                      join medfacsche in _context.m_medical_facility_schedule
                      on medfac.id equals medfacsche.medical_facility_id

                      join docof in _context.t_doctor_office
                      on medfac.id equals docof.mdeical_facility_id

                      //join doc in _context.m_doctor
                      // on docof.doctor_id equals doc.id

                      join tdos in _context.t_doctor_office_schedule
                     on medfacsche.id equals tdos.medical_facility_schedule_id

                      where medfaccat.id == 2

                      select new VMDetailDokter
                      {
                          //doctor_id=doc.id,
                          id = medfac.id,
                          medical_facility_category_id=medfaccat.id,
                          day=medfacsche.day,
                          time_schedule_start= medfacsche.time_schedule_start,
                          time_schedule_end=medfacsche.time_schedule_end,
                          jamAwal1= DateTime.ParseExact(medfacsche.time_schedule_start, "H:mm", CultureInfo.InvariantCulture).TimeOfDay,
                          jamAkhir1= DateTime.ParseExact(medfacsche.time_schedule_end, "H:mm", CultureInfo.InvariantCulture).TimeOfDay,
                          DoctorBackup_id=tdos.doctor_id
                      };


            var dokID = students.Select(a => a.doctor_id);
            var jamawal = lgc.Select(a => DateTime.ParseExact(a.time_schedule_end, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay);
            var uco = 0;
            var day = (int)DateTime.Now.DayOfWeek;
            var mgh = lgc.Select(a => a.day);
            var klmn = 0;
            var jam = DateTime.Now.TimeOfDay;

            ViewBag.JAMAWAL = jamawal;
            ViewBag.JAM = jam;
            ViewBag.KLMN = klmn;
            ViewBag.DAY = day;
            ViewBag.LGC = lgc.ToList();
            ViewBag.UCO = uco;

            var luigi = (from medfacsche in _context.m_medical_facility_schedule
                        group medfacsche by medfacsche.medical_facility_id into g
                        
                        select new { medical_facility_id = g.Key }).ToList();
            ViewBag.LUIGI = luigi;

            List<m_specialization> cl = new List<m_specialization>();
            cl.Insert(0, new m_specialization { id = 0, name = " ", is_delete = false });
            ViewBag.ListSpec = cl;

            List<m_location> cd = new List<m_location>();
            cd.Insert(0, new m_location { id = 0, name = " ", is_delete = false });
            ViewBag.ListLoc = cd;

            List<t_doctor_treatment> ck = new List<t_doctor_treatment>();
            ck.Insert(0, new t_doctor_treatment { id = 0, name = " ", is_delete = false });
            ViewBag.LisDocTreat = ck;

            ViewData["CurrentpSize"] = pSize;
            ViewBag.pageSize = PageinatedList<VMCariDokter>.PageSizeList();


            if (namadok != null || lokasi != null || spesial != null || tindakanmedis != null)
            {
                pageNumber = 1;
            }
            else
            {
                namadok = currentFilter;
                lokasi = currentFilter1;
                spesial = currentFilter2;
                tindakanmedis = currentFilter3;
            }

            ViewData["CurrentFilter"] = namadok;
            ViewData["CurrentFilter1"] = lokasi;
            ViewData["CurrentFilter2"] = spesial;
            ViewData["CurrentFilter3"] = tindakanmedis;
            ViewBag.Filter1 = namadok;
            ViewBag.Filter2 = lokasi;
            ViewBag.Filter3 = spesial;
            ViewBag.Filter4 = tindakanmedis;



            if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(namadok) && !String.IsNullOrEmpty(lokasi))
            {
                students = students6.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial) && s.fullname.Contains(namadok) && s.namaLokasi.Contains(lokasi) && s.is_delete == false);

            }
            else if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(namadok))
            {
                students = students.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial) && s.fullname.Contains(namadok));

            }
            else if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(lokasi))
            {
                students = students2.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial) && s.namaLokasi.Contains(lokasi) && s.is_delete == false);

            }
            else if (!String.IsNullOrEmpty(namadok) && !String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(lokasi))
            {
                students = students1.Where(s => s.fullname.Contains(namadok) && s.specialization.Contains(spesial) && s.namaLokasi.Contains(lokasi) && s.is_delete == false);

            }
            else if (!String.IsNullOrEmpty(spesial) && !String.IsNullOrEmpty(lokasi))
            {

                students = students1.Where(s => s.specialization.Contains(spesial) && s.namaLokasi.Contains(lokasi)&&s.is_delete==false);

            }
            else if (!String.IsNullOrEmpty(tindakanmedis) && !String.IsNullOrEmpty(spesial))
            {
                students = students3.Where(s => s.tindakanMedis.Contains(tindakanmedis) && s.specialization.Contains(spesial));

            }
            else if (!String.IsNullOrEmpty(namadok) && !String.IsNullOrEmpty(spesial))
            {
                students = students.Where(s => s.fullname.Contains(namadok) && s.specialization.Contains(spesial));

            }
            else if (!String.IsNullOrEmpty(spesial))
            {
                students = students.Where(s => s.specialization.Contains(spesial));

            }


            int pageSize = pSize ?? 8;
            ViewBag.pagsize = ViewData["CurrentpSize"];
            //return View(await students.AsNoTracking().ToListAsync()) ;
            return View(PageinatedList<VMCariDokter>.CreateAsync(students.AsNoTracking().ToList(), pageNumber ?? 1, pageSize));
            //return View(students);
        }
        public static int CalculateAge(DateTime? dob)
        {
            int age = 0;
            DateTime doba = Convert.ToDateTime(dob);

            age = DateTime.Now.Year - doba.Year;
            if (DateTime.Now.DayOfYear < doba.DayOfYear)
            {
                age = age - 1;
            }

            return age;
        }
    }
}
